<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class PatnerseedsTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('groups')->truncate();

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Users',
            'permissions' => array(
                'admin' => 0,
                'users' => 1,
            )));

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Admins',
            'permissions' => array(
                'admin' => 1,
                'users' => 1,
            )));
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Agronomists',
            'permissions' => array(
                'agronomist' => 1
            )));
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Logistics',
            'permissions' => array(
                'logistic' => 1
            )));


        Sentry::getGroupProvider()->create(array(
            'name'        => 'Partner_Admin',
            'permissions' => array(
                'patner_admin' => 1
            )));

        Sentry::getGroupProvider()->create(array(
            'name'        => 'Partner_User',
            'permissions' => array(
                'partner_user' => 1
            )));
    }
}
