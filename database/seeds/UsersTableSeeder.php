<?php

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        $users = [
            [
                'role_id' => 1,
                'name' => 'Demo Admin',
                'email' => 'admin@ojaygreene.com',
                'phone_no' => '0700111222',
                'password' => Hash::make('admin'),
                'verification_code' => '',
                'confirmed' => 1

            ],
            [
                'role_id' => 2,
                'name' => 'Demo Agronomist',
                'email' => 'agronomist@ojaygreene.com',
                'phone_no' => '0700333444',
                'password' => Hash::make('agronomist'),
                'verification_code' => '',
                'confirmed' => 1

            ],
            [
                'role_id' => 3,
                'name' => 'FarmDrive',
                'email' => 'farmdrive@farmdrive.com',
                'phone_no' => '0700555666',
                'password' => Hash::make('farmdrive'),
                'verification_code' => '',
                'confirmed' => 1

            ]
        ];

        DB::table('users')->insert($users);
    }
}
