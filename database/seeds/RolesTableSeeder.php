<?php

use Illuminate\Database\Seeder;


class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();

        $roles = array(
            [
                'name' => 'Admin',
            ],
            [
                'name' => 'Agronomist',
            ],
            [
                'name' => 'Third party',
            ],
        );

        DB::table('roles')->insert($roles);
    }
}
