<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWardFarmers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('farmers', function (Blueprint $table) {
			$table->string('ward')->default(0)->after('subscribed');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('farmers', function(Blueprint $table)
		{
			$table->dropColumn('ward');

		});
	}

}
