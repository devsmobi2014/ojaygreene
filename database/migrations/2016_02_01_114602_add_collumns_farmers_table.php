<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsFarmersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('farmers', function(Blueprint $table)
        {
            $table->integer('registration_status')->default(1);
            $table->integer('notification_status')->default(1);
            $table->integer('excel_id')->nullable();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('farmers', function(Blueprint $table)
        {
            $table->dropColumn('registration_status');
            $table->dropColumn('notification_status')->default(1);
            $table->dropColumn('excel_id');

        });
	}

}
