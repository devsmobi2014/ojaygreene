<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNationalIdFarmers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('farmers', function (Blueprint $table) {
			$table->string('national_id')->default(0)->after('subscribed');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('farmers', function(Blueprint $table)
		{
			$table->dropColumn('national_id');
			
		});
	}

}
