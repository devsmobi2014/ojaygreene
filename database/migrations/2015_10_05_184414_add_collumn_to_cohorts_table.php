<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnToCohortsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('cohorts', function(Blueprint $table)
        {
            $table->dropColumn('period');
            $table->string('start_date', 50)->nullable();
            $table->string('end_date', 50)->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('cohorts', function(Blueprint $table)
        {
        $table->string('period')->nullable();
        $table->dropColumn('start_date');
        $table->dropColumn('end_date');

        });
	}

}
