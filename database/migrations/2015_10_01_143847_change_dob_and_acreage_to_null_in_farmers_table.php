<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDobAndAcreageToNullInFarmersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('farmers', function(Blueprint $table)
		{
			$table->string('dob', 50)->nullable()->change();
			$table->string('acreage', 50)->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('farmers', function(Blueprint $table)
		{
			//
		});
	}

}
