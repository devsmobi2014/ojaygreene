<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLevelTooUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
			Schema::table('farmers', function (Blueprint $table) {
				$table->integer('level')->default(0)->after('excel_id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('farmers', function(Blueprint $table)
		{
			$table->dropColumn('level');

		});
	}

}
