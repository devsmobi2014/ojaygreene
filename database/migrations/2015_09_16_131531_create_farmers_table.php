<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('farmers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('cohort_id')->unsigned();
            $table->foreign('cohort_id')->references('id')->on('cohorts');
            $table->string('name', 200);
            $table->string('phone', 45);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('farmers');
	}

}
