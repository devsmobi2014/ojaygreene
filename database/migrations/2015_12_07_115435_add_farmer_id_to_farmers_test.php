<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFarmerIdToFarmersTest extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('farmers_test', function($table)
        {
            $table->string('farmer_id')->after('dob');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('farmers_test', function($table)
        {
            $table->dropColumn('farmer_id');

        });
	}

}
