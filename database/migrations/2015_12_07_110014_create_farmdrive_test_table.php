<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmdriveTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('farmers_test', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 200);
            $table->string('phone', 45);
            $table->string('acreage', 45);
            $table->string('gender', 45);
            $table->string('dob', 45);
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regions');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('farmers_test');
	}

}
