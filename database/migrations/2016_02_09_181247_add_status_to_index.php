<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToIndex extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {
        //
        Schema::table('inbox', function (Blueprint $table) {
            $table->integer('status')->default(0)->after('response');

        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('inbox', function(Blueprint $table)
        {
            $table->dropColumn('status');

        });


    }

}
