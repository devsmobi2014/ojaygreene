<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionCropFarmers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('region_crop_farmers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('crop_region_id')->unsigned()->index();
            $table->foreign('crop_region_id')->references('id')->on('crop_region')->onDelete('cascade');
            $table->integer('farmer_id')->unsigned()->index();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('region_crop_farmers');
	}

}
