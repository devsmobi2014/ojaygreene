<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutboxMessage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('outbox', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('phone');
            $table->integer('farmer_id');
            $table->string('message');
            $table->integer('message_id')->nullable();
            $table->integer('status');
            $table->string('type')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('outbox');
	}

}
