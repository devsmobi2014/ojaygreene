<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHarvestDateToRoundTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('rounds', function(Blueprint $table)
        {

            $table->string('harvest_date', 45)->after('start_date');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('rounds', function($table)
        {
            $table->dropColumn('harvest_date');

        });
	}

}
