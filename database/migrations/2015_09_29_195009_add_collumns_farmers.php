<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsFarmers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('farmers', function($table)
        {

            $table->string('gender')->after('acreage');
            $table->string('dob')->after('gender');
            $table->string('farmer_id')->after('dob');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

        Schema::table('farmers', function($table)
        {
            $table->dropColumn('gender');
            $table->dropColumn('dob');
            $table->dropColumn('farmer_id');
        });
	}

}
