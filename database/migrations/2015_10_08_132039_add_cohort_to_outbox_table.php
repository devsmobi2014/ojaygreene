<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCohortToOutboxTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('outbox', function($table)
        {
            $table->integer('cohort_id')->nullable()->after('message');
            $table->string('phone', 50)->change();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('outbox', function($table)
        {
            $table->dropColumn('cohort_id');
            $table->integer('phone', 50)->change();

        });
	}

}
