<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatnerCohortsFarmersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patner_cohorts_farmers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('farmer_id');
			$table->integer('cohort_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patner_cohorts_farmers');
	}

}
