<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCohortFarmersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

        Schema::table('farmers', function($table)
        {
            $table->dropForeign('farmers_cohort_id_foreign');
            $table->dropColumn('cohort_id');
        });

        Schema::table('farmers', function($table)
        {
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regions');
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
