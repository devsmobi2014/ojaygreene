<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatnerCohortsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patner_cohorts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('names');
			$table->integer('patner_id');
			$table->integer('value_chain_id');
			$table->text('description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patner_cohorts');
	}

}
