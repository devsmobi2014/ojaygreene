<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFarmersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('farmers', function (Blueprint $table) {

			$table->string('farmer_group')->change();
			$table->string('constituency')->change();

		});
	}

}
