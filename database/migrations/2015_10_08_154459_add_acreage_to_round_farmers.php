<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAcreageToRoundFarmers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('round_farmers', function($table)
        {
            $table->integer('acreage')->nullable()->after('actual_yield');


        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('round_farmers', function($table) {
            $table->dropColumn('acreage');
        });
	}

}
