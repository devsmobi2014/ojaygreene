<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatnerCohortsMeasurablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patner_cohorts_measurables', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cohort_id');
			$table->string('name');
			$table->string('unit');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patner_cohorts_measurables');
	}

}
