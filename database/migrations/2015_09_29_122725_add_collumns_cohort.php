<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsCohort extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('cohorts', function($table)
        {
            $table->string('name')->after('round_id');
            $table->integer('period')->after('name');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('cohorts', function($table)
        {
            $table->dropColumn('name');
            $table->dropColumn('period');
        });
	}

}
