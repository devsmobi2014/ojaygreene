<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPatnersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('patners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('names', 45);
			$table->string('phone', 45);
			$table->string('category', 45);
			$table->text('description');
			$table->string('patner_logo')->nullable();
			$table->string('patner_banner')->nullable();
			$table->string('background_color')->nullable();
			$table->string('head_font_color')->nullable();
			$table->string('sidebar_color')->nullable();
			$table->string('sidebar_font_color')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('patners');
	}

}
