<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultRegionPatnerRegions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('patner_regions', function (Blueprint $table) {
			$table->integer('default_region')->nullable()->default(0);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('patner_regions', function (Blueprint $table) {
			$table->dropColumn('default_region');

		});
	}

}
