<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCropRegionForegn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('rounds', function(Blueprint $table)
        {

            $table->dropForeign('rounds_region_crop_id_foreign');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('rounds', function(Blueprint $table) {
            $table->foreign('region_crop_id')->references('id')->on('crop_region');
        });
	}

}
