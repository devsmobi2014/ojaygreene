<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatnerCohortsMeasurableValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patner_cohorts_measurable_values', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('farmer_id');
			$table->integer('patner_measurable_id');
			$table->string('value');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patner_cohorts_measurable_values');
	}

}
