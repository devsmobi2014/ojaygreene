<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFarmers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('farmers', function (Blueprint $table) {
			$table->integer('language')->default(0)->after('subscribed');
			$table->integer('farmer_group')->default(0)->after('language');
			$table->integer('constituency')->default(0)->after('farmer_group');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('farmers', function(Blueprint $table)
		{
			$table->dropColumn('language');
			$table->dropColumn('farmer_group');
			$table->dropColumn('constituency');


		});
	}

}
