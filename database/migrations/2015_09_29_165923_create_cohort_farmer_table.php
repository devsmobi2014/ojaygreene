<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCohortFarmerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('cohort_farmers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('farmer_id')->unsigned()->index();
            $table->foreign('farmer_id')->references('id')->on('farmers')->onDelete('cascade');
            $table->integer('cohort_id')->unsigned()->index();
            $table->foreign('cohort_id')->references('id')->on('cohorts')->onDelete('cascade');
            $table->string('expected_yield')->nullable();
            $table->string('projected_yield')->nullable();
            $table->string('actual_yield')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('cohort_farmers');
	}

}
