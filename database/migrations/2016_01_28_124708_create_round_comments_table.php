<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('round_comments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('round_id')->unsigned();
            $table->foreign('round_id')->references('id')->on('rounds');
            $table->integer('user_id');
            $table->string('comment');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('round_comments');
	}

}
