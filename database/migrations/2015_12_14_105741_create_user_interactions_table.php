<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInteractionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('user_interactions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('phone', 45);
            $table->integer('farmer_id');
            $table->integer('message_id');
            $table->string('direction', 10);
            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_interactions');
	}

}
