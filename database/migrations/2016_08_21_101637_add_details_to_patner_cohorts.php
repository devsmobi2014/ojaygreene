<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailsToPatnerCohorts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('patner_cohorts', function (Blueprint $table) {
			$table->integer('department_id')->nullable()->after('value_chain_id');
			$table->integer('region_id')->nullable()->after('department_id');


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('patner_cohorts', function (Blueprint $table) {
			$table->dropColumn('department_id');
			$table->dropColumn('region_id');


		});
	}

}
