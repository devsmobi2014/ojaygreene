<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscribeToFarmers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('farmers', function (Blueprint $table) {
			$table->integer('subscribed')->default(0)->after('farmer_id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('farmers', function(Blueprint $table)
		{
			$table->dropColumn('subscribed');

		});
	}

}
