$(document).ready(function() {
    $('#messages_per_region').DataTable();
    $('#harvest').DataTable();
    $('#farmers_selected').DataTable();


    if($('.table').find('.btn-delete')){
        //alert('button found there');
    }
    $('.delete').on('click',function(){
        if(confirm("Are you sure you want to delete?")){
            return true;
        }
        return false;
    });
    $('.delete-cohort').on('click',function(){
        if(confirm("Are you sure you want to close this cohort?")){
            return true;
        }
        return false;
    });
    $('.activate-cohort').on('click',function(){
        if(confirm("Are you sure you want to activate this cohort?")){
            return true;
        }
        return false;
    });

    $('#farmer_type2').on('click',function(){
        $('#farmers_select').show();
        $('#all_farmers').hide();
    })
    $('#farmer_type1').on('click',function(){
        $('#farmers_select').hide();
        $('#all_farmers').show();
    })
    var selected = [];

    $('#farmers_selected tbody').on('click', '.choose', function () {


        var id = this.value;

        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
        console.log(selected)


        $(this).toggleClass('selected');
    } );

    $('#subr').on('click',function(){

        $('#farmers').val(selected);



    });


});