<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{


    protected $fillable =
        [
            'name',
            'description',
            'image',
            'unit_of_measurement'
        ];

}
