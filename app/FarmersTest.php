<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmersTest extends Model {

    //
    protected $table = 'farmers_test';



    protected $fillable =
        [
            'name',
            'phone',
            'acreage',
            'gender',
            'region_id'
        ];


}
