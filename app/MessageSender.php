<?php namespace App;

use App\Http\Controllers\NotifyController;

class MessageSender  {

    //

    public static function send($to,$message){
        
        $to='254'.substr($to,-9);

        $notify_controller=new NotifyController();
        $result=$notify_controller->sendMessage($to,$message);

        
        if($result){
            return $result;
        }else{
            return false;
        }


    }
    



}
