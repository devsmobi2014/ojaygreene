<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RoundFarmer extends Model {

    //
    protected $fillable =
        [
            'farmer_id',
            'round_id',
            'expected_yield',
            'projected_yield',
            'actual_yield'
        ];



}
