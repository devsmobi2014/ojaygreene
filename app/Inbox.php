<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Support\Facades\DB;

class Inbox extends Model {

    //
    protected $table = 'inbox';
    protected $fillable =
        [
            'phone',
            'response',
            'farmer_id'
        ];
    public static function inbox(){
        if(Sentry::getUser()->hasAccess('agronomist')){

            $user_id=Sentry::getUser()->id;
            $count= DB::table('user_regions') ->select([
                'inbox.id',
                'inbox.phone',
                'inbox.response',
                'inbox.created_at',
                'farmers.name as name',
                'rounds.name as round_name',
                'regions.name as region_name',
            ])
                ->leftJoin('regions','regions.id','=','user_regions.region_id')
                ->leftJoin('farmers','regions.id','=','farmers.region_id')
                ->leftJoin('inbox', 'inbox.farmer_id', '=', 'farmers.id')
                ->leftJoin('round_farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
                ->leftJoin('rounds', 'round_farmers.round_id', '=', 'rounds.id')
                ->where('user_regions.user_id','=',$user_id)
                ->where('inbox.status','=',0)->count();


        }else{
            $count=Inbox::whereStatus(0)->get()->count();
        }


        return $count;
    }
}
