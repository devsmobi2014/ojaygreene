<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmer extends Model {

    //
    protected $fillable =
        [
            'name',
            'phone',
            'acreage',
            'gender',
            'region_id'
        ];


}
