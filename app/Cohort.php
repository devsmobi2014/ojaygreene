<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cohort extends Model
{


    protected $fillable =
        [
            'name',
            'round_id',
            'start_date',
            'end_date'
        ];

}
