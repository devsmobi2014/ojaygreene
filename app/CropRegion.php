<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CropRegion extends Model
{

    protected $table = 'crop_region';
    protected $fillable =
        [
            'crop_id',
            'region_id',
            'duration',
        ];

}
