<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Round extends Model {

    //
    protected $fillable =
        [
            'name',
            'region_crop_id',
            'start_date',
            'harvest_date'
        ];
    public function farmers()
    {
        return $this->hasMany('App\Cohorts');
    }



}
