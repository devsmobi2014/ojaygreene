<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cohort_message extends Model
{


    protected $fillable =
        [
            'title',
            'message',
            'cohort_id',
        ];

}
