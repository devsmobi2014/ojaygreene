<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Outbox extends Model {

    //
    protected $table = 'outbox';
    protected $fillable =
        [
            'phone',
            'farmer_id',
            'message',
            'type',
            'message_id',
            'cohort_id',
            'status'

        ];
}
