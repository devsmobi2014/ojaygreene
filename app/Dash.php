<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Dash extends Model {

	protected $table = 'testbed';

    protected $fillable = [
        'extension',
        'url',
        'default_message'
    ];

    protected $hidden = ['extension'];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }



}
