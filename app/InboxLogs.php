<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class InboxLogs extends Model {

    //
    protected $table = 'inbox_logs';
    protected $fillable =
        [
            'response'

        ];
}
