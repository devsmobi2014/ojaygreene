<?php namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User_patners;
use App\ValueChain;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ValuechainController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		if(\Sentry::getUser()->hasAccess('patner_admin') ||\Sentry::getUser()->hasAccess('admin')){

			$value_chains=ValueChain::leftJoin('departments','departments.id','=','value_chains.department_id')
				->where('value_chains.patner_id',$partner_id)
				->select('value_chains.*','departments.names as department_name')->get();
		}elseif(\Sentry::getUser()->hasAccess('partner_user')){

			$user_id=Sentry::getUser()->id;
			$user_patner=User_patners::where('user_id',$user_id)->first();

			$value_chains=ValueChain::leftJoin('departments','departments.id','=','value_chains.department_id')
				->where('value_chains.department_id',$user_patner->department_id)
				->select('value_chains.*','departments.names as department_name')->get();



		}


		return view('value-chains.index',compact('value_chains'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$departments=Department::where('patner_id',$partner_id)->get();
		return view('value-chains.create',compact('departments'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
		$v = Validator::make($request->all(), [
			'name' => 'required',
			'description'=>'required',
		]);
		if($v->fails()){
			return redirect()->back()->withErrors($v->errors());
		}
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$value_chain=new ValueChain();
		$value_chain->name=$request->get('name');
		$value_chain->patner_id=$partner_id;
		$value_chain->department_id=$request->get('department_id');
		$value_chain->description=$request->get('description');
		$value_chain->save();
		return Redirect::to('value-chains');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		if(!$id){
			return Redirect::to('value-chains');
		}
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$departments=Department::where('patner_id',$partner_id)->get();
		$value_chain=ValueChain::find($id);
		
		return view('value-chains.edit',compact('departments','value_chain'));
	}
	
	public function storeUpdate(Request $request){
		$value_chain=ValueChain::find($request->get('value_chain_id'));
		$value_chain->name=$request->get('name');
		$value_chain->department_id=$request->get('department_id');
		$value_chain->description=$request->get('description');
		$value_chain->save();
		return Redirect::to('value-chains');

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$value_chain=ValueChain::find($id);
		$value_chain->delete();
		return Redirect::to('value-chains');

	}

}
