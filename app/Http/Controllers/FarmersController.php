<?php namespace App\Http\Controllers;

use App\County;
use App\ExcelExport;
use App\Farmer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MessageSender;
use App\PatnerRegions;
use App\Region;
//use Cartalyst\Sentry\Facades\CI\Sentry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use yajra\Datatables\Facades\Datatables;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

class FarmersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
	public function index()
	{
		//


        return view('farmers/index');

	}
    public function getFarmers(){
       if(Sentry::check() && Sentry::getUser()->hasAccess('agronomist')){
           $user_id=Sentry::getUser()->id;
           $user_region=DB::table('user_regions')->where('user_id','=',$user_id)->first();

           $farmers=DB::table('user_regions')
               ->select(['farmers.id', 'farmers.name as farmer_name', 'farmers.phone', 'farmers.gender', 'farmers.acreage', 'regions.name'])
               ->leftJoin('regions','regions.id','=','user_regions.region_id')
               ->leftJoin('farmers','regions.id','=','farmers.region_id')
               ->where('user_regions.user_id','=',$user_id)
               ->orderBy('farmers.created_at', 'desc');


        }else{
           $farmers = Farmer::join('regions', 'regions.id', '=', 'farmers.region_id')
               ->orderBy('farmers.created_at', 'desc')
               ->select(['farmers.id', 'farmers.name as farmer_name', 'farmers.phone', 'farmers.gender', 'farmers.acreage', 'regions.name']);

       }


        return \yajra\Datatables\Datatables::of($farmers)->make(true);
    }
    public function getPartnerFarmers(){
        $partner_id=session('patner_id');
        if(!$partner_id){
            return redirect('logout');
        }
        $farmers = PatnerRegions::join('farmers', 'farmers.region_id', '=', 'patner_regions.region_id')
            ->leftJoin('regions','regions.id','=','patner_regions.region_id')
            ->orderBy('farmers.created_at', 'desc')
            ->select(['farmers.id', 'farmers.name as farmer_name', 'farmers.phone', 'farmers.gender', 'farmers.acreage', 'regions.name as region_name'])->get();
       

        return view('patner-farmers.index',compact('farmers'));
    }
    public function addFarmersToPatners(){
        $partner_id=session('patner_id');
        if(!$partner_id){
            return redirect('logout');
        }
    }
    public function addPartnerFarmers(){

    }

    public function massUpload(){
        $excels =DB::table('excel')
            ->leftJoin('farmers','farmers.id','=','farmers.excel_id')
            ->select([
                'excel.id',
                'excel.title',
                'excel.name',
                'excel.total',

            ])
            ->orderBy('id','desc')

            ->paginate();

        return view('farmers/mass-upload',compact('excels'));

    }
    public function getExcelFarmers(){
//        $farmers = Excel::join('regions', 'regions.id', '=', 'farmers.region_id')
//            ->leftJoin('excel','excel.id',='farmers.excel_id');
//            ->orderBy('farmers.created_at', 'desc')
//            ->select(['farmers.id', 'farmers.name as farmer_name', 'farmers.phone', 'farmers.gender', 'farmers.acreage', 'regions.name']);
//        return \yajra\Datatables\Datatables::of($farmers)->make(true);
    }
    public function uploadFarmers(){
        return view('farmers/upload_farmers');
    }
    public function  storeUpload(Request $request){

        $v = Validator::make($request->all(), [
            'title' => 'required',
            'excel'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }


        $extension = $request->file('excel')->getClientOriginalExtension(); // getting the file extension

        $fileName = rand(11111,99999).'.'.$extension; // renaming the file

        $request->file('excel')->move('storage',$fileName);
        $title=$request->get('title');

        // move it to the storage folder in the public assets


        \Maatwebsite\Excel\Facades\Excel::load('storage/'.$fileName, function($reader) use($fileName,$title)  {


            $results = $reader->skip(0)->toObject();


            $excel_upload = new ExcelExport();
            $excel_upload->name='storage'.$fileName;
            $excel_upload->total=count($results);
            $excel_upload->title=$title;
            $excel_upload->save();

            $id=$excel_upload->id;




            $results->each(function($sheet) use($id)  {




                $region=Region::where('name','like',"%".$sheet->region."%")->first();

                if($region){
                    $region_id=$region->id;
                }else{
                    $region_new=new Region();
                    $region_new->name=$sheet->region;
                    $region_new->save();
                    $region_id=$region_new->id;

                }
                $farmer=Farmer::wherePhone("0".$sheet->phone)->first();
                if(!$farmer){
                    $farmer =new Farmer();
                    $farmer->name=$sheet->name;
                    $farmer->phone="0".$sheet->phone;
                    $farmer->acreage=$sheet->acreage;
                    if(strtolower($sheet->gender=="male")){
                        $farmer->gender=1;
                    }else{
                        $farmer->gender=2;

                    }

                    $farmer->region_id=$region_id;
                    $farmer->registration_status=0;
                    $farmer->notification_status=1;
                    $farmer->excel_id=$id;

                    $farmer->save();
                   //Farmers $message="Tuma ujumbe ufuatayo Farmdrive-Ojaygreene Kwa 0703671850 alafu ujibu maswali vile ilivyoelezwa ili kujisajili katika Farmdrive na OjayGreene.";
                    //MessageSender::send($farmer->phone,$message);
                }


            });


        });

        return Redirect::to('farmers/upload-farmers')->with('message','Data uploaded successfully');

    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        $partner_id=0;

        $partner_id=session('patner_id');



        if($partner_id){

            return view('farmers/create',compact('partner_id'));
        }else{
            return view('farmers/create_farmer');
        }

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//get the requests
		$v = Validator::make($request->all(), [
        'name' => 'required',
        'phone_no' => 'required',
        ]);
		if($v->fails()){
			return redirect()->back()->withErrors($v->errors());
		}
        $region=Region::where('name','like',"%".$request->get('region')."%")->first();



        if($region){
            $region_id=$region->id;
        }else{

            $region=new Region();
            $region->name=$request->get('region');
            $region->save();

            $region_id=$region->id;


        }


        $phone="0".substr(trim($request->get('phone_no')),-9);
        $farm=Farmer::where('phone',$phone)->first();




        if($farm){
            if(session('patner_id')){
                return Redirect::to('patner-farmers')->with('message', 'Farmer already added');
            }else{
                return Redirect::to('farmers')->with('message', 'Farmer already added');
            }
        }

        $farmer =new Farmer();
        $farmer->name=$request->get('name');
        $farmer->phone=$request->get('phone_no');
        $farmer->region_id=$region_id;
        $farmer->save();


        if(session('patner_id')){
            return Redirect::to('patner-farmers')->with('message', 'Successfully added a farmer');
        }else{
            return Redirect::to('farmers')->with('message', 'Successfully added a farmer');
        }

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        $farmer=Farmer::find($id);
        $regions=Region::all();
        return view('farmers/edit',compact('farmer','regions'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'phone' => 'required',
            'acreage'=>'required',
            'gender'=>'required',
            'region_id'=>'required'
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }
        $farmer=Farmer::find($id);

        $input = $request->all();


        $farmer->fill($input)->save();
        return Redirect::to('/farmers')->with('message', 'Farmer updated successful.');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        try {
            $farmer = Farmer::findOrFail($id);
        } catch (ModelNotFoundException $mnfe) {
            return Redirect::to('farmers')->with('message', 'Sorry, farmer not found');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $farmer->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return Redirect::to('farmers')->with('message', 'Successfully deleted farmer');
	}

    public function addRegions(){
        $counties=County::all();
        
        return view('patner-farmers/add_regions',compact('counties'));
    }
    public function storeActiveRegions(Request $request){
        $v = Validator::make($request->all(), [

            'name'=>'required'
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }

        $exploded_array=explode(' ',$request->get('name'));

        

    }
    public function ImportFarmers(){


        return view('patner-farmers.import-farmers');

    }
    public function storeExcelFarmers(Request $request){

        $v = Validator::make($request->all(), [
            'excel'=>'required',
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }


        $extension = $request->file('excel')->getClientOriginalExtension(); // getting the file extension

        $fileName = rand(11111,99999).'.'.$extension; // renaming the file

        $request->file('excel')->move('storage',$fileName);


        // move it to the storage folder in the public assets


        \Maatwebsite\Excel\Facades\Excel::load('storage/'.$fileName, function($reader) use($fileName)  {


            $results = $reader->skip(0)->toObject();


//
//            $excel_upload = new ExcelExport();
//            $excel_upload->name='storage'.$fileName;
//            $excel_upload->total=count($results);
//            $excel_upload->title=$title;
//            $excel_upload->save();

//            $id=$excel_upload->id;
            $partner_id=session('patner_id');
            $default_region=PatnerRegions::where('patner_id',$partner_id)->where('default_region',1)->first();
            $default_region_id=$default_region->region_id;


            $results->each(function($sheet) use($default_region_id)  {




                $region=Region::where('name','like',"%".$sheet->region."%")->first();

                if($region){
                    $region_id=$region->id;
                }else{

                    $region_id=$default_region_id;

                }
                $farmer=Farmer::wherePhone("0".$sheet->phone)->first();
                if(!$farmer){
                    $farmer =new Farmer();
                    $farmer->name=$sheet->name;
                    $farmer->phone="0".$sheet->phone;
                    $farmer->acreage=$sheet->acreage;
                    if(strtolower($sheet->gender=="male")){
                        $farmer->gender=1;
                    }else{
                        $farmer->gender=2;

                    }

                    $farmer->region_id=$region_id;
                    $farmer->registration_status=0;
                    $farmer->notification_status=1;
                    $farmer->save();
                    //Farmers $message="Tuma ujumbe ufuatayo Farmdrive-Ojaygreene Kwa 0703671850 alafu ujibu maswali vile ilivyoelezwa ili kujisajili katika Farmdrive na OjayGreene.";
                    //MessageSender::send($farmer->phone,$message);
                }


            });


        });

        return Redirect::to('patner-farmers')->with('message','Data uploaded successfully');
    }

}
