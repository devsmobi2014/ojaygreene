<?php namespace App\Http\Controllers;

use App\Cohort;
use App\Cohort_farmer;
use App\Cohort_message;
use App\Events\MessageWasSent;
use App\Farmer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Outbox;
use App\Round;
use App\Round_farmer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CohortsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
	public function index($id)
	{
		//
        $activities=Cohort::where('round_id',$id)->get();

        return view('cohorts/index',compact('activities','id'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		//

        return view('cohorts/create',compact('id'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'start_date'=>'required',
            'round_id'=>'required',
            'end_date'=>'required'
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        //check if the region exists
        $cohort_details = $request->all();


        Cohort::create($cohort_details);


        return Redirect::to('rounds/activities/'.$request->get('round_id'))->with('message', 'Successfully added region');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $cohort=Cohort::find($id);
        $cohort_messages=DB::table('cohort_messages')->where('cohort_id','=',$id)->get();


        return view('cohorts/view',compact('cohort','cohort_messages'));
	}

    public function add($id){
         $crop_region=self::getRegion($id);
         $cohort=Cohort::find($id);
         if($crop_region){
             $farmers=DB::table('region_crop_farmers')
                 ->join('farmers', 'region_crop_farmers.farmer_id', '=', 'farmers.id')
                 ->where('region_crop_farmers.crop_region_id','=',$crop_region->id)
                 ->select('farmers.id as farmer_id','farmers.name','farmers.phone','farmers.acreage', 'region_crop_farmers.crop_region_id')
                 ->get();

         }

        return view('cohorts/add',compact('cohort','farmers'));
    }
    public function addfarmer($id,$cohort_id){

        $data['farmer_id']=$id;
        $data['cohort_id']=$cohort_id;
        $search =DB::table('cohort_farmers')->where('cohort_id','=',$cohort_id)->where('farmer_id','=',$id)->first();
        if($search){
            return Redirect::to('cohorts/view/'.$cohort_id)->with('message', 'Successfully added farmer');
        }else{
            $result=DB::table('cohort_farmers')->insert($data);
            if($result){
                return Redirect::to('cohorts/view/'.$cohort_id)->with('message', 'Successfully added farmer');
            }
        }


    }
    public function getRegion($id){
        $cohort=Cohort::find($id);
        $round=Round::find($cohort->round_id);
        $crop_region= DB::table('crop_region')->where('id', $round->region_crop_id)->first();
        return $crop_region;
    }
    public function editfarmer($id){
        $farmer=Farmer::find($id);
        $cohort_farmer=DB::table('round_farmers')->where('farmer_id', $farmer->id)->first();

        return view('cohorts/edit-farmer',compact('farmer','cohort_farmer'));
    }
    public function storeFarmerDetails(Request $request){

        if($request->all()){
            $cohort_region=Cohort_farmer::find($request->get('cohort_farmer_id'));
            if($cohort_region){
                $cohort_region->expected_yield=$request->get('expected_yield');
                $cohort_region->actual_yield=$request->get('actual_yield');
                $cohort_region->projected_yield=$request->get('projected_yield');
                $cohort_region->save();
                $farmer=Farmer::find($request->get('farmer_id'));
                if($farmer){
                    $farmer->acreage=$request->get('acreage');
                    $farmer->save();
                }
                return view('cohorts/edit-farmer',compact('farmer','cohort_farmer'));
            }
        }

    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        $cohort=Cohort::find($id);
        return view('cohorts/edit',compact('cohort'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update( $id,Request $request)
	{
		//
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'start_date'=>'required',
            'round_id'=>'required',
            'end_date'=>'required'
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $cohort=Cohort::find($id);
        $input = $request->all();

        $cohort->fill($input)->save();

        return Redirect::to('rounds/activities/'.$request->get('round_id'))->with('message', 'Activity updated.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        try {
            $cohort = Cohort::findOrFail($id);
        } catch (ModelNotFoundException $mnfe) {
            return redirect()->back();
        }

        $cohort->delete();

        return redirect()->back();
	}
    public function addMessage($id){
        return view('cohorts/add-message',compact('id'));

    }
    public function storeMessage(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:45',
            'cohort_id'=>'required',
            'message'=>'required'
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $data['title']=$request->get('title');
        $data['cohort_id']=$request->get('cohort_id');
        $data['message']=$request->get('message');
        $data['created_at']=Carbon::now()->toDateTimeString();
        $data['updated_at']=Carbon::now()->toDateTimeString();
        $result=DB::table('cohort_messages')->insert($data);
        if($result){
            return Redirect::to('cohorts/view/'.$data['cohort_id'])->with('message', 'Added messages to the cohort');
        }
    }
    public function editMessage($id){
        $message=DB::table('cohort_messages')->where('id',$id)->first();

        return view('cohorts/edit-message',compact('message'));


    }
    public function updateMessage($id,Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:45',
            'message'=>'required',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $message=Cohort_message::find($id);
        $input = $request->all();

        $message->fill($input)->save();

        return Redirect::to('cohorts/view/'.$message->cohort_id)->with('message', 'Message updated.');
    }
    public function removeMessage($id){
        try {
            $cohort_message = Cohort_message::findOrFail($id);
        } catch (ModelNotFoundException $mnfe) {
            return redirect()->back();
        }

        $cohort_message->delete();

        return redirect()->back();
    }
    public function sendMessage($id){
        $message=Cohort_message::find($id);

        if($message){
            $cohort=Cohort::find($message->cohort_id);
            $farmers=DB::table('round_farmers')
                ->join('farmers', 'round_farmers.farmer_id', '=', 'farmers.id')
                ->where('round_farmers.round_id','=',$cohort->round_id)
                ->select('round_farmers.id','farmers.id as farmer_id','farmers.name','farmers.phone','farmers.acreage', 'farmers.region_id','round_farmers.expected_yield','round_farmers.projected_yield','round_farmers.actual_yield')
                ->get();

            if($farmers){
                $no_farmers=count($farmers);
                foreach($farmers as $farmer){
                    $data['phone']=$farmer->phone;
                    $data['farmer_id']=$farmer->farmer_id;
                    $data['message']=self::appendName($message->message,$farmer->name);
                    $data['type']="activity";
                    $data['cohort_id']=$message->cohort_id;
                    $data['message_id']=$message->id;
                    $res=Outbox::create($data);
                }
            }

            $messages=Outbox::where('status','=',0)->orderBy('id','desc')->take($no_farmers)->get();



            if(Event::fire(new MessageWasSent($messages))){
                $message->status=1;
            }else{
                $message->status=2;
            }
            $message->save();


            return redirect()->back();

        }
    }
    public function appendName($message,$name){
        $exploded_name=explode(' ',$name);
        $message=str_replace('{{name}}',$exploded_name[0],$message);
        return $message;
    }



}
