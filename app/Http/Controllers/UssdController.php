<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Dash;

class UssdController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		error_reporting(0);
		header('Content-type: text/plain');
		set_time_limit(100);

		//get inputs
		$sessionId   = $_REQUEST["sessionId"];
		$serviceCode = $_REQUEST["serviceCode"];
		$phoneNumber = $_REQUEST["phoneNumber"];
		$text        = $_REQUEST["text"];   //*384*Leonard#   58762*27394873#

		$exploded_text= '';
		if(!empty($text)){
			//getExtension URL
			$exploded_text = explode("*", $text,2);
			$url = self::getSubURL($exploded_text[0]);

		}else{

			$exploded_text[1]=$text;
			$url[0] = "http://45.55.146.220/ussd";
			$url[1] = "Thanks for using Inukapap";
		}
		if($exploded_text[0] < 5){
			$exploded_text[1] = $text;
		}

		$response = self::sendRequest($phoneNumber,$sessionId,$url[0],$exploded_text[1],$serviceCode);
		header_remove();

		if ((substr(strtolower(trim($response)), 0, 3) == 'end') || (substr(strtolower(trim($response)), 0, 3) == 'con')) {
			header('Content-type: text/plain');

			echo $response;
			exit ;

		}else{
			header('Content-type: text/plain');
			//echo "END ".$response;

			echo "END ".$url[1];
			exit ;
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function sendRequest($phone,$session_id,$url,$text,$serviceCode)
	{
		//$url     = $_SERVER['HTTP_HOST'] . "/Sms/api";
		$qry_str = "?phoneNumber=" . trim($phone) . "&text=" . urlencode($text) . "&sessionId=" . $session_id."&serviceCode=".$serviceCode;
		$ch      = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url . $qry_str);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, '3');
		$content = trim(curl_exec($ch));
		curl_close($ch);

		return $content;
	}

	public function getSubURL($choice){
		$url = array();

		$testbed = Dash::where('extension',$choice)->first();

		if($testbed) {

			$url[0] = $testbed->url;
			$url[1] = $testbed->default_message;

		}else{

			$url[0] = "http://45.55.146.220/ussd";
			$url[1] = "Thanks you for using Inuka PAP";

		}
		return $url;
	}


}
