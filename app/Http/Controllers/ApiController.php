<?php namespace App\Http\Controllers;

use App\FarmDriveLogs;
use App\Farmer;
use App\FarmersTest;
use App\FarmerTest;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Inbox;
use App\InboxLogs;
use App\MessageSender;
use App\Outbox;
use App\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $raw=file_get_contents('php://input');
        if(!$raw){
            $data['success']=false;
            return response()->json($data);
            exit;
        }
        $logs=new FarmDriveLogs();
        $logs->response=$raw;
        $logs->save();

        $content=$json = json_decode($raw);
        $farmer=new Farmer();
        $region=Region::where('name','like',$content->region)->first();
        if($region){
            $region_id=$region->id;
        }else{
            $region_new=new Region();
            $region_new->name=$content->region;
            $region_new->save();
            $region_id=$region_new->id;

        }
        $phone='0'.substr($content->phone_number,-9);
        $exists=Farmer::where('phone','=',$phone)->first();
        if(!$exists){
            $farmer->phone=$phone;
            $farmer->name=$content->full_name;
            $farmer->gender=$content->gender;
            $farmer->acreage=$content->land_size;
            $farmer->dob=$content->dob;
            $farmer->region_id=$region_id;
            $farmer->farmer_id=$content->farmer_id;
            //$res=$farmer->save();
            try{
                $res=$farmer->save();
                if($res){
                    $data['success']=true;
                }else{
                    $data['success']=false;
                }
            }catch (ModelNotFoundException $mnfe){
                $data['success']=false;
            }
        }else{
            $exists->name=$content->full_name;
            $exists->gender=$content->gender;
            $exists->acreage=$content->land_size;
            $exists->dob=$content->dob;
            $exists->region_id=$region_id;
            $exists->farmer_id=$content->farmer_id;
            //$data['success']=true;
            try{
                $res=$exists->save();
                if($res){
                    $data['success']=true;
                }else{
                    $data['success']=false;
                }
            }catch (ModelNotFoundException $mnfe){
                $data['success']=false;
            }
        }

        return response()->json($data);

	}
    public function reciever(){

        $raw=file_get_contents('php://input');
        if(!$raw){
            $data['success']=false;
            return response()->json($data);
            exit;
        }
        $logs=new InboxLogs();
        $logs->response=$raw;
        $logs->save();
        $response=json_decode($raw);
        if($response){
            $phone='0'.substr($response->phone_number,-9);

            $farmer=Farmer::where('phone',$phone)->first();
            if($farmer){
                $inbox=new Inbox();
                $inbox->phone=$phone;
                $inbox->response=$response->message;
                $inbox->farmer_id=$farmer->id;
                $inbox->save();
                //add to user interactions
                $user_interaction=[];
                $user_interaction['phone']=$phone;
                $user_interaction['farmer_id']=$farmer->id;
                $user_interaction['message_id']=$inbox->id;
                $user_interaction['direction']='in';

                DB::table('user_interactions')->insert($user_interaction);

                $data['success']=true;
                return response()->json($data);
                exit;
            }

        }else{
            $data['success']=false;
            return response()->json($data);
            exit;
        }

    }
    public function syncReceiver(Request $request){


        if ($request->isMethod('post')) {
            $raw=json_encode($request->all());
            //
            $message=$request->get('message');
            $phone=$request->get('from');
            
            $logs=new InboxLogs();
            $logs->response=$raw;
            $logs->save();
            self::storeInbox($phone,$message);
        }else{
            $task=$request->get('task');
            if($task=='send'){


                $data['payload']['secret']="qwa12ss";
                $data['payload']['task']="send";

                $dat['to']='+254710893801';
                $dat['message']='Testing sms sync';
                $dat['uuid']="042b3515-ef6b-f424-c4qd";

                $data['payload']['messages'][]=$dat;
                $encodes=json_encode($data);
                //self::send_response($encodes);
            }
        }


    }
    public function storeInbox($phone,$message){
        $phone='0'.substr($phone,-9);

        $farmer=Farmer::where('phone',$phone)->first();

        if($farmer){

            $inbox=new Inbox();
            $inbox->phone=$phone;
            $inbox->response=$message;
            $inbox->farmer_id=$farmer->id;
           // $inbox->save();
            //add to user interactions
            $user_interaction=[];
            $user_interaction['phone']=$phone;
            $user_interaction['farmer_id']=$farmer->id;
            $user_interaction['message_id']=$inbox->id;
            $user_interaction['direction']='in';

           // DB::table('user_interactions')->insert($user_interaction);

            $data['payload']['success']=true;
            $data['payload']['secret']='qwa12ss';
            $data['payload']['error']=null;

            self::send_response(json_encode($data)) ;
        }
    }
    public function send_response($response)
    {
        // Avoid caching
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header("Content-type: application/json; charset=utf-8");
        echo $response;
    }
    public function sender(){
        $messages=Outbox::where('status',0)->get();

        foreach($messages as $message){
            $res=MessageSender::send($message->phone,$message->message);
            $r=json_decode($res);
            if($r->success=="true"){
                $message->status=1;
            }else{
                $message->status=2;
            }
            $message->save();
        }
    }
    public function test(){
        $raw=file_get_contents('php://input');
         //echo $raw;
        if($raw){
            $logs=new FarmDriveLogs();
            $logs->response=$raw;
            $logs->save();

            ///decode the json into an object
            $content=$json = json_decode($raw);

            //instantiate the new test
            $farmer=new FarmersTest();
            //check if the region exists
            $region=Region::where('name','like',$content->region)->first();
            if($region){
                $region_id=$region->id;
            }else{
                $region_new=new Region();
                $region_new->name=$content->region;
                $region_new->save();
                $region_id=$region_new->id;

            }
           //format the phone number
            $phone='0'.substr($content->phone_number,-9);
            //Check if the farmer exists
            $exists=FarmersTest::where('phone','=',$phone)->first();

            if(!$exists){
                //Insert the farmer
                $farmer->phone=$phone;
                $farmer->name=$content->full_name;
                $farmer->gender=$content->gender;
                $farmer->acreage=$content->land_size;
                $farmer->dob=$content->dob;
                $farmer->region_id=$region_id;
                $farmer->farmer_id=$content->farmer_id;
                //$res=$farmer->save();
                try{
                    $res=$farmer->save();
                    if($res){
                        $data['success']=true;
                    }else{
                        $data['success']=false;
                    }
                }catch (ModelNotFoundException $mnfe){
                    $data['success']=false;
                }
            }else{
                //update the farmer
                $exists->name=$content->full_name;
                $exists->gender=$content->gender;
                $exists->acreage=$content->land_size;
                $exists->dob=$content->dob;
                $exists->region_id=$region_id;
                $exists->farmer_id=$content->farmer_id;
                //$data['success']=true;
                try{
                    $res=$exists->save();
                    if($res){
                        $data['success']=true;
                    }else{
                        $data['success']=false;
                    }
                }catch (ModelNotFoundException $mnfe){
                    $data['success']=false;
                }
            }

            return response()->json($data);
            //return $raw;

        }else{
            $data['success']=false;
            $data['error']="data not recieved";
           echo json_encode($data);
        }
        exit;
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
