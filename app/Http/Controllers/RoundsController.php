<?php namespace App\Http\Controllers;

//use App\Http\Requests;
use App\Crop;
use App\CropRegion;
use App\Events\Event;
use App\Events\MessageWasSent;
use App\Farmer;
use App\Http\Controllers\Controller;
use App\MessageSender;
use App\Outbox;
use App\Region;
use App\Round;
use App\Cohort;
use App\RoundFarmer;
use Carbon\Carbon;
use Cartalyst\Sentry\Facades\Native\Sentry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Redirect;

class RoundsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
	public function index($id)
	{
		//
        $crop_region=CropRegion::find($id);
        $crop=Crop::find($crop_region->crop_id);
        $region=Region::find($crop_region->region_id);
        $rounds=Round::where('region_crop_id','=',$id)->get();


        return view('rounds/index',compact('id','rounds','crop','region'));

	}
    public function all($id){
        $crop=Crop::find($id);

        return view('rounds/all',compact('id','crop'));
    }

    public function getRounds($id){

        $rounds=DB::table('crop_region')
            ->select([
                'rounds.id',
                'rounds.name',
                'rounds.start_date',
                'rounds.harvest_date',
                'rounds.status'

            ])

            ->join('rounds','crop_region.id','=','rounds.region_crop_id')
            ->where('crop_region.crop_id','=',$id);


        return \yajra\Datatables\Datatables::of($rounds)->make(true);
    }
    public function allNew(){
        $regions=Region::all();

        return view('rounds/all-new',compact('regions'));

    }
    public function storeCohort(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'regions'=>'required',
            'start_date'=>'required'
        ]);
        $data['name']=$request->get('name');
        $data['start_date']=$request->get('start_date');
        $data['harvest_date']=$request->get('harvest_date');
        $farmers_array=[];
        $regions_array=[];

        if($regions=$request->get('regions')){
            foreach($regions as $region){
                $reg=Region::find($region);
                $farmers=Farmer::whereRegionId($region)->get();
                if($farmers){
                   foreach($farmers as $farmer){
                       array_push($farmers_array,$farmer);
                   }
                }
                array_push($regions_array,$reg);
            }
        }

        //$json_data=json_encode($data);
        return view('rounds/select',compact('data','farmers_array','regions_array'));

    }
    public function addCohort(Request $request){
        if(empty($request->get('farmer_id'))) {
            // list is empty.

        }else{
            $round_details = $request->all();
            $round_details['status']=1;
            $round=Round::create($round_details);
            if($round){
                foreach($request->get('farmer_id') as $farmer_id){
                    $details['farmer_id']=$farmer_id;
                    $details['round_id']=$round->id;
                    RoundFarmer::create($details);
                }
                return Redirect::to('/all-new')->with('message',' Cohort  added successful');

            }else{
                return Redirect::to('/all-new')->with('message',' Cohort not added successful');
            }
        }

    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		//
        return view('rounds/create',compact('id'));
	}
    public function fetch(){

    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'region_crop_id'=>'required'
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        //check if the region exists
            $round_details = $request->all();


            Round::create($round_details);


        return Redirect::to('rounds/'.$request->get('region_crop_id'))->with('message', 'Successfully added region');
	}
    public function add($id){
        $round=Round::where('id','=',$id)->first();


        if($round){
            $farmers=DB::table('crop_region')
                ->join('farmers', 'crop_region.region_id', '=', 'farmers.region_id')
                ->join('regions', 'regions.id', '=', 'farmers.region_id')
                ->where('crop_region.id','=',$round->region_crop_id)
                ->select('farmers.id as farmer_id','farmers.name','farmers.phone','farmers.acreage', 'farmers.region_id','regions.name as region_name')
                ->get();
            $farmers_array=[];
            if($farmers){
                foreach($farmers as $farmer){
                    $farmer=(array) $farmer;

                    $cohorts=DB::table('round_farmers')
                        ->leftJoin('rounds','rounds.id','=','round_farmers.round_id')
                        ->select('rounds.name','rounds.id as round_id')
                        ->where('round_farmers.farmer_id','=',$farmer['farmer_id'])->get();
                    if($cohorts){
                        $cohorts=(array)$cohorts;
                        $farmer['cohorts']=$cohorts;
                    }else{
                        $cohorts="";
                        $farmer['cohorts']=$cohorts;
                    }
                    array_push($farmers_array,$farmer);
                }
            }


        }

        return view('rounds/add',compact('round','farmers_array'));
    }
    public function storeFarmers(Request $request){
        $validator = Validator::make($request->all(), [
            'round_id' => 'required|max:45',
            'farmer_id'=>'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $data=$request->all();
        $farmer_ids=explode(',',$data['farmer_id']);



        $round_id=$data['round_id'];
        foreach($farmer_ids as $farmer_id){

            $i=0;
            $exists=DB::table('round_farmers')->where('farmer_id', $farmer_id)->where('round_id', $round_id)->first();
            if(!$exists){
                $dat['farmer_id']=  $farmer_id;
                $dat['round_id' ]= $round_id;
                $dat['created_at']=Carbon::now()->toDateTimeString();
                $dat['updated_at']=Carbon::now()->toDateTimeString();

                $res=DB::table('round_farmers')->insert($dat);
                if($res){
                    $i++;
                }

            }

        }
        return Redirect::to('rounds/view/'.$round_id)->with('message', $i.' Farmers  added successful');

    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

        $round=Round::find($id);
        if($round->region_crop_id){
            $crop_region=CropRegion::find($round->region_crop_id);
            $crop=Crop::find($crop_region->crop_id);
            $region=Region::find($crop_region->region_id);
        }else{
            $crop_region='';
            $crop='';
            $region='';
        }


        $round_averages=DB::table('round_farmers')
                        ->select([
                            DB::raw('SUM(expected_yield) AS expected_yield'),
                            DB::raw('SUM(projected_yield) AS projected_yield'),
                            DB::raw('SUM(actual_yield) AS actual_yield'),
                            DB::raw('SUM(acreage) AS acreage'),

                        ])
                        ->where('round_id','=',$round->id)
                        ->first();




        $farmers=DB::table('round_farmers')
            ->join('farmers', 'round_farmers.farmer_id', '=', 'farmers.id')
            ->where('round_farmers.round_id','=',$round->id)
            ->select('round_farmers.id','farmers.id as farmer_id','farmers.name','farmers.phone','round_farmers.acreage', 'farmers.region_id','round_farmers.expected_yield','round_farmers.projected_yield','round_farmers.actual_yield')
            ->get();



        return view('rounds.view',compact('farmers','region','crop','round','round_averages'));

	}
    public function getCropsCohorts(){
        $crops=DB::table('crops')
               ->select([
                  'crops.id as id',
                  'crops.name',
                  DB::raw('count(rounds.region_crop_id) as no_of_rounds')
            ])
            ->leftJoin('crop_region','crop_region.crop_id','=','crops.id')
            ->leftJoin('rounds','crop_region.id','=','rounds.region_crop_id')
            ->groupBy('crops.id')
            ->get();



        return view('rounds/crop-cohorts',compact('crops'));
    }
    public function filterHarvestDates(){

        $default_week =self::getweek();


        $rounds=Round::where('harvest_date','=',$default_week)->get();


        return view('rounds/harvest-dates',compact('default_week','rounds'));
    }
    public function filterWeek(Request $request){
        $validator = Validator::make($request->all(), [
            'week' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $default_week =$request->get('week');


        $rounds=Round::where('harvest_date','=',$default_week)->get();


        return view('rounds/harvest-dates',compact('default_week','rounds'));
    }
    public function getweek()
    {

        $ddate = date('Y-m-d');
        $date = new \DateTime($ddate);
        $week = $date->format("W");
        return $week;
    }
    public function comments($id){
        $round=Round::find($id);

        $comments=DB::table('round_comments')
            ->leftJoin('users','users.id','=','round_comments.user_id')
            ->select('round_comments.id','round_comments.comment','round_comments.user_id','round_comments.created_at','users.first_name','users.last_name','users.username')
            ->where('round_id','=',$id)->orderBy('round_comments.id','desc')->get();

        return view('rounds/comments',compact('comments','round'));
    }
    public function newComment($id){
        $round=Round::find($id);
        return view('rounds/new-comment',compact('round'));
    }
    public function closeCohort($id,$state){
        if(!$id)
            return redirect()->back();

        $round=Round::find($id);
        if($state=="activate"){
            $round->status=1;
        }elseif($state=="close"){
            $round->status=0;
        }

        $round->save();
        return redirect()->back()->with('message','Cohort closed');

    }
    public function storeComment(Request $request){

        $validator = Validator::make($request->all(), [
            'comment' => 'required',
            'round_id'=>'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }


        $data['comment']=$request->get('comment');
        $data['round_id']=$request->get('round_id');
        $data['user_id']=$request->get('user_id');
        $data['created_at']=Carbon::now()->toDateTimeString();
        $data['updated_at']=Carbon::now()->toDateTimeString();

        DB::table('round_comments')->insert($data);
        return redirect()->back()->with('message','Comment added successfully');

    }
    public function deleteComment($id){


        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
         DB::table('round_comments')->where('id','=',$id)->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return redirect()->back();
    }
    public function activities($id){
        echo $id;
        exit;


    }
    public function sendMessage($phone,$round_id){
        $farmer=Farmer::wherePhone($phone)->first();
       return view('rounds/send-message',compact('farmer','round_id'));

    }
    public function send(Request $request){
        $farmer=Farmer::wherePhone($request->get('phone'))->first();
        $outbox=new Outbox();
        $outbox->phone=$farmer->phone;
        $outbox->farmer_id=$farmer->id;
        $outbox->message=$request->input('message');
        $outbox->type='compose';
        $outbox->save();
        $messages=Outbox::find($outbox->id);


        $messages_controller=new MessagesController();
         self::logMessages($messages);
        $this->sendTOFarmDrive($messages);
        $partner_id=session('patner_id');

        if($partner_id){

            return redirect('patner-cohort/view/'.$request->get('round_id'));
        }else{
            return back()->with('message', 'Message sent successful');
        }


       // return Redirect::to('rounds/send-message',['phone'=>$request->get('phone'),'round_id'=>$request->get('round_id')])->with('message', 'Message sent successful');
    }
    public function logMessages($message){

        $user_interaction['phone'] = $message->phone;
        $user_interaction['farmer_id'] = $message->farmer_id;
        $user_interaction['message_id'] = $message->id;
        $user_interaction['direction'] = 'out';
        $user_interaction['created_at']=Carbon::now()->toDateTimeString();
        $user_interaction['updated_at']=Carbon::now()->toDateTimeString();

        DB::table('user_interactions')->insert($user_interaction);

    }
    public function sendTOFarmDrive($message){

        $phone='254'.substr($message->phone,-9);

        $results=MessageSender::send($phone,$message->message);

        $msg=Outbox::find($message->id);
        foreach($results as $result) {

            if($result->status=="Success"){
                $msg->status=1;
            }else{
                $msg->status=2;
            }
            $msg->save();
        }

        return 1;
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $round=Round::find($id);

        return view('rounds/edit',compact('id','round'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'region_crop_id'=>'required'
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        //check if the region exists
        $round=Round::find($id);
        $input = $request->all();

        $round->fill($input)->save();


        return Redirect::to('rounds/'.$round->region_crop_id)->with('message', 'Successfully added region');
	}
    public function editfarmer($id){


        $round_farmer=RoundFarmer::find($id);
        $farmer=Farmer::find($round_farmer->farmer_id);

        return view('rounds/edit-farmer',compact('farmer','round_farmer'));
    }
    public function storeFarmerDetails(Request $request){

        if($request->all()){
            $round_farmer=RoundFarmer::find($request->get('id'));

            if($round_farmer){
                $round_farmer->expected_yield=$request->get('expected_yield');
                $round_farmer->actual_yield=$request->get('actual_yield');
                $round_farmer->projected_yield=$request->get('projected_yield');
                $round_farmer->acreage=$request->get('acreage');
                $round_farmer->save();
//                $farmer=Farmer::find($request->get('farmer_id'));
//                if($farmer){
//                    $farmer->acreage=$request->get('acreage');
//                    $farmer->save();
//                }
                return Redirect::to('rounds/view/'.$request->get('round_id'))->with('message', 'Successfully edited farmer');
            }
        }

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        try {
            $round = Round::findOrFail($id);
        } catch (ModelNotFoundException $mnfe) {
            return redirect()->back();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $round->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return redirect()->back();

	}
    public function removeFarmer($farmer_id,$round_id){
        try {
            $round_farmer = RoundFarmer::where('round_id',$round_id)->where('farmer_id',$farmer_id)->first();
        } catch (ModelNotFoundException $mnfe) {
            return redirect()->back();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $round_farmer->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return redirect()->back();
    }

}
