<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Patner;
use App\PatnerRegions;
use App\ValueChain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PatnerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('patners/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
		$v = Validator::make($request->all(), [
			'name' => 'required',
			'phone'=>'required',
			'description'=>'required',
			'category'=>'required'
		]);
		if($v->fails()){
			return redirect()->back()->withErrors($v->errors());
		}
		$patner=new Patner();
		$patner->names=$request->get('name');
		$patner->category=$request->get('category');
		$patner->phone=$request->get('phone');
		$patner->description=$request->get('description');
		$patner->save();
		return Redirect::to('dashboard/patner-intro');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		session(['patner_id'=>$id]);
		$patner=Patner::find($id);
		$no_farmers = PatnerRegions::join('farmers', 'farmers.region_id', '=', 'patner_regions.region_id')
			->leftJoin('regions','regions.id','=','patner_regions.region_id')
			->orderBy('farmers.created_at', 'desc')
			->select(['farmers.id', 'farmers.name as farmer_name', 'farmers.phone', 'farmers.gender', 'farmers.acreage', 'regions.name as region_name'])->count();
		$no_values_chains=ValueChain::where('patner_id',$id)->count();
		$no_messages=0;
		
		return view('patners/index',compact('partner','no_farmers','no_values_chains','no_messages'));
	}
	

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
