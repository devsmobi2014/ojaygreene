<?php namespace App\Http\Controllers;

use App\Crop;
use App\Farmer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\DashRequest;
use App\Http\Requests\EditRequest;
use App\Inbox;
use App\Outbox;
use App\Patner;
use App\User_patners;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Dash;
use Illuminate\Support\Facades\Redirect;

class DashController extends Controller {


    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function switcher(){


        if(\Sentry::getUser()->hasAccess('admin')){

            return Redirect::to('dashboard/patner-intro');

        }elseif(\Sentry::getUser()->hasAccess('patner_admin') || \Sentry::getUser()->hasAccess('partner_user') ){

            $user_id=Sentry::getUser()->id;
            $user_patner=User_patners::where('user_id',$user_id)->first();
            return Redirect::to('patner/view/'.$user_patner->patner_id);
        }else{

            return Redirect::to('dashboard');
        }
    }
    public function patnerIntro(){
        $patners=Patner::all();
        
        return view('dashboard.patner_intro',compact('patners'));
    }
	public function index(Request $request)
	{
        $request->session()->forget('patner_id');


        $farmers=Farmer::all()->count();
        $crops=Crop::all()->count();
        $messages_sent=Outbox::where('status',1)->count();
        $recieved_messages=Inbox::where('status',1)->count();
        $returned_data =self::getRegionsAcreage();

        $regions_acreage =$returned_data['data'];

        $total_acreage=$returned_data['acreage'];


        $registratios_per_month=self::getRegistrationsByMonth();
        $reg_per_month=json_encode($registratios_per_month['plotted_data']);
        $reg_cat=json_encode($registratios_per_month['categories']);
        $reg_cat = str_replace('"', '*', $reg_cat);
        $reg_cat = str_replace("*", "'", $reg_cat);

        $active_per_month=self::getActivePerMonth();

        $act_per_month=json_encode($active_per_month['plotted_data']);

        $act_cat=json_encode($active_per_month['categories']);
        $act_cat = str_replace('"', '*', $act_cat);
        $act_cat = str_replace("*", "'", $act_cat);


        $messages_per_region = DB::table('regions')->select([
            'regions.name',
            'regions.id',
            DB::raw('count(outbox.farmer_id) AS messages_out'),
            DB::raw('count(inbox.farmer_id) AS messages_in'),

        ])->leftJoin('farmers','farmers.region_id','=','regions.id')
            ->leftJoin('outbox','outbox.farmer_id','=','farmers.id')
            ->leftJoin('inbox','inbox.farmer_id','=','farmers.id')
            ->where('regions.status','=',1)
            ->groupBy('regions.id')->paginate(10);


		return view('dashboard.dashboard',compact('farmers',
            'crops',
            'messages_sent',
            'regions_acreage',
            'total_acreage',
            'harvest',
            'messages_per_region',
            'reg_per_month',
            'reg_cat',
            'act_per_month',
            'act_cat',
            'recieved_messages'
        ));
	}
    public function getRegionsAcreage(){
        $regions_acreage = DB::table('farmers')
            ->leftJoin('regions', 'regions.id', '=', 'farmers.region_id')
            ->select('region_id','regions.name', DB::raw('SUM(acreage) as acreage'))
            ->groupBy('regions.name')
            ->where('regions.status','=',1)
            ->get();
        $data=[];
        $total_acreage=DB::table('farmers')->select([
            DB::raw('sum(acreage) as acreage')
        ])->first();


        if($regions_acreage)
            foreach($regions_acreage as $region_acr){

                $reg=[];

                $reg['name']=$region_acr->name;

                $value=$region_acr->acreage/$total_acreage->acreage *100;
                $reg['y']=(int)$value;

                array_push($data,$reg);
            }


        $return_data['acreage']=$total_acreage->acreage;
        $return_data['data']=json_encode($data);
        return $return_data;

    }
    public function getCohorts(){
        $cohorts = DB::table('rounds')->select([
            'rounds.name',
            'rounds.id',
            'rounds.start_date',
            'rounds.harvest_date',
            DB::raw('SUM(round_farmers.expected_yield) AS expected_yield'),
            DB::raw('SUM(round_farmers.projected_yield) AS projected_yield'),
            DB::raw('SUM(round_farmers.actual_yield) AS actual_yield'),


        ])
            ->leftJoin('round_farmers','round_farmers.round_id','=','rounds.id')
            ->leftJoin('crop_region','rounds.region_crop_id','=','crop_region.id')
            ->leftJoin('regions','crop_region.region_id','=','regions.id')

          ->where('rounds.status','=',1)
          ->where('regions.status','=',1)
            ->groupBy('rounds.id');

        return \yajra\Datatables\Datatables::of($cohorts)->make(true);
    }
    public function getRegistrationsByMonth(){
        $registrations=DB::select(DB::raw("SELECT DATE_FORMAT(created_at,'%b')as month,count(farmer_id) as farmers FROM farmers WHERE date(farmers.created_at)>='2016-04-01'  GROUP BY YEAR(created_at),MONTH(created_at) "));
        $categories=[];
        $plotted_data=[];
        if($registrations){
            foreach($registrations as $disb){
                array_push($categories,$disb->month);
                array_push($plotted_data,(int)$disb->farmers);
            }
        }
        $data['plotted_data']=$plotted_data;
        $data['categories']=$categories;
        return $data;
    }
    public function getActivePerMonth(){
        $registrations=DB::select(DB::raw("SELECT DATE_FORMAT(created_at,'%b')as month,count(farmer_id) as farmers FROM round_farmers WHERE date(round_farmers.created_at)>='2016-04-01' GROUP BY YEAR(created_at),MONTH(created_at) "));
        $categories=[];
        $plotted_data=[];
        if($registrations){
            foreach($registrations as $disb){
                array_push($categories,$disb->month);
                array_push($plotted_data,(int)$disb->farmers);
            }
        }
        $data['plotted_data']=$plotted_data;
        $data['categories']=$categories;
        return $data;
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('dashboard.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param DashRequest $request
     * @return Response
     */
	public function store(DashRequest $request)
	{
		$dash_resource = new Dash($request->all());
        \Auth::user()->dash()->save($dash_resource);
        return redirect('dashboard');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$dash_resource = Dash::findOrFail($id);

        return view('dashboard.edit', compact('dash_resource'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param DashRequest|EditRequest $request
     * @return Response
     */
	public function update($id, EditRequest $request)
	{
		$dash_resource = Dash::findOrFail($id);
        $dash_resource -> update($request->all());

        return redirect('dashboard');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$dash_resource = Dash::findOrFail($id);
        $dash_resource -> delete();
        return redirect('dashboard');
	}

}
