<?php namespace App\Http\Controllers;

use App\Crop;
use App\CropRegion;
use App\Events\MessageWasSent;
use App\Farmer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Inbox;
use App\MessageSender;
use App\Outbox;
use App\Region;
use App\Round;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Validator;
use yajra\Datatables\Datatables;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

class MessagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
	public function index()
	{
        $count=Inbox::inbox();


        return view('messages.index',compact('messages'));
	}
    public function getInbox(){
        if(Sentry::check() && Sentry::getUser()->hasAccess('agronomist')){
            $user_id=Sentry::getUser()->id;

            $user_region=DB::table('user_regions')->where('user_id','=',$user_id)->first();
//            $messages=DB::table('inbox')
//                ->select([
//                    'inbox.id',
//                    'inbox.phone',
//                    'inbox.response',
//                    'inbox.created_at',
//                    'farmers.name as name',
//                    'rounds.name as round_name',
//                    'regions.name as region_name',
//                ])
//                ->leftJoin('farmers', 'farmers.id', '=', 'inbox.farmer_id')
//                ->leftJoin('round_farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
//                ->leftJoin('regions', 'farmers.region_id', '=', 'regions.id')
//                ->leftJoin('rounds', 'round_farmers.round_id', '=', 'rounds.id')
//                ->where('regions.id','=',$user_region->region_id)
//                ->orderBy('inbox.id', 'desc')
//                ->groupBy('inbox.id');

            $messages= DB::table('user_regions') ->select([
                        'inbox.id',
                        'inbox.phone',
                        'inbox.response',
                        'inbox.created_at',
                        'farmers.name as name',
                        'rounds.name as round_name',
                        'regions.name as region_name',
                    ])
                ->leftJoin('regions','regions.id','=','user_regions.region_id')
                ->leftJoin('farmers','regions.id','=','farmers.region_id')
                ->leftJoin('inbox', 'inbox.farmer_id', '=', 'farmers.id')
                ->leftJoin('round_farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
                ->leftJoin('rounds', 'round_farmers.round_id', '=', 'rounds.id')
                ->where('user_regions.user_id','=',$user_id)
                ->orderBy('inbox.id', 'desc')
                ->groupBy('inbox.id');
        }else{
            $messages=DB::table('inbox')
                ->select([
                    'inbox.id',
                    'inbox.phone',
                    'inbox.response',
                    'inbox.created_at',
                    'farmers.name as name',
                    'rounds.name as round_name',
                    'regions.name as region_name',
                ])
                ->leftJoin('farmers', 'farmers.id', '=', 'inbox.farmer_id')
                ->leftJoin('round_farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
                ->leftJoin('regions', 'farmers.region_id', '=', 'regions.id')
                ->leftJoin('rounds', 'round_farmers.round_id', '=', 'rounds.id')
                ->orderBy('inbox.id', 'desc')
                ->groupBy('inbox.id');
        }


            return \yajra\Datatables\Datatables::of($messages)->make(true);
            //return \yajra\Datatables\Datatables::of($messages)->make(true);

    }
    public function getSent(){

//        $messages=DB::table('outbox')
//            ->join('farmers', 'farmers.id', '=', 'outbox.farmer_id')
//            ->select('outbox.message','outbox.created_at','outbox.phone','farmers.name as name')
//            ->orderBy('outbox.id', 'desc');
       // return Datatables::of($messages)->make(true);
        if(Sentry::check() && Sentry::getUser()->hasAccess('agronomist')){
            $user_id=Sentry::getUser()->id;
            $user_region=DB::table('user_regions')->where('user_id','=',$user_id)->first();
            $messages=DB::table('outbox')
                ->select([
                    'outbox.id',
                    'outbox.message as message',
                    'outbox.uniq as uniq',
                    DB::raw('count(outbox.uniq) as farmers'),
                    'rounds.id as round_id',
                    'rounds.name as round_name',
                    'cohorts.name as cohort_name',
                    'regions.id as region_id',
                    'regions.name as region_name',
                    'crops.id as crop_id',
                    'crops.name as crop_name'
                ])
                ->leftJoin('cohorts', 'cohorts.id', '=', 'outbox.cohort_id')
                ->leftJoin('rounds', 'rounds.id', '=', 'cohorts.round_id')
                ->leftJoin('crop_region', 'crop_region.id', '=', 'rounds.region_crop_id')
                ->leftJoin('regions', 'regions.id', '=', 'crop_region.region_id')
                ->leftJoin('crops', 'crops.id', '=', 'crop_region.crop_id')
                ->where('regions.id','=',$user_region->region_id)
                ->orderBy('outbox.id','desc')
                ->groupBY('outbox.uniq');
        }else{
            $messages=DB::table('outbox')
                ->select([
                    'outbox.id',
                    'outbox.message as message',
                    'outbox.uniq as uniq',
                    DB::raw('count(outbox.uniq) as farmers'),
                    'rounds.id as round_id',
                    'rounds.name as round_name',
                    'cohorts.name as cohort_name',
                    'regions.id as region_id',
                    'regions.name as region_name',
                    'crops.id as crop_id',
                    'crops.name as crop_name'
                ])
                ->leftJoin('cohorts', 'cohorts.id', '=', 'outbox.cohort_id')
                ->leftJoin('rounds', 'rounds.id', '=', 'cohorts.round_id')
                ->leftJoin('crop_region', 'crop_region.id', '=', 'rounds.region_crop_id')
                ->leftJoin('regions', 'regions.id', '=', 'crop_region.region_id')
                ->leftJoin('crops', 'crops.id', '=', 'crop_region.crop_id')
                ->orderBy('outbox.id','desc')
                ->groupBY('outbox.id');

        }





        return \yajra\Datatables\Datatables::of($messages)->make(true);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}
    public function viewSent($uniq){

        if($uniq == "null"){
            return Redirect::to('messages/sent');
        }
        $farmers=DB::table('outbox')->select([
            'farmers.id',
            'farmers.phone',
            'farmers.name'

        ])->leftJoin('farmers','farmers.id','=','outbox.farmer_id')
        ->where('outbox.uniq','=',$uniq)
            ->get();
        $message="";
        $target="";


        return view('messages/confirm-messages',compact('farmers','message','target'));






    }


    public function sent(){

        return view('messages.sent');
    }
    public function cron(){

        $messages=Outbox::where('status','=',0)->get();

        Event::fire(new MessageWasSent($messages));
        echo 'cron just ran';
        exit;
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function compose(){
        $regions=Region::all();
        $crops= Crop::all();
        return view('messages/compose',compact('regions','crops'));
    }
        public function confirmMessage(Request $request){
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'message' => 'required|max:160',
            'region_id'=>'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $target="";

        if($request->input('crop_id')!="" && $request->input('cohort_id')!="" ){

            $farmers= $crops = DB::table('round_farmers')
                ->join('farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
                ->select('farmers.*')
                ->where('round_id','=',$request->input('cohort_id'))
                ->get();

            $target=[
                    'region_id'=>$request->input('region_id'),
                    'crop_id'=>$request->input('crop_id'),
                    'cohort_id'=>$request->input('cohort_id')
            ];



        }elseif($request->input('crop_id')!="" && $request->input('cohort_id')==""){

            $crop_region=CropRegion::where('crop_id',$request->input('crop_id'))->where('region_id',$request->input('region_id'))->first();
            $farmers= $crops = DB::table('rounds')
                                    ->join('round_farmers', 'rounds.id', '=', 'round_farmers.round_id')
                                    ->join('farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
                                    ->select('farmers.*')
                                    ->where('rounds.region_crop_id', '=', $crop_region->id)
                                    ->get();
            $target=['region_id'=>$request->input('region_id'),
                     'crop_id'=>$request->input('crop_id'),
                     'cohort_id'=>0,
                    ];

        }else{
            $farmers=Farmer::where('region_id','=',$request->input('region_id'))->get();
            $target=[
                'region_id'=>$request->input('region_id'),
                'crop_id'=>0,
                'cohort_id'=>0

            ];


        }
        $target=json_encode($target);

        $message=$request->input('message');


        return view('messages/confirm-messages',compact('farmers','message','target'));

        //return Redirect::to('messages/compose')->with('message', 'Successfully sent messages');


    }
    public function sendMessage(Request $request){
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'message' => 'required|max:160',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $message_data=$request->all();
        if($message_data){
            $farmers=Farmer::where('region_id','=',$message_data['region_id'])->get();
            foreach($farmers as $farmer){
                $phone='254'.substr($farmer->phone,-9);
                MessageSender::send($phone,$message_data['message']);

                return Redirect::to('messages')->with('message', 'Successfully added region');
            }
        }


    }
    public function reply($phone){
        $phone=str_replace(' ', '', $phone);
        $farmer=Farmer::wherePhone($phone)->first();
        if(!$farmer){
            return Redirect::to('messages');
        }


        $interactions=DB::table('user_interactions')->where('phone','=',$phone)->orderBy('id','desc')->get();


        $interactions_array=[];
        foreach($interactions as $interact){

              $msg=[];
              if($interact->direction=='in'){
                  $message=DB::table('inbox')->where('id','=',$interact->message_id)->first();
                  $msg['direction']='in';
                  $msg['message']=$message->response;
                  $msg['created_at']=$message->created_at;
              }elseif($interact->direction=='out'){
                  $message= $message=DB::table('outbox')->where('id','=',$interact->message_id)->first();
                  $msg['direction']='out';
                  $msg['message']=$message->message;
                  $msg['created_at']=$message->created_at;
              }
            array_push($interactions_array,$msg);
         }

            $message_to_be_replied=Inbox::where('phone','=',$phone)->orderBy('id','desc')->first();
            $message_to_be_replied->status=1;
            $message_to_be_replied->save();




        return view('messages/reply',compact('interactions_array','farmer','message_to_be_replied'));

    }
    public function sentto(Request $request){

        $farmer=Farmer::wherePhone($request->get('phone'))->first();
        $outbox=new Outbox();
        $outbox->phone=$farmer->phone;
        $outbox->farmer_id=$farmer->id;
        $outbox->message=$request->input('message');
        $outbox->type='compose';
        $outbox->save();
        $message=Outbox::find($outbox->id);
        $round_controller=new RoundsController();
        $round_controller->logMessages($message);
        $round_controller->sendTOFarmDrive($message);
        $inbox=Inbox::find($request->get('inbox_id'));
        $inbox->status=1;
        $inbox->save();
        return back()->with('message', 'Message sent successful');
    }
    public function activityMessages(){

//        $messages=DB::table('cohort_messages')
//            ->join('cohorts', 'cohorts.id', '=', 'cohort_messages.cohort_id')
//            ->join('rounds', 'rounds.id', '=', 'cohorts.round_id')
//            ->join('crop_region', 'crop_region.id', '=', 'rounds.region_crop_id')
//            ->join('regions', 'regions.id', '=', 'crop_region.region_id')
//            ->join('crops', 'crops.id', '=', 'crop_region.crop_id')
//            ->select('cohort_messages.*','rounds.id as round_id','rounds.name as round_name','cohorts.name as cohort_name','regions.id as region_id', 'regions.name as region_name','crops.id as crop_id','crops.name as crop_name')
//            ->get();
        return view('messages/activity_messages');
    }
    public function getActivityMessages(){

        if(Sentry::check() && Sentry::getUser()->hasAccess('agronomist')){
            $user_id=Sentry::getUser()->id;
            $user_region=DB::table('user_regions')->where('user_id','=',$user_id)->first();
            $messages=DB::table('cohort_messages')
                ->select([
                    'cohort_messages.cohort_id',
                    'cohort_messages.message',
                    'cohort_messages.id',
                    'cohort_messages.title',
                    'cohort_messages.status',
                    'rounds.name as round_name',
                    'cohorts.name as cohort_name',
                    'regions.name as region_name',
                    'crops.name as crop_name'
                ])
                ->leftJoin('cohorts', 'cohorts.id', '=', 'cohort_messages.cohort_id')
                ->leftJoin('rounds', 'rounds.id', '=', 'cohorts.round_id')
                ->leftJoin('crop_region', 'crop_region.id', '=', 'rounds.region_crop_id')
                ->leftJoin('regions', 'regions.id', '=', 'crop_region.region_id')
                ->leftJoin('crops', 'crops.id', '=', 'crop_region.crop_id')
                ->where('regions.id','=',$user_region->region_id)
                ->groupBy('cohort_messages.id');
        }else{
            $messages=DB::table('cohort_messages')
                ->select([
                    'cohort_messages.cohort_id',
                    'cohort_messages.message',
                    'cohort_messages.id',
                    'cohort_messages.title',
                    'cohort_messages.status',
                    'rounds.name as round_name',
                    'cohorts.name as cohort_name',
                    'regions.name as region_name',
                    'crops.name as crop_name'
                ])
                ->leftJoin('cohorts', 'cohorts.id', '=', 'cohort_messages.cohort_id')
                ->leftJoin('rounds', 'rounds.id', '=', 'cohorts.round_id')
                ->leftJoin('crop_region', 'crop_region.id', '=', 'rounds.region_crop_id')
                ->leftJoin('regions', 'regions.id', '=', 'crop_region.region_id')
                ->leftJoin('crops', 'crops.id', '=', 'crop_region.crop_id')
                ->groupBy('cohort_messages.id');
        }

        return \yajra\Datatables\Datatables::of($messages)->make(true);

    }
    public function sendFarmers(Request $request){

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'message' => 'required|max:160',
            'farmers_type'=>'required',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $farmer_ids="";
        $farmer_array=[];
            if($request->get('farmers_type')=='select'){

                $farmer_ids=$request->get('farmer_id');

                if($farmer_ids){
                    $farmer_ids=explode(',',$farmer_ids);
                    $farmer_array=[];

                    foreach($farmer_ids as $id){
                        $farmer=DB::table('farmers')->where('id','=',$id)->first();

                        array_push($farmer_array,$farmer);
                    }
                }else{
                    return redirect('messages/compose')->with('message', 'Error:Please make sure you select the farmers you want to send the message to.');
                }




            }else{
                $target=$request->get('target');
                if($target){
                    $target=json_decode($request->get('target'));

                    if($target->cohort_id >=1 && $target->region_id >=1 && $target->crop_id >=1 ){
                        $farmers= $crops = DB::table('round_farmers')
                            ->join('farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
                            ->select('farmers.*')
                            ->where('round_id','=',$target->cohort_id)
                            ->get();

                    }elseif($target->cohort_id ==0 && $target->region_id >=1 && $target->crop_id >=1){
                        $crop_region=CropRegion::where('crop_id',$target->crop_id)->where('region_id',$target->region_id)->first();
                        $farmers= $crops = DB::table('rounds')
                            ->join('round_farmers', 'rounds.id', '=', 'round_farmers.round_id')
                            ->join('farmers', 'farmers.id', '=', 'round_farmers.farmer_id')
                            ->select('farmers.*')
                            ->where('rounds.region_crop_id', '=', $crop_region->id)
                            ->get();


                    }elseif($target->cohort_id ==0 && $target->region_id >=1 && $target->crop_id ==0){
                        // $farmers=Farmer::where('region_id','=',$target->region_id)->get();
                        $farmers=DB::table('farmers')->where('region_id','=',$target->region_id)->get();

                    }
                }else{
                    $farmers=json_decode($request->get('all_farmers'));
                }



                if($farmers){
                    $farmer_array=(array) $farmers;

                }else{
                    return redirect('messages/compose')->with('message', 'Error: No farmers to send the message to.');

                }



            }



            $message=$request->input('message');




        return view('messages/confirm-send',compact('message','farmer_array'));

    }
    public function sentToFarmDrive(Request $request){


       if($request->get('farmers')){
            $farmers=json_decode($request->get('farmers'));

            $no_farmers=count($farmers);
            $rand = substr(uniqid('', true), -5);

            foreach($farmers as $farm){
                $farmer=Farmer::find($farm->id);
                $outbox=new Outbox();
                $outbox->phone=$farmer->phone;
                $outbox->farmer_id=$farmer->id;

                if(strpos($request->get('message'),"{{name}}") !==false){
                    $exploded_name=explode(' ',$farmer->name);
                    $message=str_replace('{{name}}',$exploded_name[0],$request->get('message'));

                }elseif(strpos($request->get('message'),"{{Name}}") !==false){
                    $exploded_name=explode(' ',$farmer->name);
                    $message=str_replace('{{Name}}',$exploded_name[0],$request->get('message'));
                }else{
                    $message=$request->input('message');
                }

                $outbox->message=$message;
                $outbox->uniq=$rand;
                $outbox->type='compose';
                $outbox->save();
            }
           $messages=Outbox::where('status','=',0)->orderBy('id','desc')->take($no_farmers)->get();

           $this->logMessages($messages);


           Event::fire(new MessageWasSent($messages));

           return Redirect::to('messages/compose')->with('message', 'Message sent successful');

       }else{
           return Redirect::to('messages')->with('message', 'Error in sending message');
       }
    }
    public function  logMessages($messages){
             $i=1;
            if($messages){
                foreach ($messages as $message) {

                    $user_interaction = [];
                    $user_interaction['phone'] = $message->phone;
                    $user_interaction['farmer_id'] = $message->farmer_id;
                    $user_interaction['message_id'] = $message->id;
                    $user_interaction['direction'] = 'out';
                    $user_interaction['created_at']=Carbon::now()->toDateTimeString();
                    $user_interaction['updated_at']=Carbon::now()->toDateTimeString();

                    DB::table('user_interactions')->insert($user_interaction);
                    $i++;
                }
            }


        }


}
