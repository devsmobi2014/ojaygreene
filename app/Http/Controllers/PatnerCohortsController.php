<?php namespace App\Http\Controllers;

use App\Farmer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MessageSender;
use App\Patner;
use App\PatnerCohort;
use App\PatnerCohortsFarmers;
use App\PatnerCohortsMeasurables;
use App\PatnerCohortsMeasurableValues;
use App\PatnerRegions;
use App\User_patners;
use App\UserRegions;
use App\ValueChain;
use Carbon\Carbon;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PatnerCohortsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$user_id=Sentry::getUser()->id;




		if(\Sentry::getUser()->hasAccess('patner_admin') ){

			$cohorts=PatnerCohort::join('value_chains','value_chains.id','=','patner_cohorts.value_chain_id')
				->leftJoin('user_regions','user_regions.region_id','=','patner_cohorts.region_id')
				->leftJoin('regions','patner_cohorts.region_id','=','regions.id')
				->where('user_regions.user_id',$user_id)
				->select('patner_cohorts.*','value_chains.name as value_chain_name','regions.name as region_name')
				->get();

		}elseif(\Sentry::getUser()->hasAccess('partner_user')){




			$user_patner=User_patners::where('user_id',$user_id)->first();

			$cohorts=PatnerCohort::join('value_chains','value_chains.id','=','patner_cohorts.value_chain_id')
				->leftJoin('user_regions','user_regions.region_id','=','patner_cohorts.region_id')
				->leftJoin('regions','patner_cohorts.region_id','=','regions.id')
				->where('user_regions.user_id',$user_id)
				->where('patner_cohorts.department_id',$user_patner->department_id)
				->select('patner_cohorts.*','value_chains.name as value_chain_name','regions.name as region_name')
				->get();


		}elseif(\Sentry::getUser()->hasAccess('admin')){
			$cohorts=PatnerCohort::join('value_chains','value_chains.id','=','patner_cohorts.value_chain_id')
				->leftJoin('regions','patner_cohorts.region_id','=','regions.id')
				->where('patner_cohorts.patner_id',$partner_id)
				->select('patner_cohorts.*','value_chains.name as value_chain_name','regions.name as region_name')
				->get();

		}


		return view('patner-cohorts/index',compact('cohorts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('logout');
		}

		$value_chains=ValueChain::where('patner_id','=',$partner_id)->get();

		if(\Sentry::getUser()->hasAccess('patner_admin')||\Sentry::getUser()->hasAccess('admin')){

			$regions=PatnerRegions::leftJoin('regions','regions.id','=','patner_regions.region_id')
				-> where('patner_id',$partner_id)->select('regions.*')->get();
		}elseif(\Sentry::getUser()->hasAccess('partner_user')){

			$user_id=Sentry::getUser()->id;
			$regions=UserRegions::leftJoin('regions','regions.id','=','user_regions.region_id')
				-> where('user_id',$user_id)->select('regions.*')->get();


		}



		return view('patner-cohorts.create',compact('value_chains','regions'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//

		$v = Validator::make($request->all(), [
			'names' => 'required',
			'description'=>'required',
			'value_chain_id'=>'required',
		]);
		if($v->fails()){
			return redirect()->back()->withErrors($v->errors());
		}
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$value_chain=ValueChain::find($request->get('value_chain_id'));
		$patner_cohort=new PatnerCohort();
		$patner_cohort->names=$request->get('names');
		$patner_cohort->patner_id=$partner_id;
		$patner_cohort->value_chain_id=$request->get('value_chain_id');
		$patner_cohort->description=$request->get('description');
		$patner_cohort->region_id=$request->get('region_id');
		$patner_cohort->department_id=$value_chain->department_id;
		$patner_cohort->save();
		return Redirect::to('patner-cohorts');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('logout');
		}
		$cohort=PatnerCohort::find($id);


		$farmers=PatnerCohortsFarmers::leftJoin('farmers','farmers.id','=','patner_cohorts_farmers.farmer_id')
			     ->select('farmers.*')->where('patner_cohorts_farmers.cohort_id',$cohort->id)->get();
		$measurables=PatnerCohortsMeasurables::where('cohort_id',$id)->get();

		$farmers_array=[];
		if($farmers){
			foreach($farmers as $farmer){
				$farmer_array=$farmer->toArray();
				$farmer_array['measurables']=[];

				if($measurables){
					$dat=[];
					foreach($measurables as $mes){
						$mes_arry=[];
						$input=PatnerCohortsMeasurableValues::where('patner_measurable_id',$mes->id)->where('farmer_id',$farmer_array['id'])->first();
						if($input){

							$dat[$mes->id]=$input->value;


						}else{

							$dat[$mes->id]="";


						}

					}
					$farmer_array['measurables']=$dat;

				}else{
					$farmer_array['measurables']='';
				}

				$inputs=DB::table('patner_cohorts_measurables')->leftJoin('patner_cohorts_measurable_values','patner_cohorts_measurables.id','=','patner_cohorts_measurable_values.patner_measurable_id')
				->where('farmer_id',$farmer_array['id'])
			    ->select('patner_cohorts_measurables.id','patner_cohorts_measurables.name','patner_cohorts_measurables.unit','patner_cohorts_measurable_values.value as measurable_value')
				->orderBy('patner_cohorts_measurables.id','asc')->get();



				array_push($farmers_array,$farmer_array);
			}
		}

		
		return view('patner-cohorts.view',compact('farmers_array','cohort','measurables'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('logout');
		}
		$cohort=PatnerCohort::find($id);

		$value_chains=ValueChain::where('patner_id','=',$partner_id)->get();

		if(\Sentry::getUser()->hasAccess('patner_admin')||\Sentry::getUser()->hasAccess('admin')){

			$regions=PatnerRegions::leftJoin('regions','regions.id','=','patner_regions.region_id')
				-> where('patner_id',$partner_id)->select('regions.*')->get();
		}elseif(\Sentry::getUser()->hasAccess('partner_user')){

			$user_id=Sentry::getUser()->id;
			$regions=UserRegions::leftJoin('regions','regions.id','=','user_regions.region_id')
				-> where('user_id',$user_id)->select('regions.*')->get();


		}
		return view('patner-cohorts.edit',compact('value_chains','regions','cohort'));

	}
	public function editCohort(Request $request){
		$v = Validator::make($request->all(), [
			'names' => 'required',
			'description'=>'required',
			'value_chain_id'=>'required',
			'cohort_id'=>'required',
		]);
		if($v->fails()){
			return redirect()->back()->withErrors($v->errors());
		}
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$value_chain=ValueChain::find($request->get('value_chain_id'));
		$patner_cohort=PatnerCohort::find($request->get('cohort_id'));
		$patner_cohort->names=$request->get('names');
		$patner_cohort->patner_id=$partner_id;
		$patner_cohort->value_chain_id=$request->get('value_chain_id');
		$patner_cohort->description=$request->get('description');
		$patner_cohort->region_id=$request->get('region_id');
		$patner_cohort->department_id=$value_chain->department_id;
		$patner_cohort->save();
		return Redirect::to('patner-cohorts');
	}
	public function sendMessage($id){
		$cohort=PatnerCohort::find($id);
		return view('patner-cohorts.send-message',compact('cohort'));
	}
	public function StoreMessage(Request $request){
		$validator = Validator::make($request->all(), [
			'cohort_id' => 'required|max:45',
			'message'=>'required'
		]);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}
		$message_data=$request->all();
		if($message_data){
			$farmers=PatnerCohortsFarmers::join('farmers','farmers.id','=','patner_cohorts_farmers.farmer_id')->where('patner_cohorts_farmers.cohort_id',$request->get('cohort_id'))->select('farmers.*')->get();

			if($farmers){
				foreach($farmers as $farmer){

					$phone='254'.substr($farmer->phone,-9);

					MessageSender::send($phone,$message_data['message']);
				}
			}

		}
		return Redirect::to('patner-cohort/view/'.$request->get('cohort_id'))->with('message', 'Successfully added region');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function addPartnerFarmers($id){
		$cohort=PatnerCohort::find($id);
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('logout');
		}
		$farmers_array=PatnerRegions::leftJoin('farmers','farmers.region_id','=','patner_regions.region_id')
						->leftJoin('regions','regions.id','=','patner_regions.region_id')
						->select('farmers.*','regions.name as region_name')
						->orderBy('farmers.id','desc')
						->where('patner_id',$partner_id)->get();


//		$farmers_array=

		return view('patner-cohorts.add_farmers',compact('cohort','farmers_array'));
	}
	public function removePartnerFarmers($id,$cohort_id){
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('logout');
		}
		if(!$id){
			return redirect('patner-farmers');
		}
		$farmer=PatnerCohortsFarmers::where('farmer_id',$id)->where('cohort_id',$cohort_id)->delete();

		return redirect('patner-cohort/view/'.$cohort_id);


	}
	public function storePatnerFarmers(Request $request){
		$validator = Validator::make($request->all(), [
			'cohort_id' => 'required|max:45',
			'farmer_id'=>'required'
		]);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}
		$data=$request->all();
		$farmer_ids=explode(',',$data['farmer_id']);



		$cohort_id=$data['cohort_id'];
		foreach($farmer_ids as $farmer_id){

			$i=0;
			$exists=DB::table('patner_cohorts_farmers')->where('farmer_id', $farmer_id)->where('cohort_id', $cohort_id)->first();
			if(!$exists){
				$dat['farmer_id']=  $farmer_id;
				$dat['cohort_id' ]= $cohort_id;
				$dat['created_at']=Carbon::now()->toDateTimeString();
				$dat['updated_at']=Carbon::now()->toDateTimeString();

				$res=DB::table('patner_cohorts_farmers')->insert($dat);
				if($res){
					$i++;
				}

			}

		}
		return redirect('patner-cohort/view/'.$data['cohort_id']);
	}
	public function measurables($id){
		$cohort=PatnerCohort::find($id);
		$patner_cohorts_measurables=PatnerCohortsMeasurables::where('cohort_id',$id)->get();
		return view('patner-cohorts.measurables',compact('patner_cohorts_measurables','cohort'));
	}
	public function addMeasurables($id){
		$cohort=PatnerCohort::find($id);
		return view('patner-cohorts.create_measurable',compact('cohort'));
	}
	public function storeMeasurables(Request $request){
		$validator = Validator::make($request->all(), [
			'cohort_id' => 'required|max:45',
			'name'=>'required',
			'unit'=>'required'
		]);

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}
		$patner_cohort_measurable= new PatnerCohortsMeasurables();

		$patner_cohort_measurable->cohort_id=$request->get('cohort_id');
		$patner_cohort_measurable->name=$request->get('name');
		$patner_cohort_measurable->unit=$request->get('unit');
		$patner_cohort_measurable->save();
		return redirect('patner-cohorts-measurables/'.$request->get('cohort_id'));

	}
	public function editPartnerFarmer($id,$cohort_id){

		if(!$id){
			return redirect('patner-cohort/view/'.$cohort_id);
		}
		$patner_cohorts_measurables=Db::table('patner_cohorts_measurables')->where('cohort_id',$cohort_id)->get();


		$farmer_patner_measurables=DB::table('patner_cohorts_measurables')
									->leftJoin('patner_cohorts_measurable_values','patner_cohorts_measurable_values.patner_measurable_id','=','patner_cohorts_measurables.id')
									->where('patner_cohorts_measurables.cohort_id',$cohort_id)
									->where('patner_cohorts_measurable_values.farmer_id',$id)
									->select('patner_cohorts_measurables.*','patner_cohorts_measurable_values.value')
									->get();
		$farmer=Farmer::find($id);

		return view('patner-cohorts.edit_farmer',compact('patner_cohorts_measurables','farmer_patner_measurables','farmer','cohort_id'));


	}
	public function storeFarmerDetails(Request $request){
		$validator = Validator::make($request->all(), [
			'cohort_id' => 'required|max:45',
			'farmer_id'=>'required',
		]);

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}
		$patner_measurables=$request->get('patner_measurable');
		if($patner_measurables){
			foreach($patner_measurables as $key =>$value){
				if(trim($value)){

					$exists=PatnerCohortsMeasurableValues::where('farmer_id',$request->get('farmer_id'))->where('patner_measurable_id',$key)->first();
					if($exists){
						$data['patner_measurable_id']=$key;
						$data['value']=str_replace(' ', '', $value);
						$data['farmer_id']=$request->get('farmer_id');
						$data['created_at']=Carbon::now()->toDateTimeString();
						$data['updated_at']=Carbon::now()->toDateTimeString();
						DB::table('patner_cohorts_measurable_values')->where('id',$exists->id)->update($data);
					}else{
						$data['patner_measurable_id']=$key;
						$data['value']=str_replace(' ', '', $value);
						$data['farmer_id']=$request->get('farmer_id');
						$data['created_at']=Carbon::now()->toDateTimeString();
						$data['updated_at']=Carbon::now()->toDateTimeString();
						DB::table('patner_cohorts_measurable_values')->insert($data);
					}

				}
			}
		}
		return redirect('patner-cohort/view/'.$request->get('cohort_id'));
	}


}
