<?php namespace App\Http\Controllers;

use App\CropRegion;
use App\Http\Controllers\Controller;
use App\Region;
use App\Round;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class AgronomistsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
	public function index()
	{
		//
        //$agronomists=User::where('role_id','2')->get();
        $agronomists=DB::table('users')
            ->select('users.id','users.username','users.last_login','users.email')
            ->leftJoin('users_groups','users_groups.user_id','=','users.id')
            ->where('users_groups.group_id','=',3)->paginate(10);


        return view('agronomists/index',compact('agronomists'));
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        return view('agronomists/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'email'=> 'required|unique:users|max:50',
            'password'=>'required|min:6|confirmed',
            'password_confirmation' => 'required',
            'phone_no'=>'required'
        ]);



        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $verification_code = str_random(30);
        $user = new User();
        $user -> name = $request->input('name');
        $user -> email = $request->input('email');
        $user -> phone_no = $request->input('phone_no');
        $user -> password = bcrypt($request->input('password'));
        $user -> verification_code = $verification_code;
        $user->role_id=2;

        if($user->save()) {
            $data = array(
                'name' => $user->name,
                'code' => $verification_code,
                'password'=>$request->get('password')
            );

            \Mail::later(5,'emails.verify', $data, function($message) use($user) {
                $message->to($user->email, $user->name)->subject('Welcome to Ojay greene');
            });
            \Session::flash('message', 'Agronomists created');
            return redirect()->back()->withInput();

        } else {
            \Session::flash('message', 'Your account couldn\'t be created please try again');
            return redirect()->back()->withInput();
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $user=User::find($id);

        $regions=DB::table('user_regions')
            ->join('regions','regions.id','=','user_regions.region_id')
            ->where('user_id','=',$user->id)
            ->select('user_regions.region_id','regions.name')->get();


        $rounds_array=[];
        if($regions){

            foreach($regions as $user_region){

                $crop_region=CropRegion::where('region_id','=',$user_region->region_id)->get();
                if($crop_region){
                    foreach($crop_region as $cropr){
                        $rounds=Round::where('region_crop_id','=',$cropr->id)->get();
                        foreach($rounds as $round){
                            array_push($rounds_array,$round->toArray());
                        }

                    }
                }

            }
        }
        $farmers="";

        return view('agronomists/view',compact('user','regions','rounds_array','farmers'));

	}
    public function addRegions($hash){
        $user=User::find($hash);
        $regions=Region::all();
        if($user){
            $regions_array=[];
            $user_regions=DB::table('user_regions')->where('user_id','=',$user->id)->get();
            if($user_regions){
                foreach($user_regions as $region){
                    array_push($regions_array,$region->region_id);

                }
            }


         return view('agronomists/add-regions',compact('regions','user','regions_array'));

        }else{
            return Redirect::to('users');
        }

    }
    public function storeRegion(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'regions'=> 'required'
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        if($request->get('regions')){
            DB::table('user_regions')->where('user_id', '=', $request->get('user_id'))->delete();

            foreach($request->get('regions') as $region){
                $data['user_id']=$request->get('user_id');
                $data['region_id']=$region;

                $data['created_at']=Carbon::now()->toDateTimeString();
                $data['updated_at']=Carbon::now()->toDateTimeString();
                DB::table('user_regions')->insert($data);


            }
        }

        return Redirect::to('users');




    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        $agronomist=User::find($id);
        return view('agronomists/edit',compact('agronomist'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'email'=> 'required|max:50',
            'phone_no'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $user=User::find($id);
        $input = $request->all();

        $user->fill($input)->save();

        return Redirect::to('agronomists')->with('message', 'Successfully edited user');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $mnfe) {
            return redirect()->back();
        }


        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $user->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        return Redirect::to('agronomists')->with('message', 'Successfully deleted Agronomists');

	}

}
