<?PHP namespace App\Http\Controllers;

use App\Crop;
use App\CropRegion;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Round;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use yajra\Datatables\Datatables;

class CropsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
    public function index()
    {

        $crops = Crop::all();

        return view('crops/index', compact('crops', 'crops'));
    }
    public function getCrops(){
        $crops = Crop::all();

        return Datatables::of($crops)->make(true);
    }
    public function getCohorts(Request $request){
        $crop_id=$request->input('crop_id');
        $region_id=$request->input('region_id');

        $crop_region=CropRegion::where('crop_id',$crop_id)->where('region_id',$region_id)->first();
        $time_cohorts=Round::where('region_crop_id','=',$crop_region->id)->get();
        return response()->json($time_cohorts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('crops/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //get the requests
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'description' => 'required',
//            'image' => 'required',
            'unit_of_measurement' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $crop_details = $request->all();

        Crop::create($crop_details);


//        return redirect()->back();

        return Redirect::to('crops')->with('message', 'Successfully added crop');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $crop = Crop::findOrFail($id);
        } catch (ModelNotFoundException $mnfe) {
            return Redirect::to('crops')->with('message', 'Sorry, crop not found');
        }

        return view('crops.view')->withCrop($crop);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $crop=Crop::find($id);

        return view('crops/edit',compact('crop'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        //]
        //get the requests
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
            'description' => 'required',
            'unit_of_measurement' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $crop=Crop::find($id);
        $input = $request->all();

        $crop->fill($input)->save();


        return Redirect::to('crops')->with('message', 'Successfully added crop');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $crop = Crop::findOrFail($id);
        } catch (ModelNotFoundException $mnfe) {
            return Redirect::to('crops')->with('message', 'Sorry, crop not found');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $crop->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return Redirect::to('crops')->with('message', 'Successfully deleted crop');
    }

}
