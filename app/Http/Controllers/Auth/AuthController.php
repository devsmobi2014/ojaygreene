<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     * @return \App\Http\Controllers\Auth\AuthController
     */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}
	public function activateAccount($code, User $user)
	{
		if($user->accountIsActive($code)) {
			\Session::flash('message', 'Success, your account has been activated.');
			return redirect('dashboard');
		}
		\Session::flash('message', 'Your account couldn\'t be activated, please try again');
		return redirect('dashboard');
	}

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $user = User::whereEmail($request->email)->first();
        if($user){
            if ($user->confirmed == 0) {
                \Session::flash('message', 'Please activate your account to proceed.');
                return redirect()->guest('auth/login');
            } else {

                $credentials = $request->only('email', 'password');

                if ($this->auth->attempt($credentials, $request->has('remember')))
                {
                    return redirect()->intended($this->redirectPath());
                }
                return redirect($this->loginPath())
                    ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'email' => $this->getFailedLoginMessage(),
                    ]);
            }
        }else{
            \Session::flash('message', 'Wrong login credentials.');
            return redirect()->guest('auth/login');
        }



    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\View\View
     */
    public function postRegister(Request $request)
    {
        $validator = $this->registrar->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $verification_code = str_random(30);
        $user = new User;
        $user -> name = $request->input('name');
        $user -> email = $request->input('email');
        $user -> phone_no = $request->input('phone_no');
        $user -> password = bcrypt($request->input('password'));
        $user -> verification_code = $verification_code;

        if($user->save()) {
            $data = array(
                'name' => $user->name,
                'code' => $verification_code
            );
            \Mail::later(5,'emails.verify', $data, function($message) use($user) {
                $message->to($user->email, $user->name)->subject('Welcome to Ojay greene');
            });
            \Session::flash('message', 'Please check the email you provided. We have sent a verification link.');
            return view('emails.confirm');
        } else {
            \Session::flash('message', 'Your account couldn\'t be created please try again');
            return redirect()->back()->withInput();
        }

    }

    /**
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : 'dashboard';
    }

}