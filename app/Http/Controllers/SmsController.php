<?php namespace App\Http\Controllers;

use App\County;
use App\Farmer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Inbox;
use App\MessageSender;
use App\Region;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SmsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request )
	{
		//
		error_reporting(0);
		header('Content-type: text/plain');
		set_time_limit(100);

		//get inputs
		$from = $_REQUEST['from'];
		$to =   $_REQUEST['to'];
		$text = $_REQUEST['text'];
		$date = $_REQUEST['date'];
		$id =   $_REQUEST['id'];
		$linkId = $_REQUEST['linkId'];



		$phone='0'.substr($from,-9);



		$checkuser=Farmer::where('phone','=',$phone)->where('subscribed','=',1)->first();
		




		if($checkuser){
			self::recieveMessage($phone,$text);

		}else{
           self::registration($phone,$text);
		}

	}

	public function registration($phone,$text){





		//Start the registration
		$user=Farmer::wherePhone($phone)->first();

		if(!$user){
			$user=new Farmer();
			$user->phone=$phone;
			$user->save();
		}


		if($user->level==0){
			$message="Karibu Ustawihub, Tafadhali Jibu maswali ifuatayo .Andika jina lako kama ilivyo kwenye kitambulisho.".PHP_EOL;
			self::sendMessage($phone,$message);
			$user->level=1;
			$user->save();
		}elseif($user->level==1){
			$user->name=$text;
			$user->level=2;
			$user->save();
			$message="Andikisha namba ya kitambulisho";
			self::sendMessage($phone,$message);

		}elseif($user->level==2){
			$user->national_id=$text;
			$user->level=3;
			$user->save();
			$message="Jibu na ".PHP_EOL."1. Kama ni mwanaume na".PHP_EOL." 2. Kama ni mwanamke";
			self::sendMessage($phone,$message);
		}elseif($user->level==3){
			if($text==1 || $text=="mwanaume"){
				$user->gender=1;
			}elseif($text==1 || $text=="mwanamke"){
				$user->gender=2;
			}
			$user->level=4;
			$user->save();
			$message="Shamba lako liko katika Kaunti gani?";
			self::sendMessage($phone,$message);

		}elseif($user->level==4){
			$reg=strtoupper($text);
			$region=County::where('name','%like%',$reg)->first();
			if($region){
				$region_id=$region->id;
			}else{
				$region_new=new Region();
				$region_new->name=$text;
				$region_new->save();
				$region_id=$region_new->id;

			}
			$user->region_id=$region_id;
			$user->level=5;
			$user->save();
			$message="Shamba lako liko katika wilaya (constituency) ipi?";
			self::sendMessage($phone,$message);

		}elseif($user->level==5){
			$user->constituency=$text;
			$user->level=6;
			$user->save();
			$message="Shamba lako liko katika ward ipi?";
			self::sendMessage($phone,$message);
		}
		elseif($user->level==6){
			$user->ward=$text;
			$user->level=7;
			$user->save();
			$message="Je uko katika kikundi chochote cha ukulima? Jibu NDIO au LA?";
			self::sendMessage($phone,$message);
		}
		elseif($user->level==7){
			$text=strtolower($text);
			if($text=="ndio" || $text=="yes"){
				$message="Kikundi chako cha Ukulima kinaitwa aje?";
			}elseif($text=="la" || $text=="no"){
				$user->level=8;
				$user->save();
				$message="Shamba lako ni ekari ngapi? Jibu na nambari pekee, Mfano 0.25, 0.5, 1";
			}else{
				$user->farmer_group=$text;
				$user->level=8;
				$user->save();
				$message="Shamba lako ni ekari ngapi? Jibu na nambari pekee, Mfano 0.25, 0.5, 1";
			}

			self::sendMessage($phone,$message);

		}elseif($user->level==8){
			$user->acreage=$text;
			$user->level=8;
			$user->subscribed=1;
			$user->save();
			$message="Shukrani kwa Kukamilisha usajili wako. Soma sheria na masharti ifuatayo. http://ustawihub.com/terms";
			self::sendMessage($phone,$message);

		}



	}
	public function sendMessage($phone,$message){
		MessageSender::send($phone,$message);

	}
	public function recieveMessage($phone,$text){
		$farmer=Farmer::where('phone',$phone)->first();
		if($farmer){
			$inbox=new Inbox();
			$inbox->phone=$phone;
			$inbox->response=$text;
			$inbox->farmer_id=$farmer->id;
			$inbox->save();
			//add to user interactions
			$user_interaction=[];
			$user_interaction['phone']=$phone;
			$user_interaction['farmer_id']=$farmer->id;
			$user_interaction['message_id']=$inbox->id;
			$user_interaction['direction']='in';
			$user_interaction['created_at']=Carbon::now()->toDateTimeString();
			$user_interaction['updated_at']=Carbon::now()->toDateTimeString();
			DB::table('user_interactions')->insert($user_interaction);
			exit;
		}


	}


}
