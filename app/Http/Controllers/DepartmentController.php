<?php namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User_patners;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		if(\Sentry::getUser()->hasAccess('patner_admin') ||\Sentry::getUser()->hasAccess('admin')){

			$departments=Department::where('patner_id',$partner_id)->get();
		}elseif(\Sentry::getUser()->hasAccess('partner_user')){

			$user_id=Sentry::getUser()->id;
			$user_patner=User_patners::where('user_id',$user_id)->first();
			$departments=Department::where('id',$user_patner->department_id)->get();


		}



		return view('departments.index',compact('departments'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		
		return view('departments.create');
	}
	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
		$v = Validator::make($request->all(), [
			'names' => 'required',
			'description'=>'required',
		]);
		if($v->fails()){
			return redirect()->back()->withErrors($v->errors());
		}
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}


		
		$department=new Department();
		$department->names=$request->get('names');
		$department->patner_id=$partner_id;
		$department->description=$request->get('description');

		$res=$department->save();
		

		return Redirect::to('departments');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		if(!$id){
			return Redirect::to('departments');
		}
		$department=Department::find($id);

		return view('departments.edit',compact('department'));



	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function storeUpdate(Request $request)
	{
		//
		$department=Department::find($request->get('department_id'));
		$department->names=$request->get('names');
		$department->description=$request->get('description');
		$department->save();
		return Redirect::to('departments');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$department=Department::find($id);
		$department->delete();
		return Redirect::to('departments');
	}

}
