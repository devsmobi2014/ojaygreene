<?php namespace App\Http\Controllers;

use App\Crop;
use App\Farmer;
use App\User;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $farmers=Farmer::all()->count();
        $crops=Crop::all()->count();
        $agronomists=User::where('role_id','=',2)->count();
		return view('landing',compact('agronomists','crops','farmers'));
	}
	public function terms(){
		
		return view('terms');
	}

}
