<?php namespace App\Http\Controllers;

use App\CropRegion;
use App\Farmer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PatnerRegions;
use App\Region;
use App\Crop;
use App\UserRegions;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

class RegionsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        $this->middleware('sentry.auth');
    }
	public function index()
	{
	    $regions=Region::all();

        return view('regions/index', compact('regions'));
	}
    public function createPatnerRegion(){
        return view('regions.create_patner_regions');
    }
    public function StorePatnerRegion(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        //check if the region exists
        if(Region::where('name','%'.$request->get('name').'%')->first()){
            return Redirect::to('patner-regions')->with('message', 'Region already exists');
        }else{

            $region_details = $request->all();
            $partner_id=session('patner_id');
            $res=Region::create($region_details);
            $patner_regions=new PatnerRegions();
            $patner_regions->region_id=$res->id;
            $patner_regions->patner_id=$partner_id;
            $patner_regions->save();
            return Redirect::to('patner-regions')->with('message', 'Region added successfully');
        }
    }
    public  function patnerRegions(){
        $partner_id=session('patner_id');
        if(!$partner_id){
            return redirect('dashboard/patner-intro');
        }
        if(\Sentry::getUser()->hasAccess('patner_admin')||\Sentry::getUser()->hasAccess('admin')){

            $regions=PatnerRegions::leftJoin('regions','regions.id','=','patner_regions.region_id')
                -> where('patner_id',$partner_id)->select('regions.*')->get();
        }elseif(\Sentry::getUser()->hasAccess('partner_user')){

            $user_id=Sentry::getUser()->id;
            $regions=UserRegions::leftJoin('regions','regions.id','=','user_regions.region_id')
                -> where('user_id',$user_id)->select('regions.*')->get();


        }



        return view('regions.patner_regions',compact('regions'));

    }
    public function manage(){
        $regions=Region::all();
        return view('regions/manage', compact('regions'));

    }
    public function manageStore(Request $request){
        $validator = Validator::make($request->all(), [
            'from' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
         DB::table('regions')->where('status', '=', 1)->update(array('status' => 0));


        foreach($request->get('from') as $region_id){


            $region=Region::find($region_id);


             $region->status=1;

            $region->save();
        }

        return redirect()->back()->with('Management successful');

    }
    public function getRegions(){




        if(Sentry::check() && Sentry::getUser()->hasAccess('agronomist')){
            $user_id=Sentry::getUser()->id;
            $regions=DB::table('user_regions')->select([
                'regions.name',
                'regions.id',
                DB::raw('count(farmers.region_id) AS region_count'),
            ])
                ->leftJoin('regions','regions.id','=','user_regions.region_id')
                ->leftJoin('farmers','regions.id','=','farmers.region_id')
                ->where('user_regions.user_id','=',$user_id)
                ->groupBy('regions.id');
        }else{
            $regions = DB::table('regions')->select([
                'regions.name',
                'regions.id',
                DB::raw('count(farmers.region_id) AS region_count'),

            ])->leftJoin('farmers','regions.id','=','farmers.region_id')
                ->groupBy('regions.id');

        }

        return \yajra\Datatables\Datatables::of($regions)->make(true);

    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		//
        return view('regions.create');
	}
    public function migrate(){
        $regions= Region::all();

        return view('regions/migrate',compact('regions'));
    }
    public function migrateStore(Request $request){

        $validator = Validator::make($request->all(), [
            'from' => 'required',
             'to'   =>'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $data=$request->all();
        foreach($data['from'] as $reg){
            $farmers=Farmer::where('region_id','=',$reg)->get();
            foreach($farmers as $farmer){
                $farmer->region_id=$data['to'];
                $farmer->save();
            }
        }
        return Redirect::to('regions')->with('message', 'Migration successful');
    }
    public function getCrops(Request $request){
         $region_id=$request->input('region_id');
         $crops = DB::table('crop_region')
            ->join('crops', 'crops.id', '=', 'crop_region.crop_id')
            ->select('crops.*')
            ->where('crop_region.region_id', '=', $region_id)
            ->get();

        return response()->json($crops);
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        //get the requests
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:45',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        //check if the region exists
        if(Region::where('name',$request->get('name'))->first()){
            return Redirect::to('regions')->with('message', 'Region already exists');
        }else{
            $partner_id=session('patner_id');
            if(!$partner_id){
                echo "here ";
                exit;
            }

            $region_details = $request->all();

            Region::create($region_details);
        }

        return Redirect::to('regions')->with('message', 'Successfully added region');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        try {
            $region = Region::findOrFail($id);
            $user_regions=DB::table('user_regions')->where('region_id','=',$region->id)
                ->leftJoin('users','users.id','=','user_regions.user_id')
                ->select('users.username')->get();


            $crops = DB::table('crop_region')
                ->join('regions', 'regions.id', '=', 'crop_region.region_id')
                ->join('crops', 'crops.id', '=', 'crop_region.crop_id')
                ->select('crop_region.id as crop_region_id','crops.*','crop_region.duration')
                ->where('crop_region.region_id', '=', $id)
                ->get();

            if($crops){

                foreach($crops as $crop){

                    $rounds= DB::table('rounds')->where('region_crop_id','=',$crop->crop_region_id)->get();
                    $farmers=0;
                    foreach($rounds as $round){
                        $farm=DB::table('round_farmers')->where('round_id','=',$round->id)->count();
                        $farmers+=$farm;
                    }
                    $crop->farmers=$farmers;


                }

            }



        } catch (ModelNotFoundException $mnfe) {
            return Redirect::to('regions')->with('message', 'Sorry, region not found');
        }



        return view('regions.view',compact('region','crops','user_regions'));
	}
    public function getRegionFarmers($region_id){
        $farmers=Farmer::select(
            [
                'farmers.id', 'farmers.name as farmer_name', 'farmers.phone', 'farmers.gender', 'farmers.acreage','farmers.excel_id'
            ]
        )->where('region_id','=',$region_id)->orderBy('id','desc');


        return \yajra\Datatables\Datatables::of($farmers)->make(true);

    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}
    public function addcrop($id){

      try{
          $region = Region::findOrFail($id);
          $crops=Crop::all();
      }catch (ModelNotFoundException $mnfe){
            return Redirect::to('regions')->with('message', 'Sorry, region not found');
      }

        return view('regions.addcrop',compact('region','crops','id'));
    }
    public function storeCrop(Request $request){
        $validator = Validator::make($request->all(), [
            'region_id' => 'required|max:45',
            'duration'=>'required',
            'crop_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $search =DB::table('crop_region')->where('crop_id','=',$request->get('crop_id'))->where('region_id','=',$request->get('region_id'))->first();
        if($search){
            return redirect()->to('regions/view/'.$request->get('region_id'));
        }else{
            $region_details = $request->all();

            CropRegion::create($region_details);
        }




        return Redirect::to('regions/view/'.$request->get('region_id'))->with('message', 'Successfully added region');
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *e
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        try {

            $region = Region::findOrFail($id);

        } catch (ModelNotFoundException $mnfe) {
            return redirect()->back();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $region->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        return redirect()->back();
	}
   public function  removeCrop($id){
       try {
           $crop_region=CropRegion::find($id);
       } catch (ModelNotFoundException $mnfe) {
           return redirect()->back();
       }

       DB::statement('SET FOREIGN_KEY_CHECKS=0;');
       $crop_region->delete();
       DB::statement('SET FOREIGN_KEY_CHECKS=1;');
       return redirect()->back();
   }

}
