<?php namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PatnerRegions;
use App\User_patners;
use App\UserRegions;
use Cartalyst\Sentry\Users\Kohana\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Sentinel\Repositories\User\SentinelUserRepositoryInterface;
use Sentinel\DataTransferObjects\BaseResponse;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Client;
use Vinkla\Hashids\HashidsManager;

class PatnerUsersController extends Controller {

	public function __construct(SentinelUserRepositoryInterface $userRepository,HashidsManager $hashids) {
		$this->userRepository = $userRepository;
		$this->hashids = $hashids;

	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$users       = $this->userRepository->all();

		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}

		$users=DB::table('user_patners')
			    ->leftJoin('users','users.id','=','user_patners.user_id')
			    ->leftJoin('departments','user_patners.department_id','=','departments.id')
			    ->select('users.*','departments.names as department_name')
			    ->orderBy('order','asc')
				->where('user_patners.patner_id',$partner_id)->get();

		


		$departments="";


		return view('patner-users.index',compact('users','departments'));
	}
	public function getAll(){
		$users= $this->userRepository->all();

		return view('sentinel.users.index',compact('users'));


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$departments=Department::where('patner_id',$partner_id)->get();
		return view('patner-users.create',compact('departments'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		//
		// Gather input
		$data = Input::all();
		if($data['name']){
			$names_array=explode(' ',$data['name']);

			if(count($names_array)>2){

				$data['first_name']=$names_array[0];
				$data['last_name']=$names_array[1];
				$data['username']=$data['first_name'].'_'.$data['last_name'];
			}else{
				$data['first_name']=$data['name'];
				$data['username']=$data['name'];
			}


		}
		$user_patner=new User_patners();

		// Attempt Registration
		$result = $this->userRepository->store($data);
		$user=self::getPayload($result);

		if($data['role']=="admin"){
			$groups['Partner_Admin']=1;
			$user_patner->patner_id=$partner_id;

			
		}elseif($data['role']="department"){
			$user_patner->patner_id=$partner_id;
			$user_patner->department_id=$data['department_id'];
			$groups['Partner_User']=1;

		}
		$user_patner->user_id=$user->id;
		$user_patner->save();
		$result = $this->userRepository->changeGroupMemberships($user->id, $groups);
		
		
		//update membership

		return redirect('patner-users');

	}
	public function editUser($id){
		$user = $this->userRepository->retrieveById($id);
		

		$user_groups=DB::table('users_groups')->where('user_id',$id)->get();
		$groups=DB::table('groups')->get();

		$user_patner=User_patners::where('user_id',$id)->first();
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		$departments=Department::where('patner_id',$partner_id)->get();

		return view('patner-users.edit',compact('departments','user_patner','groups','user_groups','user'));



	}
	public function  updateUser(Request $request){
		$partner_id=session('patner_id');
		if(!$partner_id){
			return redirect('dashboard/patner-intro');
		}
		//
		// Gather input
		$data = Input::all();

		if($data['name']){
			$names_array=explode(' ',$data['name']);

			if(count($names_array)>2){

				$data['first_name']=$names_array[0];
				$data['last_name']=$names_array[1];
				$data['username']=$data['first_name'].'_'.$data['last_name'];
			}else{
				$data['first_name']=$data['name'];
				$data['username']=$data['name'];
			}


		}

		$data['id'] = $this->hashids->decode($request->get('hash'))[0];

		// Attempt to update the user
		$result = $this->userRepository->update($data);


		$user=self::getPayload($result);

		$user_patner=User_patners::where('user_id',$user->id)->first();


		if($data['role']=="admin"){
			$groups['Partner_Admin']=1;


		}elseif($data['role']="department"){
			$user_patner->patner_id=$partner_id;
			$user_patner->department_id=$data['department_id'];
			$user_patner->save();
			$groups['Partner_User']=1;

		}


		$result = $this->userRepository->changeGroupMemberships($user->id, $groups);



		//update membership

		return redirect('patner-users');

	}
	public function getPayload(BaseResponse $response){
		$res=$response->getPayload();
		return $res['user'];
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// Decode the hashid
		if(!$id){
			return redirect('patner-users');
		}

		//DB::table('user_patners')->where('user_id',$id)->delete();


		// Remove the user from storage
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('user_regions')->where('user_id',$id)->delete();
		$result = $this->userRepository->destroy($id);
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		return redirect('patner-users');
	}

	public function manageRegions($id){
		if(!$id){
			return redirect('patner-users');
		}
		$user_patner=User_patners::where('user_id',$id)->first();

		$regions=PatnerRegions::leftJoin('regions','regions.id','=','patner_regions.region_id')
			-> where('patner_id',$user_patner->patner_id)->select('regions.*')->get();
		
		$user_regions=DB::table('user_regions')->where('user_id',$id)->get();



		$user = $this->userRepository->retrieveById($id);

		return view('patner-users.manage_regions',compact('user','regions','user_regions'));

	}
	public function storeUserRegions(Request $request){

		 $regions=$request->get('region');
		 
		 if(isset($regions)){
			 DB::table('user_regions')->where('user_id',$request->get('user_id'))->delete();
			foreach ($regions as $region){
				DB::table('user_regions')->insert(['region_id'=>$region,'user_id'=>$request->get('user_id')]);
			}
		 }else{
			 return redirect()->back()->with('Message','Please select a regions');
		 }
		return redirect('patner-users/manage-regions/'.$request->get('user_id'))->with('message','User Regions updated');
	}

}
