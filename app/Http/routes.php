<?php
Route::group(["domain" => \Illuminate\Support\Facades\Request::server('SERVER_NAME')], function () {
    Route::group(['before' => 'Sentinel\auth'], function () {

        Route::get('switcher', array('as' => 'home', 'uses' => 'DashController@switcher'));
    });

    Route::get('/', function () {
        if (!Sentry::check()) {

            $farmers=\App\Farmer::all()->count();
            $crops=\App\Crop::all()->count();
            $agronomists="";


            return view('landing',compact('farmers','crops','agronomists'));

        } elseif (Sentry::check()) {
            return Redirect::to('switcher');
        }
    });

});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('terms', 'TermsController@index');
Route::resource('ussd', 'UssdController@index');
Route::resource('receiver', 'IpnController@index');


    Route::get('dashboard','DashController@index');
    Route::get('farmers/create', 'FarmersController@create');
    Route::get('dash/get-cohorts', 'DashController@getCohorts');
    Route::get('farmers/mass-upload', 'FarmersController@massUpload');
    Route::get('farmers/upload-farmers', 'FarmersController@uploadFarmers');
    Route::post('farmers/store-farmers', 'FarmersController@storeUpload');
    Route::get('farmers/edit/{id}', 'FarmersController@edit');
    Route::resource('farmers/update', 'FarmersController@update');
    Route::post('farmers/store', 'FarmersController@store');
    Route::get('farmers', 'FarmersController@index');
    Route::get('farmers/get-excel-farmers', 'FarmersController@getExcelFarmers');
    Route::get('farmers/get-farmers', 'FarmersController@getFarmers');
    Route::get('regions/get-region-farmers/{id}', 'RegionsController@getRegionFarmers');
    Route::resource('farmers/destroy', 'FarmersController@destroy');

    Route::resource('agronomists/destroy', 'AgronomistsController@destroy');
    Route::resource('agronomists/update', 'AgronomistsController@update');
    Route::get('agronomists/edit/{id}', 'AgronomistsController@edit');
    Route::get('agronomists/view/{id}', 'AgronomistsController@show');
    Route::get('/agronomists/delete/{id}', 'AgronomistsController@destroy');

    Route::get('agronomists/create', 'AgronomistsController@create');
    Route::post('agronomists/store', 'AgronomistsController@store');
    Route::post('agronomists/store-region', 'AgronomistsController@storeRegion');
    Route::get('agronomists', 'AgronomistsController@index');
    Route::get('agronomists/add-region/{hash}','AgronomistsController@addRegions');

    Route::get('crops', 'CropsController@index');
    Route::post('crops/get-cohorts', 'CropsController@getCohorts');
    Route::get('crops/get-crops', 'CropsController@getCrops');
    Route::get('crops/create', 'CropsController@create');
    Route::post('crops/store', 'CropsController@store');
    Route::get('crops/view/{crop_id}', 'CropsController@show');
    Route::get('crops/edit/{crop_id}', 'CropsController@edit');
    Route::resource('crops/update', 'CropsController@update');
    Route::resource('crops/destroy', 'CropsController@destroy');

    Route::get('regions', 'RegionsController@index');
    Route::get('regions/get-regions', 'RegionsController@getRegions');
    Route::post('regions/get-crops', 'RegionsController@getCrops');

    Route::get('regions/create', 'RegionsController@create');
    Route::get('regions/addcrop/{id}', 'RegionsController@addcrop');
    Route::post('regions/store', 'RegionsController@store');
    Route::get('regions/manage', 'RegionsController@manage');
    Route::post('regions/manage-store', 'RegionsController@manageStore');
    Route::post('regions/record-crop', 'RegionsController@storeCrop');
    Route::get('regions/remove-crop/{id}', 'RegionsController@removeCrop');
    Route::get('regions/view/{crop_id}', 'RegionsController@show');
    Route::get('regions/edit/{crop_id}', 'RegionsController@edit');
    Route::get('regions/migrate', 'RegionsController@migrate');
    Route::post('regions/migrate-store','RegionsController@migrateStore');
    Route::put('regions/update/{crop_id}', 'RegionsController@update');
    Route::resource('regions/destroy', 'RegionsController@destroy');

   // Route::get('dashboard', 'DashController@index');

    Route::get('messages', 'MessagesController@index');
    Route::post('messages/store', 'MessagesController@store');
    Route::get('messages/sent', 'MessagesController@sent');
    Route::post('messages/send-to-farmers', 'MessagesController@sendFarmers');
    Route::get('messages/get-sent','MessagesController@getSent');
    Route::get('messages/get-inbox','MessagesController@getInbox');
    Route::get('messages/get-activity-messages','MessagesController@getActivityMessages');
    Route::get('messages/compose', 'MessagesController@compose');
    Route::post('messages/save', 'MessagesController@sendMessage');
    Route::post('messages/confirm', 'MessagesController@confirmMessage');
    Route::get('messages/activity-messages', 'MessagesController@activityMessages');
    Route::post('messages/send-to-farmdrive', 'MessagesController@sentToFarmDrive');
    Route::get('messages/reply/{phone}', 'MessagesController@reply');
    Route::get('messages/view-sent/{uniq}', 'MessagesController@viewSent');
    Route::post('messages/sentto', 'MessagesController@sentto');

    Route::get('cohorts/{id}', 'CohortsController@index');
    Route::get('cohorts/edit/{id}', 'CohortsController@edit');
    Route::get('cohorts/delete/{id}', 'CohortsController@delete');
    Route::resource('cohorts/update', 'CohortsController@update');
    Route::resource('cohorts/destroy', 'CohortsController@destroy');
    Route::get('cohorts/add-message/{id}', 'CohortsController@addMessage');
    Route::post('cohorts/store-message', 'CohortsController@storeMessage');


    Route::post('cohorts/store', 'CohortsController@store');
    Route::get('cohorts/sent', 'CohortsController@sent');
    Route::get('cohorts/create/{id}', 'CohortsController@create');
    Route::get('cohorts/view/{id}', 'CohortsController@show');
    Route::get('cohorts/add/{id}', 'CohortsController@add');
    Route::get('cohorts/addfarmer/{id}/{cohort_id}', 'CohortsController@addfarmer');
    Route::get('cohort-message/edit/{id}', 'CohortsController@editMessage');
    Route::resource('cohorts/update-message', 'CohortsController@updateMessage');
    Route::get('cohorts/remove-message/{id}', 'CohortsController@removeMessage');
    Route::get('cohorts/send-message/{id}', 'CohortsController@sendMessage');


    Route::get('rounds/{id}', 'RoundsController@index');
    Route::get('cohorts', 'RoundsController@getCropsCohorts');
    Route::get('view-harvest-dates', 'RoundsController@filterHarvestDates');
    Route::post('filter-harvest-dates', 'RoundsController@filterWeek');
     Route::get('crop-cohorts/all/{id}', 'RoundsController@all');
    Route::get('get-rounds/{id}', 'RoundsController@getRounds');
    Route::get('all-new', 'RoundsController@allNew');
    Route::post('rounds/store-cohort', 'RoundsController@storeCohort');
    Route::post('rounds/create-cohort', 'RoundsController@addCohort');


    Route::get('rounds/view/{id}','RoundsController@show');
    Route::get('rounds/close-cohort/{id}/{state}','RoundsController@closeCohort');
    Route::get('rounds/edit/{id}','RoundsController@edit');
    Route::resource('rounds/update', 'RoundsController@update');
    Route::resource('rounds/destroy', 'RoundsController@destroy');
    Route::post('rounds/store', 'RoundsController@store');
    Route::get('rounds/create/{id}', 'RoundsController@create');
    Route::get('rounds/add/{id}', 'RoundsController@add');
    Route::post('rounds/store-farmer', 'RoundsController@storeFarmers');
    Route::get('rounds/activities/{id}', 'CohortsController@index');
    Route::get('rounds/comments/{id}','RoundsController@comments');
    Route::get('round_comment/new/{id}','RoundsController@newComment');
    Route::post('round/store-comment','RoundsController@storeComment');
    Route::get('round_comments/destroy/{id}','RoundsController@deleteComment');
    Route::get('rounds/editfarmer/{id}', 'RoundsController@editfarmer');
    Route::post('rounds/storefarmerdetails','RoundsController@storeFarmerDetails');
    Route::get('rounds/remove-farmer/{farmer_id}/{round_id}','RoundsController@removeFarmer');
    Route::get('rounds/send-message/{phone}/{round_id}','RoundsController@sendMessage');
    Route::post('rounds/send','RoundsController@send');



    Route::get('messages/send','MessagesController@send');
    //Route::get('messages/cron','MessagesController@cron');






Route::get('emails/verified/{code}',
    'Auth\AuthController@activateAccount');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::resource('farmdrive', 'ApiController');

Route::post('farmdrive/test', 'ApiController@test');

Route::post('api/recieve','ApiController@reciever');
Route::any('api/recieve-sync','ApiController@syncReceiver');
Route::get('api/send','ApiController@sender');
Route::get('messages/cron','MessagesController@cron');
Route::post('sms-reciever','SmsController@index');


/// new Routes for the new dashboard


Route::get('dashboard/patner-intro','DashController@patnerIntro');
Route::get('patner/create','PatnerController@create');
Route::post('patners/store','PatnerController@store');
Route::get('patner/view/{id}','PatnerController@show');
Route::get('departments','DepartmentController@index');
Route::get('departments/edit/{id}','DepartmentController@edit');
Route::get('departments/create','DepartmentController@create');
Route::post('departments/store','DepartmentController@store');
Route::post('departments/update','DepartmentController@storeUpdate');
Route::get('departments/delete/{id}','DepartmentController@destroy');

Route::get('value-chains','ValuechainController@index');
Route::get('value-chains/edit/{id}','ValuechainController@edit');
Route::post('value-chains/update','ValuechainController@storeUpdate');
Route::get('value-chains/create','ValuechainController@create');
Route::post('value-chains/store','ValuechainController@store');
Route::get('value-chains/delete/{id}','ValuechainController@destroy');
Route::get('patner-cohorts','PatnerCohortsController@index');

Route::get('patner-cohort/view/{id}','PatnerCohortsController@show');
Route::get('patner-cohort/edit/{id}','PatnerCohortsController@edit');
Route::get('patner-cohorts/create','PatnerCohortsController@create');
Route::post('patner-cohorts/store','PatnerCohortsController@store');
Route::post('patner-cohorts/edit-cohort','PatnerCohortsController@editCohort');
Route::get('patner-cohorts-measurables/{id}','PatnerCohortsController@measurables');
Route::get('patner-measurables/create/{id}','PatnerCohortsController@addMeasurables');
Route::post('patner-measurables/store','PatnerCohortsController@storeMeasurables');
Route::get('patner-cohorts/send-message/{id}','PatnerCohortsController@sendMessage');
Route::post('patner-cohorts/send-to-farmers','PatnerCohortsController@StoreMessage');
Route::get('patner-cohorts-excel-farmers','FarmersController@ImportFarmers');
Route::post('patner-cohorts/store-excel-farmers','FarmersController@storeExcelFarmers');

Route::get('patner-users','PatnerUsersController@index');
Route::get('all-users','PatnerUsersController@getAll');
Route::get('patner-cohorts/view/{id}','PatnerUsersController@show');
Route::get('patner-cohorts/edit/{id}','PatnerUsersController@edit');
Route::get('patner-users/create','PatnerUsersController@create');
Route::post('patner-users/store','PatnerUsersController@store');
Route::get('patner-users/edit/{id}','PatnerUsersController@editUser');
Route::post('patner-users/update-user','PatnerUsersController@updateUser');
Route::get('patner-users/delete/{id}','PatnerUsersController@destroy');
Route::get('patner-users/manage-regions/{id}','PatnerUsersController@manageRegions');
Route::post('patner-users/store-user-regions','PatnerUsersController@storeUserRegions');

Route::get('patner-farmers','FarmersController@getPartnerFarmers');
Route::get('patner-cohorts-add-farmers/{id}','PatnerCohortsController@addPartnerFarmers');
Route::get('patner-cohort/remove-farmer/{id}/{cohort_id}','PatnerCohortsController@removePartnerFarmers');
Route::get('patner-cohort/editfarmer/{id}/{cohort_id}','PatnerCohortsController@editPartnerFarmer');
Route::post('patner-cohort/store-farmer-details','PatnerCohortsController@storeFarmerDetails');

Route::post('patner-cohorts/store-farmer','PatnerCohortsController@storePatnerFarmers');
Route::get('patner-add-regions','FarmersController@addRegions');
Route::post('patner-add-active-regions','FarmersController@storeActiveRegions');


Route::get('patner-regions','RegionsController@patnerRegions');
Route::get('regions/create-patner-regions','RegionsController@createPatnerRegion');
Route::post('regions/store-patner-regions','RegionsController@StorePatnerRegion');
