<?php


Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', url('dashboard'));
});
Breadcrumbs::register('regions', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Regions', url('regions'));
});
Breadcrumbs::register('farmers', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('farmers', url('farmers'));
});
Breadcrumbs::register('agronomists', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('agronomists', url('agronomists'));
});
Breadcrumbs::register('crops', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('crops', url('crops'));
});
Breadcrumbs::register('messages', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('messages', url('messages'));
});

Breadcrumbs::register('region', function($breadcrumbs, $region) {
    $breadcrumbs->parent('regions');
    $breadcrumbs->push($region->name, url('/regions/view',$region->id));
});
 Breadcrumbs::register('crop', function($breadcrumbs, $region,$crop) {

    $breadcrumbs->parent('region',$region);
    $breadcrumbs->push($crop->name, url('/rounds',$crop->id));
});
Breadcrumbs::register('cohort', function($breadcrumbs,$region,$crop,$round) {

    $breadcrumbs->parent('crop',$region,$crop);
    $breadcrumbs->push($round->name, url('/rounds/view',$round->id));
});


