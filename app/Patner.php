<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Patner extends Model {

	//
    protected $fillable=['name','phone','description','category'];

}
