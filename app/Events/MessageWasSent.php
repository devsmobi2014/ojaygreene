<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class MessageWasSent extends Event {

	use SerializesModels;
    public $messages;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($messages)
	{
		//
        $this->messages=$messages;
	}

}
