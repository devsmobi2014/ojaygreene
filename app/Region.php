<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {

	//
    protected $fillable =
        [
            'name',
            'description',
            'status'
        ];
    public function farmers()
    {
        return $this->hasMany('App\Farmers');
    }



}
