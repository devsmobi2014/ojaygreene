<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'phone_no', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    /**
     * @param $code
     * @return bool
     */
    public function accountIsActive($code) {
		$user = User::where('verification_code', '=', $code)->first();
		$user->confirmed = 1;
		$user->verification_code = '';
		if ($user->save()) {
			\Auth::login($user);
		}

		return true;
	}

    /**
     * @return HasMany
     */
    public function dash()
    {
        return $this->hasMany('App\Dash');
    }

    /**
     * @param $phone
     */
    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone_no'] = preg_replace("/[^0-9]/","",$phone);
    }

}
