<?php namespace App\Listeners;

use App\Events\MessageWasSent;

use App\MessageSender;
use App\Outbox;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendMessages {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  MessageWasSent  $event
	 * @return void
	 */
	public function handle(MessageWasSent $event)
	{
		//
        if($event->messages){
            foreach($event->messages as $message){
                $phone='254'.substr($message->phone,-9);

				$results=MessageSender::send($phone,$message->message);


                $msg=Outbox::find($message->id);
				foreach($results as $result) {
                // Note that only the Status "Success" means the message was sent
//				echo " Number: " .$result->number;
//				echo " Status: " .$result->status;
//				echo " MessageId: " .$result->messageId;
//				echo " Cost: "   .$result->cost."\n";
					if($result->status=="Success"){
						$msg->status=1;
					}else{
						$msg->status=2;
					}
					$msg->save();
                }

            }
        }

	}

}
