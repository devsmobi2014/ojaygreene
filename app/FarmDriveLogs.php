<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmDriveLogs extends Model
{

    protected $table = 'farm_drive_logs';
    protected $fillable =
        [
            'response'
        ];

}
