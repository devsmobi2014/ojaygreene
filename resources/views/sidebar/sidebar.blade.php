        <div class="col-sm-3" id="sidebar">

            <h4><i class="glyphicon glyphicon-wrench"></i> Tools</h4>

            <ul class="nav nav-stacked list-unstyled">

                <li class="nav-header"><a href="#" data-toggle="collapse" data-target="#menuOne">USSD Testbed <i class="glyphicon glyphicon-chevron-down"></i></a>
                    <ul class="nav nav-stacked collapse in" id="menuOne">
                        <li class="item"><a href="#">My Extensions</a></li>
                    </ul><!-- nav-stacked -->
                </li><!-- nav-header -->

                <li class="nav-header"><a href="#" data-toggle="collapse" data-target="#menuOne">USSD Testbed <i class="glyphicon glyphicon-chevron-down"></i></a>
                    <ul class="nav nav-stacked collapse in" id="menuOne">
                        <li class="item"><a href="/crops">Crops</a></li>
                        {{--<li class="item"><a href="#">Item Two</a></li>--}}
                        {{--<li class="item"><a href="#">Item Three</a></li>--}}
                    </ul><!-- nav-stacked -->
                </li><!-- nav-header -->
                <li class="nav-header"><a href="#" data-toggle="collapse" data-target="#">USSD Testbed <i class="glyphicon glyphicon-chevron-down"></i></a>
                    <ul class="nav nav-stacked collapse in" id="menuOne">
                        <li class="item"><a href="/regions">Regions</a></li>
                        {{--<li class="item"><a href="#">Item Two</a></li>--}}
                        {{--<li class="item"><a href="#">Item Three</a></li>--}}
                    </ul><!-- nav-stacked -->
                </li><!-- nav-header -->

                <li class="nav-header"><a href="#" data-toggle="collapse" data-target="#menuTwo">Billing <i class="glyphicon glyphicon-chevron-right"></i></a>
                    <ul class="nav nav-stacked collapse" id="menuTwo">
                        <li class="item"><a href="#">Account balance</a></li>
                        {{--<li class="item"><a href="#">Item Two</a></li>--}}
                        {{--<li class="item"><a href="#">Item Three</a></li>--}}
                    </ul><!-- nav-stacked -->
                </li><!-- nav-header -->

                <li class="nav-header"><a href="#" data-toggle="collapse" data-target="#menuThree">API/Docs <i class="glyphicon glyphicon-chevron-right"></i></a>
                    <ul class="nav nav-stacked collapse" id="menuThree">
                        <li class="item"><a href="#">Sample USSD Apps</a></li>
                        {{--<li class="item"><a href="#">Item Two</a></li>--}}
                        {{--<li class="item"><a href="#">Item Three</a></li>--}}
                    </ul><!-- nav-stacked -->
                </li><!-- nav-header -->

            </ul><!-- nav -->


        </div><!-- end of col-sm-3 -->

