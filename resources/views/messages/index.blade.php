@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('messages') !!}
            </div>

            <div class="clearfix"></div>
        </div>
<div class="page-content">
    <div class="row">
        <div class="col-sm-3 col-md-2"></div>
        <div class="col-sm-9 col-md-10">
            <div class="pull-right">

                <div class="btn-group mlm">
                    {{--<button type="button" class="btn btn-default"><span--}}
                                {{--class="glyphicon glyphicon-chevron-left"></span></button>--}}
                    {{--<button type="button" class="btn btn-default"><span--}}
                                {{--class="glyphicon glyphicon-chevron-right"></span></button>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="mtl mbl"></div>
    <div class="row">
        <div class="col-sm-3 col-md-2"><a href="{{url('messages/compose')}}" role="button" class="btn btn-danger btn-sm btn-block">COMPOSE</a>

            <div class="mtm mbm"></div>
            <div class="panel">
                <div class="panel-body pan">
                    <ul style="background: #fff" class="nav nav-pills nav-stacked">
                        <li class="active"><a href="{{url('messages')}}"><i class="fa fa-inbox fa-fw mrs"></i>Inbox</a></li>
                        <li><a href="/messages/sent"><i class="fa fa-plane fa-fw mrs"></i>Sent Messages</a></li>
                        <li><a href="/messages/activity-messages"><i class="fa fa-plane fa-fw mrs"></i>Activity  Messages</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="col-sm-9 col-md-10">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab"><span
                                class="glyphicon glyphicon-inbox"></span>&nbsp;
                        Inbox</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="list-group mail-box table-responsive">

                        <table class="table" id="inbox">
                            <thead>
                            <tr class="filters">

                                <th>Farmers Name</th>
                                <th>Phone number</th>
                                <th>Response</th>
                                <th>Cohort </th>
                                <th>Region</th>
                                <th>Created at</th>
                                <th>Reply</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
</div>
    @stop
@section('js')
    $(function() {
    $('#inbox').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": true,
    ajax: '{{ url('/messages/get-inbox') }}',
    "columns": [
    {
    "data": "name",
    "name":"farmers.name",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "phone",
    "name":"inbox.phone",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "response",
    "name":"inbox.response",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "round_name",
    "name":"rounds.name",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "region_name",
    "name":"regions.name",
    searchable: false,
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "created_at",
    "name":"inbox.created_at",
    searchable: false,
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "phone",
    "name":"inbox.phone",
    "render": function(data, type, full, meta) {
    return '<a href="/messages/reply/'+ full.phone +'" class="btn btn-warning btn-outlined btn-sm">Reply</a>';
    },
    "defaultContent": "<i>Not set</i>"
    },
    ]
    });
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.messages').addClass("active");

@endsection
