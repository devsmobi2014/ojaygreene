@extends('layouts.master')


@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-sm-3 col-md-2"></div>
            <div class="col-sm-9 col-md-10">
                <div class="pull-right">

                    <div class="btn-group mlm">
                        {{--<button type="button" class="btn btn-default"><span--}}
                                    {{--class="glyphicon glyphicon-chevron-left"></span></button>--}}
                        {{--<button type="button" class="btn btn-default"><span--}}
                                    {{--class="glyphicon glyphicon-chevron-right"></span></button>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="mtl mbl"></div>
        <div class="row">
            <div class="col-sm-3 col-md-2"><a href="{{url('messages/compose')}}" role="button" class="btn btn-danger btn-sm btn-block">COMPOSE</a>

                <div class="mtm mbm"></div>
                <div class="panel">
                    <div class="panel-body pan">
                        <ul style="background: #fff" class="nav nav-pills nav-stacked">
                            <li><a href="{{url('/messages')}}"><i class="fa fa-inbox fa-fw mrs"></i>Inbox</a></li>
                            <li><a href="/messages/sent"><i class="fa fa-plane fa-fw mrs"></i>Sent Messages</a></li>
                            <li class="active"><a href="/messages/sent"><i class="fa fa-plane fa-fw mrs"></i>Activity  Messages</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-sm-9 col-md-10">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab"><span
                                    class="glyphicon glyphicon-inbox"></span>&nbsp;
                            Activity messages</a></li>
                </ul>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class="list-group mail-box">
                            <table class="table" id="activity_messages">
                                <thead>
                                <tr class="filters">
                                    <th>Activity Name</th>
                                    <th>Title</th>
                                    <th>Message</th>
                                    <th>Region</th>
                                    <th>Crop</th>
                                    <th>Round</th>
                                    <th>Actions</th>

                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    </div>
@stop
@section('js')
    $(function() {
    $('#activity_messages').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": false,
    ajax: '{{ url('/messages/get-activity-messages') }}',
    "columns": [
    {
    "data": "cohort_name",
    "name": "cohorts.name",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "title",
    "name":"cohort_messages.title",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "message",
    "name":"cohort_messages.message",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "region_name",
    "name":"regions.name",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "crop_name",
    "name":"crops.name",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "round_name",
    "name":"rounds.name",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "id",
    "name": "cohort_messages.id",
    "render": function(data, type, full, meta) {

    return '<a href="/cohorts/view/'+ full.cohort_id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-eye"></i></a><a href="/cohorts/edit/'+ full.id +'"  class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-pencil"></i></a>';
    },
    "defaultContent": "<i>Not set</i>"
    },

    ]
    });
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.messages').addClass("active");


@endsection
