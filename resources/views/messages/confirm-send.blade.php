@extends('layouts.master')
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><a href="/"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                            <li><h4>Recipients</h4></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-8">
                                    <div class="panel panel-red">
                                        @if (session('message'))
                                            <div class="alert alert-success">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <div class="panel-body pan">
                                            <form  method="post" action="/messages/send-to-farmdrive">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="message" value="{{$message}}">
                                                <input type="hidden" name="farmers" value="{{ json_encode($farmer_array) }}">
                                                <div class="form-body pal">
                                                    <div class="form-group"><label class="control-label">Message</label><textarea
                                                                rows="3"  name="message" class="form-control">{{$message}}</textarea></div>
                                                    <div class="row">
                                                        <div class="panel-heading">Recipients: {{count($farmer_array)}} farmers</div>
                                                        <table class="table">
                                                            <thead>
                                                            <th>name</th>
                                                            <th>Phone</th>
                                                            </thead>
                                                            <tbody>
                                                            @if($farmer_array)
                                                                <?php $n=0; ?>
                                                                <div class="form-group">
                                                                    @foreach($farmer_array as $farmer)
                                                                        <?php $n++?>
                                                                        <tr>
                                                                            <td>{{$farmer->name}}</td>
                                                                            <td>{{$farmer->phone}}</td>

                                                                        </tr>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="submit" class="btn btn-red">Send Message</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    $(function() {
    $('#region').on('change',function(){
    $('#crops').empty()
    $('#cohorts').append($("<option></option>").attr("value","").text("Choose time cohort"));
    var formData = {
    'region_id': $('#region').val(),
    };

    $.ajax({
    url: '{{ url('/regions/get-crops') }}',
    type: 'post',
    dataType: 'json',
    data:formData,
    success: function (data) {
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    if(data.length===0){
    $('#crops').append($("<option></option>").attr("value","").text("Not added crops to this region"));
    }else{
    $('#crops').append($("<option></option>").attr("value","").text("Choose crop"));
    $.each(data, function(key, value) {
    console.log(value.id, value.name);
    $('#crops').append($("<option></option>").attr("value",value.id).text(value.name));
    });
    }
    },
    });


    });
    $('#crops').on('change',function(){
    $('#cohorts').empty()
    var formData = {
    'crop_id': $('#crops').val(),
    'region_id': $('#region').val(),
    };


    $.ajax({
    url: '{{ url('/crops/get-cohorts') }}',
    type: 'post',
    dataType: 'json',
    data:formData,
    success: function (data) {
    if(data.length===0){
    $('#cohorts').append($("<option></option>").attr("value","").text("Not added cohorts for the crop selected"));
    }else{
    $('#cohorts').append($("<option></option>").attr("value","").text("Choose time cohort"));
    $.each(data, function(key, value) {
    console.log(value.id, value.name);
    $('#cohorts').append($("<option></option>").attr("value",value.id).text(value.name));
    });
    }
    },
    });


    });
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.messages').addClass("active");

@endsection
