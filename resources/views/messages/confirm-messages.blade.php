@extends('layouts.master')
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><a href="/"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                            <li><h4>Choose farmers</h4></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-8">
                                    <div class="panel panel-red">
                                        @if (session('message'))
                                            <div class="alert alert-success">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <div class="panel-body pan">
                                            <form  method="post" action="/messages/send-to-farmers">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="target" value="{{$target}}">
                                                <div class="form-body pal">
                                                    <div class="form-group"><label class="control-label">Message</label><textarea
                                                                rows="3"  name="message" required="" class="form-control">{{$message}}</textarea></div>
                                                    <div class="row">
                                                        <div class="col-lg-2"></div>
                                                        <div class="col-lg-8">
                                                        <div class="panel-heading">Choose farmers</div>
                                                            <div class="form-group">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" name="farmers_type" id="farmer_type1" value="all" >
                                                                        All Farmers
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" name="farmers_type" id="farmer_type2" value="select" >
                                                                        Choose farmers(please select the farmers)
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="farmers_select" style="display: none">
                                                            <table id="farmers_selected"  class="table">
                                                                <thead>
                                                                <th>Select farmer</th>
                                                                <th>name</th>
                                                                <th>Phone</th>
                                                                </thead>
                                                                <tbody>
                                                                @if($farmers)
                                                                <?php $n=0; ?>
                                                                <div class="form-group">
                                                                    @foreach($farmers as $farmer)
                                                                    <?php $n++?>
                                                                    <tr>
                                                                        <td><input class="radio choose"  type="checkbox" value="<?php echo $farmer->id ?>" /></td>
                                                                        <td>{{$farmer->name}}</td>
                                                                        <td>{{$farmer->phone}}</td>

                                                                    </tr>
                                                                    @endforeach
                                                                </div>
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>



                                                        <p style="display:none" id="all_farmers">
                                                            <tr>{{count($farmers)}} farmers will recieve the message</tr>
                                                        </p>
                                                    </div>

                                                </div>
                                                <input id="farmers" type="hidden" name="farmer_id" value="" />
                                                <input id="all_farmers" type="hidden" name="all_farmers" value="{{json_encode($farmers)}}" />
                                                <div class="form-actions text-right pal">
                                                    <button id="subr"type="submit" class="btn btn-red">Send Message</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    $(function() {
   $('farmers_select').dataTable();
    $('#region').on('change',function(){
    $('#crops').empty()
    $('#cohorts').append($("<option></option>").attr("value","").text("Choose time cohort"));
    var formData = {
    'region_id': $('#region').val(),
    };

    $.ajax({
    url: '{{ url('/regions/get-crops') }}',
    type: 'post',
    dataType: 'json',
    data:formData,
    success: function (data) {
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    if(data.length===0){
    $('#crops').append($("<option></option>").attr("value","").text("Not added crops to this region"));
    }else{
    $('#crops').append($("<option></option>").attr("value","").text("Choose crop"));
    $.each(data, function(key, value) {
    console.log(value.id, value.name);
    $('#crops').append($("<option></option>").attr("value",value.id).text(value.name));
    });
    }
    },
    });


    });
    $('#crops').on('change',function(){
    $('#cohorts').empty()
    var formData = {
    'crop_id': $('#crops').val(),
    'region_id': $('#region').val(),
    };


    $.ajax({
    url: '{{ url('/crops/get-cohorts') }}',
    type: 'post',
    dataType: 'json',
    data:formData,
    success: function (data) {
    if(data.length===0){
    $('#cohorts').append($("<option></option>").attr("value","").text("Not added cohorts for the crop selected"));
    }else{
    $('#cohorts').append($("<option></option>").attr("value","").text("Choose time cohort"));
    $.each(data, function(key, value) {
    console.log(value.id, value.name);
    $('#cohorts').append($("<option></option>").attr("value",value.id).text(value.name));
    });
    }
    },
    });


    });
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.messages').addClass("active");

@endsection
