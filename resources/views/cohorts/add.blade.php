@extends('layouts.master')
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
<div class="page-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="portlet box">
                <div class="portlet-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption pull-left">
                                <ul class="list-unstyled list-inline">
                                    <li><a href="region-crop-cohort.html"><i class="fa fa-arrow-circle-left fa-4"></i> </a></li>
                                    <li><h4>Add farmers to this cohort</h4></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row mbm">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <div class="panel panel-red filterable">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{$cohort->name}} cohort</h3>
                                        <div class="pull-right">
                                            <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                        </div>
                                    </div>
                                    <table class="table">
                                        <thead>
                                        <tr class="filters">
                                            <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                            <th><input type="text" class="form-control" placeholder="Farmers Name" disabled></th>
                                            <th><input type="text" class="form-control" placeholder="Phone Number" disabled></th>

                                            <th width="">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($farmers)
                                            <?php $n=0; ?>
                                            @foreach($farmers as $farmer)
                                                <?php $n++?>
                                                <tr>
                                                    <td>{{$n}}</td>
                                                    <td>{{$farmer->name}}</td>
                                                    <td>{{$farmer->phone}}</td>

                                                    <td>
                                                        <ul class="list-unstyled list-inline">
                                                            <li><a href="{{url('/cohorts/addfarmer',['id'=>$farmer->farmer_id,'cohort_id'=>$cohort->id])}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                            class=" fa fa-plus"></i>&nbsp;
                                                                </a></li>
                                                            <li><a href="edit-farmer.html" class="btn btn-green btn-outlined btn-sm"><i
                                                                            class="fa fa-pencil"></i>&nbsp;
                                                                </a></li>
                                                            <li><a class="btn btn-green btn-outlined btn-sm"><i
                                                                            class="fa fa-trash-o"></i>&nbsp;
                                                                </a></li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                    </table>
                                    <div class="clearfix"></div>
                                    <ul class="pagination pull-right pagination-red ">
                                        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop