@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

    <div class="page-content">
        <div class="row">
            <div class="col-lg-8">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="caption">Edit a Message</div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">Edit Message</div>
                                    <div class="panel-body pan">
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form class="form-horizontal" role="form" method="PATCH" action="{{url('cohorts/update-message',$message->id)}}">
                                            <div class="form-body pal">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Title</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left"><input
                                                                    id="RegionName"  name="title" value="{{$message->title}}" type="text" placeholder="" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Messages body</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left">
                                                            <textarea class="form-control" name="message">{{$message->message}}" </textarea>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="form-actions pal">
                                                <div class="form-group mbn">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-yellow pull-right">Edit message</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
