@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>{{$cohort->name}} Messages</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{url('cohorts/add-message',$cohort->id)}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;Add message</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">{{$cohort->name}} Cohort Messages</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th>#</th>
                                                <th>Message</th>
                                                <th>Created_at</th>
                                                <th>Status</th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($cohort_messages)
                                                <?php $n=0; ?>
                                                @foreach($cohort_messages as $message)
                                                    <?php $n++?>
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>{{$message->message}}</td>
                                                        <td>{{$message->created_at}}</td>
                                                        <td>@if($message->status==0)
                                                                Pending
                                                            @elseif($message->status==1)
                                                                Sent
                                                            @elseif($message->status==2)
                                                                Stopped
                                                            @endif

                                                        </td>

                                                        <td>
                                                            <ul class="list-unstyled list-inline">

                                                                <li><a href="{{url('cohort-message/edit',$message->id)}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-pencil"></i>&nbsp;
                                                                    </a></li>


                                                                <li>
                                                                    @if($message->status==0)
                                                                        <a href="{{url('cohorts/send-message',$message->id)}}" class="btn btn-green btn-outlined btn-sm">Send to queue &nbsp;
                                                                    </a>
                                                                     @else
                                                                        sent
                                                                    @endif
                                                                </li>
                                                                <li><a href="{{url('cohorts/remove-message',$message->id)}}" class="btn btn-green btn-outlined btn-sm delete"><i
                                                                                class="fa fa-trash-o"></i>&nbsp;
                                                                    </a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        <ul class="pagination pull-right pagination-green">
                                            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop