@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

    <div class="page-content">
        <div class="row">
            <div class="col-lg-8">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="caption">Create an Activity</div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">New Round</div>
                                    <div class="panel-body pan">
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form class="form-horizontal" role="form" method="PATCH" action="{{url('/cohorts/update',$cohort->id)}}">
                                            <div class="form-body pal">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="round_id" value="{{$cohort->round_id}}">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left"><input
                                                                    id="RegionName"  name="name" value="{{$cohort->name}}" type="text" placeholder="" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Start Week(weeks)</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left"><input
                                                                    id="start_date "  name="start_date" value="{{$cohort->start_date}}" type="number" placeholder="" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">End Week(weeks)</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left"><input
                                                                    id="start_date "  value="{{$cohort->end_date}}" name="end_date" type="number" placeholder="" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions pal">
                                                <div class="form-group mbn">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-yellow pull-right">Edit a Cohort</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
