
@if ($errors->any())
	 <ul class="alert alert-danger">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    Whoops, there were problems with your entered fields:<br>
		 @foreach($errors->all() as $error)
			 <li  id="validation_alert">{{ $error }}</li>
		 @endforeach
	 </ul>
@endif