
@if (Session::has('message'))
    <div class="alert alert-info alert-{{ Session::get('message') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        {{ Session::get('message') }}
    </div>
@endif