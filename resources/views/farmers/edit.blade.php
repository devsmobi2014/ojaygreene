@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
    <div class="container" id="dashboard_container">
        <div class="row">
            <br>
            <br>
            <br>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit farmer</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="PATCH" action="{{url('/farmers/update',$farmer->id)}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $farmer->name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Phone Number</label>
                                <div class="col-md-6">
                                    <input type="text"   value="{{ $farmer->phone}}"class="form-control" name="phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Gender</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="gender">
                                        @if($farmer->gender==1)
                                            <option value="1" selected>Male</option>
                                            <option value="2" >Female</option>
                                        @else
                                            <option value="2" selected>Female</option>
                                            <option value="1">Male</option>
                                        @endif
                                    </select>
                               </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Acreage</label>
                                <div class="col-md-6">
                                    <input type="text"   value="{{ $farmer->acreage}}"class="form-control" name="acreage">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Region</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="region_id">
                                        @foreach($regions as $region)
                                            @if($farmer->region_id==$region->id)
                                                <option value="{{$region->id}}" selected>{{$region->name}}</option>
                                            @else
                                                <option value="{{$region->id}}" >{{$region->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                            </div>



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Edit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
        @section('js')
            $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
            $('.farmers').addClass("active");
@endsection
