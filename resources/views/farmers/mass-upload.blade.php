@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('farmers') !!}
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><h4>Farmers</h4></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="new-region pull-right"><a href="{{url('farmers/upload-farmers')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;Upload farmers via excel</a></div>
                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Farmers</h3>
                                                <div class="pull-right">

                                                </div>
                                            </div>
                                            <table id="farmers" class="table">
                                                <thead>
                                                <tr class="filters">

                                                    <th>Title

                                                    <th>Total number of farmers</th>
                                                    <th>

                                                    </th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                   @if($excels)
                                                       @foreach($excels as $excel)
                                                           <tr>
                                                               <td>{{$excel->title}}</td>

                                                               <td>{{$excel->total}}</td>
                                                               <td></td>
                                                           </tr>
                                                           @endforeach
                                                       @endif
                                                </tbody>

                                            </table>
                                            <div class="clearfix"></div>
                                            {{$excels->render()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.farmers').addClass("active");
@endsection
