@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('farmers') !!}
            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                @if (session('message'))
                                    <div class="alert alert-success">
                                        {{ session('message') }}
                                    </div>
                                @endif
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Farmers</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{url('farmers/mass-upload')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-eye"></i>&nbsp;&nbsp;Manage Excel uploads</a></div>
                                <div class="new-region pull-right"><a href="{{url('farmers/create')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-eye"></i>&nbsp;&nbsp;Add Farmer</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Farmers</h3>
                                            <div class="pull-right">

                                            </div>
                                        </div>
                                        <table id="farmers" class="table">
                                            <thead>
                                            <tr class="filters">

                                                <th>Name</th>
                                                <th>Gender</th>
                                                <th>Phone Number</th>
                                                <th>Region</th>
                                                <th>Acreage</th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>

                                        </table>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('js')
    $(function() {
        $('#farmers').DataTable({
            processing: true,
            serverSide: true,
            "bStateSave": true,
            ajax: '{{ url('/farmers/get-farmers') }}',
            "columns": [
            {
                "data": "farmer_name",
                "name": "farmers.name",
                "defaultContent": "<i>Not set</i>",
                "searchable":true,
            },
                {
                "data": "gender",
                "name": "farmers.gender",
                "defaultContent": "<i>Not set</i>",
                "render": function(data, type, full, meta){
                return (data == 1) ? 'Male' : 'Female';
                }
                },
            {
                "data": "phone",
                "name" : "farmers.phone",
                "defaultContent": "<i>Not set</i>"
            },
            {
                "data": "name",
                "name":"regions.name",
                "defaultContent": "<i>Not set</i>",
            },
            {
                "data": "acreage",
                "name":"farmers.acreage",
                "defaultContent": "<i>Not set</i>"
            },
                    {
                    "data": "id", // can be null or undefined
                    "render": function(data, type, full, meta) {
                    console.log(full)

                    return '<a href="/farmers/edit/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-pencil"></i></a><a href="/farmers/destroy/'+ full.id +'" id="delete" class="btn btn-warning btn-outlined btn-sm delete"><i class="fa fa-trash"></i></a>';
                    },
                    "defaultContent": "<i>Not set</i>"
                    },
            ]
        });
    });
    $("#farmers").on("click", ".delete", function(){
    if(confirm("Are you sure you want to delete this farmer")){
    return true;
    }
    return false;
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.farmers').addClass("active");
@endsection