@extends('layouts.master')
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><a href="/"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                            <li><h4>Upload a excel</h4></li>
                                            <li class="new-region pull-right"><a href="{{url('farmers/mass-upload')}}" class="btn btn-green btn-outlined btn-sm ">&nbsp;&nbsp;Back</a></li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-8">
                                    <div class="panel panel-red">
                                        @if (session('message'))
                                            <div class="alert alert-success">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="panel-heading">Upload</div>
                                        <div class="panel-body pan">
                                <!-- BEGIN FORM-->
                                <form method="POST" action="{{url('farmers/store-farmers')}}" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Title</label>
                                            <div class="col-md-4">
                                                <input type="text" value="" name="title" class="form-control input-circle" placeholder="">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Upload Excel</label>
                                            <div class="col-md-4">
                                                <input type="file" value="{{Input::old('excel')}}" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" name="excel" class="form-control input-circle" placeholder="Enter Sacco name">

                                            </div>
                                        </div>


                                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.farmers').addClass("active");
@endsection