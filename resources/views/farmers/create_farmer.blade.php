@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="container" id="dashboard_container">
            <div class="row">
                <br>
                <br>
                <br>
                <div class="col-md-8 col-md-offset-2">
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                    <div class="panel panel-default">
                        <div class="panel-heading">Create farmer</div>
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form class="form-horizontal" role="form" method="POST" action="/farmers/store">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Name</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Phone Number</label>
                                    <div class="col-md-6">
                                        <input type="text"   value=""class="form-control" name="phone_no">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Gender</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="gender">
                                               <option value="" >Select Gender</option>
                                                <option value="1" >Male</option>
                                                <option value="2" >Female</option>

                                        </select>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Region</label>
                                    <div class="col-md-6">
                                        <input type="text"   value=""class="form-control" name="region">
                                    </div>

                                </div>



                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('js')
            $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
            $('.farmers').addClass("active");
@endsection