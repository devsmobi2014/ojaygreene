@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="{{url('departments')}}"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>Edit Department</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">New Department Details</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('departments/update')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-body pal">
                                                <div class="form-group"><label class="control-label"> Name</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" value="{{$department->names}}" name="names" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <input type="hidden" name="department_id" value="{{$department->id}}">

                                                <div class="form-group"><label class="control-label">Description</label><textarea
                                                            rows="3" name="description" class="form-control">{{$department->description}}</textarea></div>

                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Edit Department</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop