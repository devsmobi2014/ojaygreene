@extends('layouts.patner_dash')

@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Departments</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{url('departments/create')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Department</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Departments</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Name" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Description" disabled></th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($departments)
                                                <?php $n=1;?>
                                                @foreach($departments as $department)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>{{$department->names}}</td>
                                                        <td>{{$department->description}}</td>

                                                        <td width="15%">
                                                            <ul class="list-unstyled list-inline">
                                                                <li><a href="{{url('departments/edit',['id'=>$department->id])}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-pencil"></i>&nbsp;
                                                                    </a></li>
                                                                <li><a href="{{url('departments/delete',['id'=>$department->id])}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-trash-o"></i>&nbsp;
                                                                    </a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <?php $n++?>
                                            @endforeach
                                                @endif


                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        <ul class="pagination pull-right pagination-green">
                                            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @stop