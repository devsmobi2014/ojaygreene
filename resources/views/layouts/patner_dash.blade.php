<!DOCTYPE html>
<html lang="en">
<head><title>UstawiHub</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <link rel="shortcut icon" href="/images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="/images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="/css/style-custom.css">
    <link type="text/css" rel="stylesheet" href="/css/orange-blue.css" >
    <link type="text/css" rel="stylesheet" href="/css/style-responsive.css">
    <link type="text/css" rel="stylesheet" href="/css/custom.css">
    <link type="text/css" rel="stylesheet" href="/css/bootstrap-table.min.css">
    <link type="text/css" rel="stylesheet" href="/css/bootstrap-table.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" />


</head>
<body class=" ">
<div>
    <!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP-->
    <!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">

        <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;" class="navbar navbar-default navbar-static-top nav-custom">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span
                            class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
        </nav>
        <!--END TOPBAR-->
        <div id="wrapper"><!--BEGIN SIDEBAR MENU-->
            <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
                <div class="sidebar-collapse menu-scroll">
                    <ul id="side-menu" class="nav">
                        <li class="user-panel">
                            <div class="logo logo-2"><img class="logo" src="/images/logo.png"></div>
                            <div class="clearfix"></div>
                        </li>
                        <li class="{!! Request::is('dashboard*') ? 'start active open' : '' !!}">
                            <a href="{{ url('/dashboard') }}">
                                <span class="title">Dashboard</span>
                                <span class="{!! Request::is('dashboard*') ? 'selected' : '' !!}"></span>
                            </a>
                        </li>
                        {{--<li class="{!! Request::is('regions*') ? 'start active open' : '' !!}">--}}
                            {{--<a href="{{ url('#') }}">--}}
                                {{--<span class="title">Regions</span>--}}
                                {{--<span class="{!! Request::is('regions*') ? 'selected' : '' !!}"></span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        <li class="{!! Request::is('patner-cohorts*') ? 'start active open' : '' !!}">
                            <a href="{{ url('/patner-cohorts') }}">
                                <span class="title">Cohorts</span>
                                <span class="{!! Request::is('patner-cohorts*') ? 'selected' : '' !!}"></span>
                            </a>
                        </li>
                        <li class="{!! Request::is('patner-regions*') ? 'start active open' : '' !!}">
                            <a href="{{ url('patner-regions') }}">
                                <span class="title">Regions</span>
                                <span class="{!! Request::is('patner-regions*') ? 'selected' : '' !!}"></span>
                            </a>
                        </li>

                        <li class="{!! Request::is('patner-farmers*') ? 'start active open' : '' !!}">
                            <a href="{{ url('patner-farmers') }}">
                                <span class="title">Farmers</span>
                                <span class="{!! Request::is('patner-farmers*') ? 'selected' : '' !!}"></span>
                            </a>
                        </li>
                        <li class="{!! Request::is('departments*') ? 'start active open' : '' !!}">
                            <a href="{{ url('/departments') }}">
                                <span class="title">Departments</span>
                                <span class="{!! Request::is('departments*') ? 'selected' : '' !!}"></span>
                            </a>
                        </li>
                        <li class="{!! Request::is('value-chains*') ? 'start active open' : '' !!}">
                            <a href="{{ url('/value-chains') }}">
                                <span class="title">Value Chains</span>
                                <span class="{!! Request::is('value-chains*') ? 'selected' : '' !!}"></span>
                            </a>
                        </li>




                        <li class="{!! Request::is('patner-users*') ? 'start active open' : '' !!}">
                            <a href="{{ url('/patner-users') }}">
                                <span class="title">Users</span>
                                <span class="{!! Request::is('patner-users*') ? 'selected' : '' !!}"></span>
                            </a>
                        </li>
                        <li><a href="#"  ><span class="menu-title">Messaging</span></a></li>
                        @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                            <li class="{!! Request::is('dashboard/patner-intro*') ? 'start active open' : '' !!}">
                                <a href="{{ url('/') }}">
                                    <span class="title">Go back to Patners </span>
                                    <span class="{!! Request::is('dashboard/patner-intro*') ? 'selected' : '' !!}"></span>
                                </a>
                            </li>
                            @endif
                        <!--<li><a href="javascript:;"  ><i class="fa fa-clock-o fa-fw">
                        </i><span class="menu-title">History</span></a></li>-->
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div class="partner-barner">
                    <div id="slider">
                        <figure>
                            <img src="/images/livestock.jpg" alt>
                            <img src="/images/fisheries.jpg" alt>
                            <img src="/images/cooperate.jpg" alt>
                            <img src="/images/livestock.jpg" alt>
                            <img src="/images/fisheries.jpg" alt>
                        </figure>
                    </div>
                </div>

                <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;" class="navbar navbar-default navbar-static-top">

                    <div class="topbar-sub">
                        <div class="pagetitle">{{\App\Patner::find(session('patner_id'))->names}} Dashboard</div>
                        <ul class="nav navbar navbar-top-links navbar-sub navbar-right mbn">
                            <li class="dropdown topbar-user"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i class="fa fa-user"></i>&nbsp;<span class="hidden-xs">Admin(Lamu county)</span>&nbsp;<span
                                            class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-user pull-right">
                                    <li><a href="#"><i class="fa fa-user"></i>My Profile</a></li>
                                    <li><a href="/logout"><i class="fa fa-key"></i>Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </nav>

                <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
               @yield('content')
                <!--END CONTENT--></div>
        </div>
        <!--BEGIN FOOTER-->
        <div id="footer">
            <div class="copyright">2016 © ustawihub</div>
        </div>
        <!--END FOOTER--><!--END PAGE WRAPPER--></div>
</div>
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/js/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
@yield('new-js')
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script>
@yield('js')
</script>

<!--loading bootstrap js-->
<script src="/css/bootstrap/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-table.min.js"></script>
<script src="/css/bootstrap/js/bootstrap-hover-dropdown.js"></script>
<script src="/js/html5shiv.js"></script>
<script src="/js/respond.min.js"></script>
<script src="/js/jquery.metisMenu.js"></script>

<script src="/js/custom.js"></script>
<!--CORE JAVASCRIPT-->
<script src="/js/main.js"></script>


</body>
</html>