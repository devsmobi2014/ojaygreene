<!DOCTYPE html>
<html lang="en">
<head><title>Dashboard</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <link rel="shortcut icon" href="/images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="/images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="/css/orange-blue.css" >
    <link type="text/css" rel="stylesheet" href="/css/chat.css" >
    <link type="text/css" rel="stylesheet" href="/css/bubble.css" >
    <link type="text/css" rel="stylesheet" href="/css/style-responsive.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet">

    <!--HTML5 Shiv-->
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


</head>
<body class=" ">
<div>
    <!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP-->
    <!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar">
        <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;" class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span
                            class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span></button>
                <a id="logo" href="/" class="navbar-brand"><span class="fa fa-rocket"></span>
                    <span class="logo-text"><img class="logo" src="/images/logo.png"></span>
                </a>
            </div>
            <div class="topbar-main"><a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>
                <ul class="nav navbar-nav    ">
                    <li class="active"><a href="/dashboard">Dashboard</a></li>
                    <li><a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle">Layouts
                            &nbsp;<i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="layout-left-sidebar.html">Left Sidebar</a></li>
                            <li><a href="layout-right-sidebar.html">Right Sidebar</a></li>
                            <li><a href="layout-left-sidebar-collapsed.html">Left Sidebar Collasped</a></li>
                            <li><a href="layout-right-sidebar-collapsed.html">Right Sidebar Collasped</a></li>
                            <li class="dropdown-submenu"><a href="javascript:;" data-toggle="dropdown"
                                                            class="dropdown-toggle">More Options</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Second level link</a></li>
                                    <li class="dropdown-submenu"><a href="javascript:;" data-toggle="dropdown"
                                                                    class="dropdown-toggle">More Options</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Third level link</a></li>
                                            <li><a href="#">Third level link</a></li>
                                            <li><a href="#">Third level link</a></li>
                                            <li><a href="#">Third level link</a></li>
                                            <li><a href="#">Third level link</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Second level link</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar navbar-top-links navbar-right mbn">
                    <ul class="nav navbar-nav">
                        @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                            <li {{ (Request::is('users*') ? 'class="active"' : '') }}><a href="{{ action('\\Sentinel\Controllers\UserController@index') }}">Users</a></li>
                            <li {{ (Request::is('groups*') ? 'class="active"' : '') }}><a href="{{ action('\\Sentinel\Controllers\GroupController@index') }}">Groups</a></li>
                        @endif
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if (Sentry::check())
                            <li {{ (Request::is('profile') ? 'class="active"' : '') }}><a href="{{ route('sentinel.profile.show') }}">{{ Session::get('email') }}</a>
                            </li>
                            <li>
                                <a href="{{ route('sentinel.logout') }}">Logout</a>
                            </li>
                        @else
                            <li {{ (Request::is('login') ? 'class="active"' : '') }}><a href="{{ route('sentinel.login') }}">Login</a></li>
                            <li {{ (Request::is('users/create') ? 'class="active"' : '') }}><a href="{{ route('sentinel.register.form') }}">Register</a></li>
                        @endif
                    </ul>
                    <li class="dropdown topbar-user">
                        @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                        <a data-hover="dropdown" href="#" class="dropdown-toggle"><img
                                    src="/images/avatar/48.jpg" alt=""
                                    class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs">Yvette ondachi</span>&nbsp;<span
                                    class="caret"></span></a>
                        @else
                        <a data-hover="dropdown" href="#" class="dropdown-toggle"><img
                                src="/images/avatar/icon-user.png" alt=""
                                class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs">{{\Sentry::getUser()->username}}</span>&nbsp;<span
                                class="caret"></span></a>

                        @endif
                        <ul class="dropdown-menu dropdown-user pull-right">
                            @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                                <li {{ (Request::is('all-users*') ? 'class="active"' : '') }}><a href="{{ url('all-users') }}">Users</a></li>
                                <li {{ (Request::is('groups*') ? 'class="active"' : '') }}><a href="{{ action('\\Sentinel\Controllers\GroupController@index') }}">Groups</a></li>
                            @endif
                                @if (Sentry::check())
                                    <li {{ (Request::is('profile') ? 'class="active"' : '') }}><a href="{{ route('sentinel.profile.show') }}">{{ Session::get('email') }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('sentinel.logout') }}">Logout</a>
                                    </li>
                                @else
                                    <li {{ (Request::is('login') ? 'class="active"' : '') }}><a href="{{ route('sentinel.login') }}">Login</a></li>
                                    <li {{ (Request::is('users/create') ? 'class="active"' : '') }}><a href="{{ route('sentinel.register.form') }}">Register</a></li>
                                @endif

                        </ul>
                    </li>
                </ul>
            </div>
            <div class="collapse navbar-collapse">

            </div><!--/.nav-collapse -->
        </nav>
        <div id="wrapper"><!--BEGIN SIDEBAR MENU-->
            <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
                <div class="sidebar-collapse menu-scroll">
                    <ul id="side-menu" class="nav">
                        <li class="user-panel">
                            <!--<div class="logo"><img src="images/logo.png"></div>-->
                            <div class="clearfix"></div>
                        </li>
                        @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                        <li class="dashboard active"><a href="{{url('/dashboard')}}"><i class="fa fa-tachometer fa-fw">
                                </i><span class="menu-title">Dashboard</span></a></li>
                        <li class="regions"><a  href="{{url('/regions')}}"  ><i class="fa fa-map-marker fa-fw">
                                </i><span class="menu-title">Regions</span></a></li>
                        <li class="cohorts"><a  href="{{url('/cohorts')}}"  ><i class="fa fa-calendar-o">
                                </i><span class="menu-title">Cohorts</span></a></li>
                        <li class="farmers"><a href="{{url('/farmers')}}"  ><i class="fa fa-user fa-fw">
                                </i><span class="menu-title">Farmers</span></a></li>
                        <li class="agronomists"><a href="{{url('/agronomists')}}"  ><i class="fa fa-graduation-cap fa-fw">
                                </i><span class="menu-title">Agronomists</span></a></li>
                        <li class="crops"><a href="/crops"  ><i class="fa fa-tree fa-fw">
                                </i><span class="menu-title">Value Chain</span></a></li>
                        <li class="messages"><a class="badge1" data-badge="{{\App\Inbox::inbox()}}" href="{{url('/messages')}}"  ><i class="fa fa-envelope fa-fw">
                                </i>Messages</a></li>
                        @elseif(Sentry::check() && Sentry::getUser()->hasAccess('agronomist'))
                        <li class="dashboard active"><a href="{{url('/dashboard')}}"><i class="fa fa-tachometer fa-fw">
                                </i><span class="menu-title">Dashboard</span></a></li>
                        <li class="regions"><a  href="{{url('/regions')}}"  ><i class="fa fa-map-marker fa-fw">
                                </i><span class="menu-title">Regions</span></a></li>

                        <li class="farmers"><a href="{{url('/farmers')}}"  ><i class="fa fa-user fa-fw">
                                </i><span class="menu-title">Farmers</span></a></li>

                        <li class="crops"><a href="/crops"  ><i class="fa fa-tree fa-fw">
                                </i><span class="menu-title">Crops</span></a></li>
                        <li class="messages"><a class="badge1" data-badge="{{\App\Inbox::inbox()}}" href="{{url('/messages')}}"  ><i class="fa fa-envelope fa-fw">
                                </i>Messages</a></li>
                        @elseif(Sentry::check() && Sentry::getUser()->hasAccess('logistic'))
                        <li class="dashboard active"><a href="{{url('/dashboard')}}"><i class="fa fa-tachometer fa-fw">
                                </i><span class="menu-title">Dashboard</span></a></li>
                        <li class="regions"><a  href="{{url('/regions')}}"  ><i class="fa fa-map-marker fa-fw">
                                </i><span class="menu-title">Regions</span></a></li>

                        <li class="cohorts"><a  href="{{url('/cohorts')}}"  ><i class="fa fa-calendar-o">
                                </i><span class="menu-title">Cohorts</span></a></li>

                        <li class="crops"><a href="/crops"  ><i class="fa fa-tree fa-fw">
                                </i><span class="menu-title">Crops</span></a></li>

                        @endif
                        <!--<li><a href="javascript:;"  ><i class="fa fa-clock-o fa-fw">
                        </i><span class="menu-title">History</span></a></li>-->
                    </ul>
                </div>
            </nav>

            {{--<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->--}}
                {{--<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">--}}
                    {{--<div class="page-header pull-left">--}}

                    {{--</div>--}}
                    {{--<ol class="breadcrumb page-breadcrumb">--}}
                        {{--<li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i--}}
                                    {{--class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>--}}
                        {{--<li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;--}}
                        {{--</li>--}}
                        {{--<li class="active">Dashboard</li>--}}
                    {{--</ol>--}}
                    {{--<div class="clearfix"></div>--}}
                {{--</div>--}}
                @yield('content')
            </div>
        </div>
        <!--BEGIN FOOTER-->
        <div id="footer">
            <div class="copyright">2016 © &mu;ojaygreene</div>
        </div>
        <!--END FOOTER--><!--END PAGE WRAPPER--></div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
{{--<script src="//code.jquery.com/jquery.js"></script>--}}
<script src="/js/custom/custom.js"></script>
<script>
    @yield('js')
</script>
    @yield('page_scripts');

{{--<script src="/js/jquery-1.10.2.min.js"></script>--}}
{{--<script src="/js/jquery-migrate-1.2.1.min.js"></script>--}}
{{--<script src="/js/jquery-ui.js"></script>--}}
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
{{--<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--}}

<!--loading bootstrap js-->
<script src="/css/bootstrap/js/bootstrap.min.js"></script>
<script src="/css/bootstrap/js/bootstrap-hover-dropdown.js"></script>
{{--<script src="/js/html5shiv.js"></script>--}}
{{--<script src="/js/respond.min.js"></script>--}}
{{--<script src="/js/jquery.metisMenu.js"></script>--}}
{{--<script src="/js/jquery.menu.js"></script>--}}
{{--<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.js"></script>--}}

<!--CORE JAVASCRIPT-->
<!--LOADING SCRIPTS FOR PAGE-->
{{--<script src="/js/flot-chart/jquery.flot.js"></script>--}}
{{--<script src="/js/flot-chart/jquery.flot.categories.js"></script>--}}
{{--<script src="/js/flot-chart/jquery.flot.pie.js"></script>--}}
{{--<script src="/js/charts-flotchart.js"></script>--}}


</body>
</html>