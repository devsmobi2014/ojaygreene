@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {{--{!! Breadcrumbs::render('cohort',$region,$crop,$round) !!}--}}
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><h4>Agronomist profile</h4></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if (Sentry::check() && Sentry::getUser()->hasAccess('admin')) <div class="new-region pull-right"><a href="{{url('agronomists/delete',$user->id)}}" class="btn btn-green btn-outlined btn-sm delete"><i class="fa fa-eye"></i>&nbsp;&nbsp;Remove  the agronomist</a></div> @endif
                                    {{--<div class="new-region pull-right"><a href="{{url('rounds/add',$round->id)}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;Add farmers</a></div>--}}
                                    {{--<div class="new-region pull-right"><a href="{{url('rounds/activities',$round->id)}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-eye"></i>&nbsp;&nbsp;View Activities</a></div>--}}
                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3 class="panel-title">Summary</h3>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Email:</td><td>{{$user->email}}</td>
                                        </tr>
                                        <tr>
                                            <td>Username :</td><td>{{$user->username}}</td>
                                        </tr>
                                        <tr>
                                            <td>Last login:</td><td>{{$user->last_login}}</td>
                                        </tr>
                                        <tr>
                                            <td>Regions:</td><td>
                                            @if($regions)
                                                @foreach($regions as $region)
                                                    {{$region->name}}
                                                @endforeach
                                            @endif
                                                <a class="pull-right" href="{{url('/agronomists/add-region',$user->id)}}"> Edit regions</a>

                                            </td>
                                        </tr>
                                        </tbody>


                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <h3 class="panel-title">Farmers : {{$farmers}}</h3>

                                </div>
                            </div>
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"> Cohorts </h3>
                                                <div class="pull-right">
                                                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                                </div>
                                            </div>
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <table class="table">
                                                <thead>
                                                <tr class="filters">

                                                    <td>Name</td>
                                                    <td>Start date</td>
                                                    <td>Harvest date</td>

                                                    <th>Actions</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($rounds_array)
                                                    <?php $n=0; ?>
                                                    @foreach($rounds_array as $rounds)
                                                        <?php $n++?>
                                                        <tr>
                                                            <td>{{$rounds['name']}}</td>
                                                            <td>{{$rounds['start_date']}}</td>
                                                            <td>{{$rounds['harvest_date']}}</td>
                                                            <td>
                                                                <a href="{{url('rounds/view',$rounds['id'])}}" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-eye"></i></a>
                                                            </td>


                                                        </tr>
                                                    @endforeach
                                                @endif

                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>
                                            {{--<ul class="pagination pull-right pagination-green">--}}
                                                {{--<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>--}}
                                                {{--<li class="active"><a href="#">1</a></li>--}}
                                                {{--<li><a href="#">2</a></li>--}}
                                                {{--<li><a href="#">3</a></li>--}}
                                                {{--<li><a href="#">4</a></li>--}}
                                                {{--<li><a href="#">5</a></li>--}}
                                                {{--<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>--}}
                                            {{--</ul>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop