@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('agronomists') !!}

            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Agronomists</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{ route('sentinel.users.create') }}" class="btn btn-red btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;<Edit></Edit> Agronomist</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-red filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Agronomists</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        @if (session('message'))
                                            <div class="alert alert-success">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Username" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Email" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Last login" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Action" disabled></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($agronomists)
                                            @foreach($agronomists as $ag)
                                            <tr>
                                                <td>1</td>
                                                <td><a href="{{url('agronomists/view',$ag->id)}}">{{$ag->username}}</a></td>
                                                <td>{{$ag->email}}</td>
                                                <td>{{$ag->last_login}}</td>
                                                <td><a href="{{url('agronomists/view',$ag->id)}}"><i class="fa fa-eye"></i> view </a> </td>

                                            </tr>
                                                @endforeach
                                                @endif

                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        {{{$agronomists->render()}}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.agronomists').addClass("active");
@endsection