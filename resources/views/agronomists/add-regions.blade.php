@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div     class="caption">Assign {{$user->username}} a region</div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="panel panel-yellow">
                                        <div class="panel-heading">Assign</div>
                                        <div class="panel-body pan">
                                            @if (session('message'))
                                                <div class="alert alert-success">
                                                    {{ session('message') }}
                                                </div>
                                            @endif
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <form class="form-horizontal" role="form" method="POST" action="/agronomists/store-region">
                                                <div class="form-body pal">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">


                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Regions</label>
                                                        <div class="col-md-9">
                                                            @foreach($regions as $region)
                                                                <div class="checkbox">
                                                                <label>
                                                                    @if(in_array($region->id,$regions_array))
                                                                    <input type="checkbox" checked="true" name="regions[]"value="<?php echo $region->id ?>"/>
                                                                    {{$region->name}}
                                                                    @else
                                                                    <input type="checkbox" name="regions[]"value="<?php echo $region->id ?>"/>
                                                                    {{$region->name}}
                                                                    @endif


                                                                </label>
                                                                    </div>

                                                            @endforeach
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="form-actions pal">
                                                    <div class="form-group mbn">
                                                        <div class="col-md-12">
                                                            <button type="submit" class="btn btn-yellow pull-right">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
