@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><a href="crops.html"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                            <li><h4>New Agronomist</h4></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-8">
                                    <div class="panel panel-green">
                                        <div class="panel-heading">Add Agronomist</div>
                                        <div class="panel-body pan">
                                            @if (session('message'))
                                                <div class="alert alert-success">
                                                    {{ session('message') }}
                                                </div>
                                            @endif
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <form class="form-horizontal" role="form" method="PATCH" action="{{url('agronomists/update',$agronomist->id)}}">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Name</label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="name" value="{{ $agronomist->name }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">E-Mail Address</label>
                                                    <div class="col-md-6">
                                                        <input type="email" class="form-control" name="email" value="{{ $agronomist->email }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Phone Number</label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" value="{{ $agronomist->phone_no }}" name="phone_no">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <button type="submit" class="btn btn-primary">
                                                            Register
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.agronomists').addClass("active");
@endsection