@extends('layouts.patner_dash')

@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <div class="new-region pull-left"><a href="{{url('patner-cohort/view',['id'=>$cohort->id])}}" class="btn btn-green btn-outlined btn-sm ">&nbsp;&nbsp;Back</a></div>
                                        <li><h4>{{$cohort->names }}Cohorts</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{url('patner-measurables/create',['id'=>$cohort->id])}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Measurable</a></div>

                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"> Measurables</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Name" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Unit" disabled></th>

                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($patner_cohorts_measurables)
                                                <?php $n=1;?>
                                                @foreach($patner_cohorts_measurables as $measurable)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td><a href="{{url('patner-cohort/view',['id'=>$measurable->id])}}">{{$measurable->name}}</a> </td>
                                                        <td>{{$measurable->unit}}</td>
                                                        <td>{{$measurable->description}}</td>


                                                        <td width="15%">
                                                            <ul class="list-unstyled list-inline">
                                                                <li><a href="edit-crop.html" class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-pencil"></i>&nbsp;
                                                                    </a></li>
                                                                <li><a class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-trash-o"></i>&nbsp;
                                                                    </a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <?php $n++?>
                                                @endforeach
                                            @endif


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop