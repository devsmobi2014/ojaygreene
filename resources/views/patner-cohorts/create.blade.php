@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="crops.html"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>New Cohort</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">New Cohort Details</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patner-cohorts/store')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-body pal">
                                                <div class="form-group"><label class="control-label"> Name</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="names" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Value Chains</label>

                                                    <div class="input-icon left">
                                                        <select name="value_chain_id" class="form-control">
                                                            <option>Select Value chain</option>
                                                            @if($value_chains)
                                                                @foreach($value_chains as $value_chain)
                                                                    <option value="{{$value_chain->id}}">{{$value_chain->name}}</option>
                                                                    @endforeach
                                                                @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Region</label>

                                                    <div class="input-icon left">
                                                        <select name="region_id" class="form-control">
                                                            <option>Select Region</option>
                                                            @if($regions)
                                                                @foreach($regions as $region)
                                                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group"><label class="control-label">Description</label><textarea
                                                            rows="3" name="description" class="form-control"></textarea></div>

                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Create Value Chain</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop