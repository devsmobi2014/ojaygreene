@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="crops.html"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>New Measurable</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">New Measurable</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patner-measurables/store')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="cohort_id" value="{{ $cohort->id }}">
                                            <div class="form-body pal">
                                                <div class="form-group"><label class="control-label"> Name</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="name" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Unit</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="unit" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>



                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop