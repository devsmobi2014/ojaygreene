@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="{{url('patner-cohort/view',['id'=>$cohort->id])}}"><i class="fa fa-arrow-circle-left fa-4"></i>Back</a></li>
                                        <li><h4>New Message</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">New Message to Cohort</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patner-cohorts/send-to-farmers')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="cohort_id" value="{{ $cohort->id }}">
                                            <div class="form-body pal">

                                                <div class="form-group"><label class="control-label"> Message</label>

                                                    <div class="input-icon left"></i><textarea
                                                                id="cropName" name="message" type="text" placeholder=""
                                                                class="form-control"></textarea></div>
                                                </div>



                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop