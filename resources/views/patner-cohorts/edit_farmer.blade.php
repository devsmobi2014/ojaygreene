
@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="{{url('patner-cohort/view',['id'=>$cohort->id])}}"><i class="fa fa-arrow-circle-left fa-4"></i>Back</a></li>
                                        <li><h4>New Measurable</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">Edit {{$farmer->name}} Details</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patner-cohort/store-farmer-details')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="cohort_id" value="{{ $cohort_id }}">
                                            <input type="hidden" name="farmer_id" value="{{ $farmer->id }}">
                                            <div class="form-body pal">
                                                @if($patner_cohorts_measurables)
                                                    @foreach($patner_cohorts_measurables as $measurable)
                                                        <div class="form-group"><label class="control-label">{{$measurable->name}} ( {{$measurable->unit}})</label>

                                                            <div class="input-icon left"></i>
                                                                <input id="cropName" name="patner_measurable[{{$measurable->id}}]" value="@if($farmer_patner_measurables) @foreach($farmer_patner_measurables as $mes) @if($mes->id == $measurable->id) {{$mes->value  }} @endif @endforeach @endif" type="text" placeholder="" class="form-control"/>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    @endif


                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop