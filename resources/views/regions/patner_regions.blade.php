@extends('layouts.patner_dash')
@section('content')
    {{--<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->--}}
        {{--<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">--}}
            {{--<div class="page-header pull-left">--}}
                {{--{!! Breadcrumbs::render('regions') !!}--}}
            {{--</div>--}}

            {{--<div class="clearfix"></div>--}}
        {{--</div>--}}
        <div class="page-content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="caption pull-left"><h4> Region Cohort</h4> </div>
                                </div>
                                @if (Sentry::check())
                                    @if(Sentry::getUser()->hasAccess('patner_admin') || Sentry::getUser()->hasAccess('admin'))
                                    <div class="col-md-4">
                                        {{--<div class="new-region pull-right"><a href="{{url('regions/migrate')}}" class="btn btn-yellow btn-outlined btn-sm "></i>&nbsp;Migrate farmers</a></div>--}}
                                        {{--<div class="new-region pull-right"><a href="{{url('messages/compose')}}" class="btn btn-yellow btn-outlined btn-sm "><i class="fa fa-envelope-o"></i>&nbsp;Send message</a></div>--}}
                                    </div>
                                    <div class="col-md-4">
                                        <div class="new-region pull-right"><a href="{{url('regions/create-patner-regions')}}" class="btn btn-yellow btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;New Region</a></div>
                                    </div>
                                    <div class="col-md-4">
                                        {{--<div class="new-region pull-right"><a href="{{url('regions/manage')}}" class="btn btn-yellow btn-outlined btn-sm "><i class="fa fa-cog"></i>&nbsp;Manage Regions</a></div>--}}
                                    </div>
                                    @endif
                                @endif
                            </div>

                        </div>
                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-primary filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">{{count($regions)}} Regions</h3>
                                                <div class="pull-right">

                                                </div>
                                            </div>
                                            <table  class="table">
                                                <thead>
                                                <tr class="filters">

                                                    <th><input type="text" class="form-control" placeholder="Region" disabled></th>
                                                    {{--<th><input type="text" class="form-control" placeholder="Farmers" disabled></th>--}}
                                                    <th width="40%">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                  @if($regions )
                                                      @foreach($regions as $region)
                                                          <tr>
                                                              
                                                          <td>{{$region->name}}</td>
                                                          {{--<td><a href="/regions/view/{{$region->id}}" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-eye"></i></a></td>--}}
                                                          {{--<td><a href="/regions/destroy/'{{$region->id}}" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-trash"></i></a></td>--}}
                                                          </tr>

                                                      @endforeach
                                                      @endif
                                                </tbody>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    $(function() {
    $('#regions').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": true,
    ajax: '{{ url('/regions/get-regions') }}',
    "columns": [
    {
    "data": "name",
    "name": "regions.name",
    "defaultContent": "<i>Not set</i>"
    },
    {data: 'region_count', name: 'region_count', searchable: false},
    {
    "data": "id",
    "name":"regions.id",
    "render": function(data, type, full, meta) {

    return '<a href="/regions/view/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-eye"></i></a><a href="/regions/destroy/'+ full.id +'"  class="btn btn-warning btn-outlined btn-sm delete">delete</a>';
    },
    "defaultContent": "<i>Not set</i>"
    },
    ]
    });
    });


    $("#regions").on("click", ".delete", function(){
    if(confirm("Are you sure you want to delete this region")){
    return true;
    }
    return false;
    });

    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.regions').addClass("active");


@endsection