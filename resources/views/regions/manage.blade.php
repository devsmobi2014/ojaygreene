@extends('layouts.master')

@section('content')
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">

        </div>

        <div class="clearfix"></div>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="{{url('regions')}}"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>Manage regions</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    <div class="panel-heading">Select active regions </div>
                                    <div class="panel-body pan">
                                        @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        <form method="post" action="/regions/manage-store">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-body pal">
                                                <div class="row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">

                                                            <label class="control-label"><span class="cr">From</span></label>
                                                            @foreach($regions as $region)
                                                            <div class="checkbox">
                                                                <label>
                                                                    @if($region->status)
                                                                    <input name="from[]" checked class="cr" type="checkbox" value="{{$region->id}}">
                                                                    @else
                                                                    <input name="from[]"  class="cr" type="checkbox" value="{{$region->id}}">
                                                                    @endif

                                                                    {{$region->name}}
                                                                </label>
                                                            </div>
                                                            @endforeach

                                                        </div>
                                                    </div>


                                                </div>



                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
$("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
$('.regions').addClass("active");
@endsection
