@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('region',$region) !!}
            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">

        <div class="row">
            <div class="col-lg-8">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href=""><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>Region crops</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                               {{--@if($user_regions)--}}
                                    {{--Assigned to:--}}
                                    {{--@foreach($user_regions as $region)--}}
                                        {{--{{$region->username}}--}}
                                    {{--@endforeach--}}
                                {{--@endif--}}
                                {{--<div class="new-region pull-right "><a href="compose-message-region-crop-cohort.html" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Send Message</a></div>--}}
                            </div>
                            <div class="col-md-3">
                                <div class="new-region pull-right"><a href="{{url('/regions/addcrop',$region->id)}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Region Crop</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">{{$region->name}}</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Crop" disabled></th>
                                                <th width="">Farmers</th>
                                                <th width="">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($crops)
                                                <?php $n=0; ?>
                                                @foreach($crops as $crop)
                                                    <?php $n++; ?>
                                                    <tr>
                                            <tr>
                                                <td>{{$n}}</td>
                                                <td>{{$crop->name}}</td>
                                                <td>{{$crop->farmers}}</td>
                                                <td>
                                                    <ul class="list-unstyled list-inline">
                                                        <li><a href="{{url('/rounds',$crop->crop_region_id)}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                        class="fa fa-eye"></i>&nbsp;
                                                            </a></li>
                                                        <li><a href="{{url('/regions/remove-crop',$crop->crop_region_id)}}" class="btn btn-success btn-outlined btn-sm delete"><i
                                                                        class="fa fa-trash-o "></i>&nbsp;
                                                            </a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            @endforeach
                                           @endif


                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class=" panel panel-green filterable panel-heading">
                    <h3 class="panel-title">{{$region->name}}  Farmers</h3>
                    <div class="pull-right">
                        {{--<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>--}}
                    </div>
                </div>
                <table id="region_farmers" class="table">
                    <thead>
                    <tr class="filters">

                        <th>Name</th>
                        <th>Gender</th>
                        <th>Phone Number</th>
                        <th>Channel of registration</th>
                        <th>Acreage</th>
                        <th>Actions</th>

                    </tr>
                    </thead>

                </table>
            </div>

        </div>
    </div>
    </div>

@endsection
@section('js')
    $(function() {
    $('#region_farmers').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": true,
    ajax: '{{ url('/regions/get-region-farmers',array('id'=>$region->id)) }}',
    "columns": [
    {
    "data": "farmer_name",
    "name": "farmers.name",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "gender",
    "name": "farmers.gender",
    "defaultContent": "<i>Not set</i>",
    "render": function(data, type, full, meta){
    return (data == 1) ? 'Male' : 'Female';
    }
    },
    {
    "data": "phone",
    "name" : "farmers.phone",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "excel_id",
    "name":"farmers.excel_id",
    "searchable": true,

    "render":function(data,type, full,meta){
       if(data){
          return 'Via Excel'
        }else{
        return 'Remotely added'

       }

    }
    },
    {
    "data": "acreage",
    "name":"farmers.acreage",
    "defaultContent": "<i>Not set</i>"
    },
    {
    "data": "id", // can be null or undefined
    "render": function(data, type, full, meta) {
    console.log(full)

    return '<a href="/farmers/edit/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-pencil"></i></a><a href="/farmers/destroy/'+ full.id +'" id="delete" class="btn btn-warning btn-outlined btn-sm delete"><i class="fa fa-trash"></i></a>';
    },
    "defaultContent": "<i>Not set</i>"
    },
    ]
    });
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.regions').addClass("active");
@endsection