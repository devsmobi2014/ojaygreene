@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="crops.html"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>New ValueChain</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    <div class="panel-heading">New Value Chain details</div>
                                    <div class="panel-body pan">
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form method="post" action="/regions/record-crop">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="region_id" value="{{$id}}">
                                            <div class="form-body pal">

                                                <div class="form-group">

                                                        <select class="form-control"  required="" name="crop_id">
                                                            <option value="">Select a Value Chain</option>
                                                            @foreach($crops as $crop)
                                                                <option value="{{$crop->id}}">{{$crop->name}}</option>
                                                            @endforeach
                                                        </select>

                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Duration (weeks)</label>

                                                            <div class="input-icon left"><i class="fa fa-clock-o"></i>
                                                                <input id="duration" name="duration" type="text" placeholder=""
                                                                        class="form-control"/></div>
                                               </div>

                                                {{--<div class="form-group"><label class="control-label">Crop description</label><textarea--}}
                                                            {{--rows="3" class="form-control"></textarea></div>--}}

                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Add ValueChain</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.regions').addClass("active");
@endsection
