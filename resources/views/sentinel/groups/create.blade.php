@extends('layouts.master')

{{-- Web site Title --}}
@section('title')
    @parent
    Users
@stop

{{-- Content --}}
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        {{--<ul class="list-unstyled list-inline">--}}
                                            {{--<h1>Available roles</h1>--}}
                                        {{--</ul>--}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='btn-toolbar pull-right'>
                                        <div class='btn-group'>
                                            <a class='btn btn-primary' href="{{ route('sentinel.groups.index') }}">Back to groups</a>
                                        </div>
                                    </div>
                                    {{--<div class="new-region pull-right"><a href="add-farmer.html" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Farmer</a></div>--}}
                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                {{--<h3 class="panel-title">Current users</h3>--}}
                                                <div class="pull-right">

                                                </div>
                                            </div>
                                                <form method="POST" action="{{ route('sentinel.groups.store') }}" accept-charset="UTF-8">

                                                    <h2>Create New Group</h2>

                                                    <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                                        <input class="form-control" placeholder="Name" name="name" type="text"  value="{{ Input::old('email') }}">
                                                        {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-lg-3">

                                                        </div>
                                                        <div class="col-lg-9">
                                                            <label for="Permissions">Permissions</label>
                                                            <div class="form-group">
                                                                <?php $defaultPermissions = config('sentinel.default_permissions', []); ?>
                                                                @foreach ($defaultPermissions as $permission)
                                                                    <label class="checkbox-inline">
                                                                        <input name="permissions[{{ $permission }}]" value="1" type="checkbox"
                                                                        @if (Input::old('permissions[' . $permission .']'))
                                                                               checked
                                                                                @endif
                                                                                > {{ ucwords($permission) }}
                                                                    </label>
                                                                @endforeach
                                                            </div>

                                                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                                            <input class="btn btn-primary" value="Create New Group" type="submit">

                                                        </div>
                                                    </div>

                                                </form>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop