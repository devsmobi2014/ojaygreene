@extends('layouts.master')

{{-- Web site Title --}}
@section('title')
    @parent
    Users
@stop

{{-- Content --}}
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <h1>Available roles</h1>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='btn-toolbar pull-right'>
                                        <div class='btn-group'>
                                            <a class='btn btn-primary' href="{{ route('sentinel.groups.create') }}">Create Group</a>
                                        </div>
                                    </div>
                                    {{--<div class="new-region pull-right"><a href="add-farmer.html" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Farmer</a></div>--}}
                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                {{--<h3 class="panel-title">Current users</h3>--}}
                                                <div class="pull-right">

                                                </div>
                                            </div>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                <th>User</th>
                                                <th>Status</th>
                                                <th>Options</th>
                                                </thead>
                                                <tbody>
                                                @foreach ($groups as $group)
                                                    <tr>
                                                        <td><a href="{{ route('sentinel.groups.show', $group->hash) }}">{{ $group->name }}</a></td>
                                                        <td>
                                                            <?php
                                                            $permissions = $group->getPermissions();
                                                            $keys = array_keys($permissions);
                                                            $last_key = end($keys);
                                                            ?>
                                                            @foreach ($permissions as $key => $value)
                                                                {{ ucfirst($key) . ($key == $last_key ? '' : ', ') }}
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-default" onClick="location.href='{{ route('sentinel.groups.edit', [$group->hash]) }}'">Edit</button>
                                                            <button class="btn btn-default action_confirm {{ ($group->name == 'Admins') ? 'disabled' : '' }}" type="button" data-token="{{ csrf_token() }}" data-method="delete" href="{{ route('sentinel.groups.destroy', [$group->hash]) }}">Delete</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


