
@extends('layouts.master')

{{-- Web site Title --}}
@section('title')
@parent
Users
@stop

{{-- Content --}}
@section('content')
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">

        </div>

        <div class="clearfix"></div>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    {{--<ul class="list-unstyled list-inline">--}}
                                        {{--<h1>Available roles</h1>--}}
                                        {{--</ul>--}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='btn-toolbar pull-right'>
                                    <div class='btn-group'>
                                        <a class='btn btn-primary' href="{{ url('/agronomists') }}">Back to users</a>
                                    </div>
                                </div>
                                {{--<div class="new-region pull-right"><a href="add-farmer.html" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Farmer</a></div>--}}
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <?php
                                // Determine the edit profile route
                                if (($user->email == Sentry::getUser()->email)) {
                                    $editAction = route('sentinel.profile.edit');
                                } else {
                                    $editAction =  action('\\Sentinel\Controllers\UserController@edit', [$user->hash]);
                                }
                                ?>

                                <h4>Account Profile</h4>

                                <div class="well clearfix">
                                    <div class="col-md-8">
                                        @if ($user->first_name)
                                        <p><strong>First Name:</strong> {{ $user->first_name }} </p>
                                        @endif
                                        @if ($user->last_name)
                                        <p><strong>Last Name:</strong> {{ $user->last_name }} </p>
                                        @endif
                                        <p><strong>Email:</strong> {{ $user->email }}</p>

                                    </div>
                                    <div class="col-md-4">
                                        <p><em>Account created: {{ $user->created_at }}</em></p>
                                        <p><em>Last Updated: {{ $user->updated_at }}</em></p>
                                        <button class="btn btn-primary" onClick="location.href='{{ $editAction }}'">Edit Profile</button>
                                    </div>
                                </div>

                                <h4>Group Memberships:</h4>
                                <?php $userGroups = $user->getGroups(); ?>
                                <div class="well">
                                    <ul>
                                        @if (count($userGroups) >= 1)
                                        @foreach ($userGroups as $group)
                                        <li>{{ $group['name'] }}</li>
                                        @endforeach
                                        @else
                                        <li>No Group Memberships.</li>
                                        @endif
                                    </ul>
                                </div>

                                <hr />


                                <div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
