@extends('layouts.master')

{{-- Web site Title --}}
@section('title')
    @parent
    Users
@stop

{{-- Content --}}
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><h4>Current users</h4></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='btn-toolbar pull-right'>
                                        <div class='btn-group'>
                                            <a class='btn btn-primary' href="{{ route('sentinel.users.create') }}">Create User</a>
                                        </div>
                                    </div>
                                    {{--<div class="new-region pull-right"><a href="add-farmer.html" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Farmer</a></div>--}}
                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Current users</h3>
                                                <div class="pull-right">

                                                </div>
                                            </div>
                                            <table id="all-users" class="table table-striped table-hover">
                                                <thead>
                                                <td>#</td>
                                                <th>User</th>
                                                <th>Status</th>
                                                <th>Options</th>
                                                </thead>
                                                <tbody>
                                                <?php $n=1;?>
                                                @foreach ($users as $user)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td><a href="{{ route('sentinel.users.show', array($user->hash)) }}">{{ $user->email }}</a></td>
                                                        <td>{{ $user->status }} </td>
                                                        <td>
                                                            <button class="btn btn-default" type="button" onClick="location.href='{{ route('sentinel.users.edit', array($user->hash)) }}'">Edit</button>
                                                            @if ($user->status != 'Suspended')
                                                                <button class="btn btn-default" type="button" onClick="location.href='{{ route('sentinel.users.suspend', array($user->hash)) }}'">Suspend</button>
                                                            @else
                                                                <button class="btn btn-default" type="button" onClick="location.href='{{ route('sentinel.users.unsuspend', array($user->hash)) }}'">Un-Suspend</button>
                                                            @endif
                                                            @if ($user->status != 'Banned')
                                                                <button class="btn btn-default" type="button" onClick="location.href='{{ route('sentinel.users.ban', array($user->hash)) }}'">Ban</button>
                                                            @else
                                                                <button class="btn btn-default" type="button" onClick="location.href='{{ route('sentinel.users.unban', array($user->hash)) }}'">Un-Ban</button>
                                                            @endif
                                                            <button class="btn btn-default action_confirm" type="button" onClick="location.href='{{ route('sentinel.users.destroy', array($user->hash)) }}' " data-token="{{ Session::getToken() }}" data-method="delete">Delete</button>
                                                            <a href="{{url('agronomists/add-region',$user->id)}}" class="btn btn-default">Assign regions</a>

                                                        </td>
                                                    </tr>
                                                    <?php $n++;?>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('js')
    $(function() {
    $('#all-users').DataTable();
    });
@stop