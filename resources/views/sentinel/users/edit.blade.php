
@extends('layouts.master')

{{-- Web site Title --}}
@section('title')
@parent
Users
@stop

{{-- Content --}}
@section('content')

<?php
// Pull the custom fields from config
$isProfileUpdate = ($user->email == Sentry::getUser()->email);
$customFields = config('sentinel.additional_user_fields');

// Determine the form post route
if ($isProfileUpdate) {
    $profileFormAction = route('sentinel.profile.update');
    $passwordFormAction = route('sentinel.profile.password');
} else {
    $profileFormAction =  route('sentinel.users.update', $user->hash);
    $passwordFormAction = route('sentinel.password.change', $user->hash);
}
?>
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">

        </div>

        <div class="clearfix"></div>
    </div>
    <div class="page-content">
        <div class="row">
            <div class='page-header'>
                <h1>
                    Edit
                    @if ($isProfileUpdate)
                    Your
                    @else
                    {{ $user->email }}'s
                    @endif
                    Account
                </h1>
            </div>
        </div>
        @if (Sentry::getUser()->hasAccess('admin') && ($user->hash != Sentry::getUser()->hash))
        <div class="row">
            <h4>Group Memberships</h4>
            <div class="well">
                <form method="POST" action="{{ route('sentinel.users.memberships', $user->hash) }}" accept-charset="UTF-8" class="form-horizontal" role="form">

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            @foreach($groups as $group)
                            <label class="checkbox-inline">
                                <input type="checkbox" name="groups[{{ $group->name }}]" value="1" {{ ($user->inGroup($group) ? 'checked' : '') }}> {{ $group->name }}
                            </label>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                            <input class="btn btn-primary" value="Update Memberships" type="submit">
                        </div>
                    </div>

                </form>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    {{--<ul class="list-unstyled list-inline">--}}
                                        {{--<h1>Available roles</h1>--}}
                                        {{--</ul>--}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='btn-toolbar pull-right'>
                                    <div class='btn-group'>
                                        <a class='btn btn-primary' href="{{ url('/agronomists') }}">Back to users</a>
                                    </div>
                                </div>
                                {{--<div class="new-region pull-right"><a href="add-farmer.html" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Farmer</a></div>--}}
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">

                        <div class="row">
                            <h4>Change Password</h4>
                            <div class="well">
                                <form method="POST" action="{{ $passwordFormAction }}" accept-charset="UTF-8" class="form-inline" role="form">

                                    @if(! Sentry::getUser()->hasAccess('admin'))
                                    <div class="form-group {{ $errors->has('oldPassword') ? 'has-error' : '' }}">
                                        <label for="oldPassword" class="sr-only">Old Password</label>
                                        <input class="form-control" placeholder="Old Password" name="oldPassword" value="" id="oldPassword" type="password">
                                    </div>
                                    @endif

                                    <div class="form-group {{ $errors->has('newPassword') ? 'has-error' : '' }}">
                                        <label for="newPassword" class="sr-only">New Password</label>
                                        <input class="form-control" placeholder="New Password" name="newPassword" value="" id="newPassword" type="password">
                                    </div>

                                    <div class="form-group {{ $errors->has('newPassword_confirmation') ? 'has-error' : '' }}">
                                        <label for="newPassword_confirmation" class="sr-only">Confirm New Password</label>
                                        <input class="form-control" placeholder="Confirm New Password" name="newPassword_confirmation" value="" id="newPassword_confirmation" type="password">
                                    </div>

                                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                    <input class="btn btn-primary" value="Change Password" type="submit">

                                    {{ ($errors->has('oldPassword') ? '<br />' . $errors->first('oldPassword') : '') }}
                                    {{ ($errors->has('newPassword') ?  '<br />' . $errors->first('newPassword') : '') }}
                                    {{ ($errors->has('newPassword_confirmation') ? '<br />' . $errors->first('newPassword_confirmation') : '') }}

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

