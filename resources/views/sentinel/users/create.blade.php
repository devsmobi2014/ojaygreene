@extends('layouts.master')

{{-- Web site Title --}}
@section('title')
    @parent
    Users
@stop

{{-- Content --}}
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        {{--<ul class="list-unstyled list-inline">--}}
                                        {{--<h1>Available roles</h1>--}}
                                        {{--</ul>--}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='btn-toolbar pull-right'>
                                        <div class='btn-group'>
                                            <a class='btn btn-primary' href="{{ url('/agronomists') }}">Back to users</a>
                                        </div>
                                    </div>
                                    {{--<div class="new-region pull-right"><a href="add-farmer.html" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Farmer</a></div>--}}
                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                {{--<h3 class="panel-title">Current users</h3>--}}
                                                <div class="pull-right">

                                                </div>
                                            </div>
                                            <form method="POST" action="{{ route('sentinel.users.store') }}" accept-charset="UTF-8">

                                                <h2>Create New User</h2>

                                                <div class="form-group {{ ($errors->has('username')) ? 'has-error' : '' }}">
                                                    <input class="form-control" placeholder="Username" name="username" type="text"  value="{{ Input::old('username') }}">
                                                    {{ ($errors->has('username') ? $errors->first('username') : '') }}
                                                </div>

                                                <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                                                    <input class="form-control" placeholder="E-mail" name="email" type="text"  value="{{ Input::old('email') }}">
                                                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                                </div>

                                                <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                                                    <input class="form-control" placeholder="Password" name="password" value="" type="password">
                                                    {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                                                </div>

                                                <div class="form-group {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                                                    <input class="form-control" placeholder="Confirm Password" name="password_confirmation" value="" type="password">
                                                    {{ ($errors->has('password_confirmation') ?  $errors->first('password_confirmation') : '') }}
                                                </div>

                                                <div class="form-group">
                                                    <label class="checkbox">
                                                        <input name="activate"  value="activate" type="checkbox"> Activate
                                                    </label>
                                                </div>

                                                <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                                <input class="btn btn-primary" value="Create" type="submit">

                                            </form>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop