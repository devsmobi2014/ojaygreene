@extends('home_layout')

@section('content')


    <section id="about" class="section-content">
        <div class="container">
            <div class="row mbxxl">
                <div class="col-lg-12">
                    <h2 class="section-heading">What We <span>Do</span></h2>
                    <div class="line"></div>
                    <p class="section-description">We help smallholder crop farmers get their produce to profitable urban markets by utilizing a sms system that advises farmers on quality of seed, land optimization and offers them support through Agronomists who ensure higher better quality yields, thereby increasing their competitiveness and income levels.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 mbl">
                    <div class="farmer">
                        <img class="img-responsive" src="/images/farmer.jpg">
                    </div>
                    <h3>{{$farmers}}</h3>
                    <p>Farmers.</p>
                </div>
                <div class="col-lg-4 mbl">
                    <div class="crops">
                        <img class="img-responsive"  src="/images/crops.jpg">
                    </div>
                    <h3>{{$crops}}</h3>
                    <p>Crop Variety</p>
                </div>
                <div class="col-lg-4 mbl">
                    <div class="agronomist">
                        <img class="img-responsive" src="/images/agronomist.jpg">
                    </div>
                    <h3>{{$agronomists}}</h3>
                    <p>Agronomist</p>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="section-content">


        <div class="container">
            <form action="#">
                <div class="row mtxxl">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 mbm">
                                <input type="text" placeholder="Full Name" class="form-control input-lg" />
                            </div>
                            <div class="col-lg-6 mbm">
                                <input type="text" placeholder="E-mail Address" class="form-control input-lg" />
                            </div>
                            <div class="col-lg-12">
                                <input type="text" placeholder="Subject" class="form-control input-lg" />
                            </div>
                            <div class="col-lg-12">
                                <textarea placeholder="Message" rows="5" class="form-control input-lg mtm"></textarea>
                            </div>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-yellow btn-block">Send Message</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h3 class="title">Contacts</h3>
                        <ul class="list-unstyled">
                            <li>Company:&nbsp;Ojay Greene Farm</li>
                            <li>Email:&nbsp;sms@ojaygreene.com</li>
                            <li>Phone:&nbsp;+254 721 383595</li>
                        </ul>
                        <div class="social">
                            <a href="#" class="ico-socialize-twitter1 ico-socialize type2 ico-lg"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="ico-socialize-facebook1 ico-socialize type2 ico-lg"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="ico-socialize-google2 ico-socialize type2 ico-lg"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="ico-socialize-dribbble1 ico-socialize type2 ico-lg"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="ico-socialize-youtube1 ico-socialize type2 ico-lg"><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </section>


@endsection

