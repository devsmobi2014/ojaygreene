@extends('layouts.patner_dash')

@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Farmers</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                {{--<div class="new-region pull-right"><a href="{{url('patner-add-regions')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Regions</a></div>--}}
                                <div class="new-region pull-right"><a href="{{url('farmers/create')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Farmer</a></div>
                                <div class="new-region pull-right"><a href="{{url('patner-cohorts-excel-farmers')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;Import farmers</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Farmers</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table id="farmers" class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Name" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Phone" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Region" disabled></th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($farmers)
                                                <?php $n=1;?>
                                                @foreach($farmers as $farmer)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>{{$farmer->farmer_name}}</td>
                                                        <td>{{$farmer->phone}}</td>
                                                        <td>{{$farmer->region_name }}</td>

                                                        <td width="15%">
                                                            <ul class="list-unstyled list-inline">
                                                                <li><a href="#" class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-pencil"></i>&nbsp;
                                                                    </a></li>
                                                                <li><a class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-trash-o"></i>&nbsp;
                                                                    </a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <?php $n++?>
                                                @endforeach
                                            @endif


                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('js')
    $('#farmers').DataTable();
    @stop