@extends('layouts.patner')

@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="{{url('dashboard/patner-intro')}}"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>Back </h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="panel panel-green">
                                    <div class="panel-heading">Add Partner</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patners/store')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-body pal">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Partner Name</label>

                                                            <div class="input-icon left"><i class="fa fa-user"></i><input
                                                                        id="FarmersName" name="name" type="text" placeholder=""
                                                                        class="form-control"/></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Phone Number</label>

                                                            <div class="input-icon left"><i class="fa fa-phone"></i><input
                                                                        id="inputEmail" name="phone" type="text" placeholder=""
                                                                        class="form-control"/></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Category</label>

                                                            <div class="input-icon left"><input
                                                                        id="FarmersName" name="category" type="text" placeholder=""
                                                                        class="form-control"/></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Description</label>

                                                            <div class="input-icon left"><textarea
                                                                        id="inputEmail" name="description" type="text" placeholder=""
                                                                        class="form-control"></textarea></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Logo</label>

                                                            <div class="new-region right">
                                                                        <span
                                                                                class="btn btn-green btn-outlined btn-sm fileinput-button"><i
                                                                                    class="glyphicon glyphicon-plus"></i>&nbsp;<span>Upload partner logo</span><input
                                                                                    type="file" name="files[]" multiple="multiple"/></span>
                                                            </div>
                                                            <div class="partner-img left">
                                                                <img class="partner-img-inner" src="/images/5.jpg">
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Barner</label>

                                                            <div class="new-region right">
                                                                        <span
                                                                                class="btn btn-green btn-outlined btn-sm fileinput-button"><i
                                                                                    class="glyphicon glyphicon-plus"></i>&nbsp;<span>Upload partner barner</span><input
                                                                                    type="file" name="files[]" multiple="multiple"/></span>
                                                            </div>
                                                            <div class="partner-img left">
                                                                <img class="partner-img-inner fit-in" src="/images/4.jpg">
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Header Background Color</label>

                                                            <div class="form-group">
                                                                <input type="text" id="hue-demo" class="form-control demo" data-control="hue" value="#2a3b4c">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Header Font Color</label>

                                                            <div class="form-group">
                                                                <input type="text" id="hue-demo" class="form-control demo" data-control="hue" value="#fff">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Sidebar Color</label>

                                                            <div class="form-group">
                                                                <input type="text" id="hue-demo" class="form-control demo" data-control="hue" value="#33485c">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group"><label class="control-label">Sidebar Font Color</label>

                                                            <input type="text" id="saturation-demo" class="form-control demo" data-control="saturation" value="#fff">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @stop