@extends('layouts.patner_dash')

@section('content')


        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><h4> {{$cohort->names}} Cohort</h4></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="new-region pull-right"><a href="" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-eye"></i>&nbsp;&nbsp;Comments</a></div>
                                    <div class="new-region pull-right"><a href="" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;Add farmers</a></div>
                                    <div class="new-region pull-right"><a href="" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-eye"></i>&nbsp;&nbsp;View Activities</a></div>
                                </div>
                                <div class="col-md-2">

                                        <a href="" class="btn btn-warning delete-cohort"><i class="fa fa-close"></i>&nbsp;&nbsp;Close cohort</a>

                                        {{--<a href="" class="btn btn-warning activate-cohort"><i class="fa fa-close"></i>&nbsp;&nbsp;Activate cohort</a>--}}

                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3 class="panel-title">Summary</h3>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Start week :</td><td></td>
                                        </tr>
                                        <tr>
                                            <td>Expected Harvest week :</td><td></td>
                                        </tr>
                                        <tr>
                                            <td>Yield:</td><td></td>
                                        </tr>
                                        <tr>
                                            <td>Expected yield:</td><td></td>
                                        </tr>
                                        <tr>
                                            <td>Projected Yield:</td><td></td>
                                        </tr>
                                        <tr>
                                            <td>Actual Yield:</td><td></td>
                                        </tr>


                                        </tbody>


                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <h3 class="panel-title">Farmers : </h3>
                                    <h3 class="panel-title">Status  </h3    >

                                </div>
                            </div>
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">  Farmers</h3>
                                                <div class="pull-right">
                                                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                                </div>
                                            </div>
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <table class="table">
                                                <thead>
                                                <tr class="filters">
                                                    <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                    <th><input type="text" class="form-control" placeholder="Name" disabled></th>
                                                    <th><input type="text" class="form-control" placeholder="Phone number" disabled></th>
                                                    <td>Expected yield</td>
                                                    <td>Projected yield</td>
                                                    <td>Actual yield</td>
                                                    <td>Acreage</td>
                                                    <th>Actions</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($farmers)
                                                    <?php $n=0; ?>
                                                    @foreach($farmers as $farmer)
                                                        <?php $n++?>
                                                        <tr>
                                                            <td>{{$n}}</td>
                                                            <td>{{$farmer->name}}</td>
                                                            <td>{{$farmer->phone}}</td>
                                                            <td>{{round($farmer->expected_yield)}}</td>
                                                            <td>{{round($farmer->projected_yield)}}</td>
                                                            <td>{{round($farmer->actual_yield)}}</td>
                                                            <td>{{$farmer->acreage}}</td>

                                                            <td>
                                                                <ul class="list-unstyled list-inline">

                                                                    <li><a href="{{url('rounds/editfarmer',$farmer->id)}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                                    class="fa fa-pencil"></i>&nbsp;
                                                                        </a></li>
                                                                    <li><a href="{{url('rounds/remove-farmer',['farmer_id'=>$farmer->farmer_id,'round_id'=>$round->id])}}" class="btn btn-green btn-outlined btn-sm delete"><i
                                                                                    class="fa fa-trash-o">remove farmer</i>&nbsp;
                                                                        </a></li>
                                                                    <li><a href="{{url('rounds/send-message',['phone'=>$farmer->phone,'round_id'=>$round->id])}}" class="btn btn-green btn-outlined btn-sm">Send message&nbsp;
                                                                        </a></li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@stop
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.cohorts').addClass("active");
@stop