@extends('layouts.patner_dash')


@section('content')

    <div class="page-content">
        <div id="sum_box" class="row">
            <div class="col-sm-6 col-md-4">
                <div class="panel profit db mbm">
                    <div class="panel-body"><p class="icon "><i class="icon icon-farmer fa fa-user"></i></p><h4
                                class="value"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                    data-duration="0">{{$no_farmers}}</span></h4>

                        <p class="description">Farmers registered</p>

                        <div class="progress progress-sm mbn">
                            <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 80%;" class="progress-bar progress-bar-yellow"><span
                                        class="sr-only">80% Complete (success)</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <div class="panel income db mbm">
                        <div class="panel-body">

                            <p class="icon "><i class="icon icon-crop fa fa-leaf"></i></p><h4
                                    class="value"><span>{{$no_values_chains}}</span></h4>

                            <p class="description">Value chains</p>

                            <div class="progress progress-sm mbn">
                                <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                     style="width: 80%;" class="progress-bar progress-bar-success"><span
                                            class="sr-only">60% Complete (success)</span></div>
                            </div>
                        </div>
                    </div>
                </a>
                <ul class="dropdown-menu chain-menu pull-right">
                    <li><a href="fisheries.html"><i class="fa fa-angle-right"></i><span class="submenu-title"> Fisheries</span></a>
                    </li>
                    <li><a href="livestock.html"><i class="fa fa-angle-right"></i><span class="submenu-title"> Livestock</span></a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="panel task db mbm">
                    <div class="panel-body"><p class="icon "><i class="icon icon-message fa fa-envelope"></i></p><h4
                                class="value"><span>{{$no_messages}}</span></h4>

                        <p class="description">Messages sent</p>

                        <div class="progress progress-sm mbn">
                            <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 80%;" class="progress-bar progress-bar-danger"><span
                                        class="sr-only">50% Complete (success)</span></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="caption">Pie Chart</div>
                        <div class="tools"><i class="fa fa-chevron-up"></i><i data-toggle="modal"
                                                                              data-target="#modal-config"
                                                                              class="fa fa-cog"></i><i
                                    class="fa fa-refresh"></i><i class="fa fa-times"></i></div>
                    </div>
                    <div class="portlet-body">
                        <div id="pie-chart" style="width: 100%; height:300px"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="caption">Bar Chart</div>
                        <div class="tools"><i class="fa fa-chevron-up"></i><i data-toggle="modal"
                                                                              data-target="#modal-config"
                                                                              class="fa fa-cog"></i><i
                                    class="fa fa-refresh"></i><i class="fa fa-times"></i></div>
                    </div>
                    <div class="portlet-body">
                        <div id="bar-chart" style="width: 100%; height:300px"></div>
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-6">--}}
                {{--<div class="portlet box">--}}
                    {{--<div class="portlet-header">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-8">--}}
                                {{--<div class="caption pull-left"><h4> Expected yield per crop.</h4> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="portlet-body">--}}
                        {{--<div class="row mbm">--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<div class="table-responsive">--}}
                                    {{--<div class="panel panel-green filterable">--}}
                                        {{--<div class="panel-heading">--}}
                                            {{--<h3 class="panel-title">&nbsp</h3>--}}
                                            {{--<div class="pull-right">--}}
                                                {{--<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<table class="table">--}}
                                            {{--<thead>--}}
                                            {{--<tr class="filters">--}}
                                                {{--<th><input type="text" class="form-control hidden" placeholder="#" disabled></th>--}}
                                                {{--<th><input type="text" class="form-control" placeholder="Crop" disabled></th>--}}
                                                {{--<th><input type="text" class="form-control" placeholder="Expected Yield" disabled></th>--}}
                                            {{--</tr>--}}
                                            {{--</thead>--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td>1</td>--}}
                                                {{--<td>Managu</td>--}}
                                                {{--<td>293</td>--}}
                                            {{--</tr>--}}
                                            {{--<tr>--}}
                                                {{--<td>2</td>--}}
                                                {{--<td>Spinach</td>--}}
                                                {{--<td>210</td>--}}
                                            {{--</tr>--}}
                                            {{--<tr>--}}
                                                {{--<td>3</td>--}}
                                                {{--<td>Kale</td>--}}
                                                {{--<td>430</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<div class="clearfix"></div>--}}
                                        {{--<ul class="pagination pull-right pagination-green">--}}
                                            {{--<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>--}}
                                            {{--<li class="active"><a href="#">1</a></li>--}}
                                            {{--<li><a href="#">2</a></li>--}}
                                            {{--<li><a href="#">3</a></li>--}}
                                            {{--<li><a href="#">4</a></li>--}}
                                            {{--<li><a href="#">5</a></li>--}}
                                            {{--<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
        <div id="sum_boxs" class="row">
            <div class="col-lg-6">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="caption pull-left"><h4> Messages per region</h4> </div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-red filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">&nbsp</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th width="">Messages  sent</th>
                                                <th width="">Responses</th>
                                                <th><input type="text" class="form-control" placeholder="Region" disabled></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>120</td>
                                                <td>240</td>
                                                <td>Kiambu</td>

                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>140</td>
                                                <td>320</td>
                                                <td>Lari</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>190</td>
                                                <td>280</td>
                                                <td>Nyeri</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        <ul class="pagination pull-right pagination-red">
                                            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="caption pull-left"><h4> Expected harvest dates</h4> </div>
                            </div>
                        </div>
                    </div>
                    <div id="table-sorter-tab" class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">&nbsp</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
                                            <thead>
                                            <tr class="filters">
                                                <th width="1%"></th>
                                                <th><input type="text" class="form-control" placeholder="Date" data-field="name" data-align="center" data-sortable="true" disabled></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>5</td>
                                                <td>20-05-2014</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>20-05-2014</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>20-05-2014</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        <ul class="pagination pull-right pagination-green">
                                            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div id="sum_boxs" class="row">--}}
            {{--<div class="col-lg-6">--}}
                {{--<div class="portlet box">--}}
                    {{--<div class="portlet-header">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-8">--}}
                                {{--<div class="caption pull-left"><h4>Inbound messaged per week</h4> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="portlet-body">--}}
                        {{--<div class="row mbm">--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<div class="table-responsive">--}}
                                    {{--<div class="panel panel-red filterable">--}}
                                        {{--<div class="panel-heading">--}}
                                            {{--<h3 class="panel-title">&nbsp</h3>--}}
                                            {{--<div class="pull-right">--}}
                                                {{--<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<table class="table">--}}
                                            {{--<thead>--}}
                                            {{--<tr class="filters">--}}
                                                {{--<th><input type="text" class="form-control hidden" placeholder="#" disabled></th>--}}
                                                {{--<th width="">Messages</th>--}}
                                                {{--<th><input type="text" class="form-control" placeholder="Week" disabled></th>--}}

                                            {{--</tr>--}}
                                            {{--</thead>--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td>1</td>--}}
                                                {{--<td>120</td>--}}
                                                {{--<td>1</td>--}}

                                            {{--</tr>--}}
                                            {{--<tr>--}}
                                                {{--<td>2</td>--}}
                                                {{--<td>140</td>--}}
                                                {{--<td>2</td>--}}
                                            {{--</tr>--}}
                                            {{--<tr>--}}
                                                {{--<td>3</td>--}}
                                                {{--<td>190</td>--}}
                                                {{--<td>3</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<div class="clearfix"></div>--}}
                                        {{--<ul class="pagination pull-right pagination-red">--}}
                                            {{--<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>--}}
                                            {{--<li class="active"><a href="#">1</a></li>--}}
                                            {{--<li><a href="#">2</a></li>--}}
                                            {{--<li><a href="#">3</a></li>--}}
                                            {{--<li><a href="#">4</a></li>--}}
                                            {{--<li><a href="#">5</a></li>--}}
                                            {{--<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-6">--}}
                {{--<div class="portlet box">--}}
                    {{--<div class="portlet-header">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-8">--}}
                                {{--<div class="caption pull-left"><h4> Something else</h4> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div id="table-sorter-tab" class="portlet-body">--}}
                        {{--<div class="row mbm">--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<div class="table-responsive">--}}
                                    {{--<div class="panel panel-green filterable">--}}
                                        {{--<div class="panel-heading">--}}
                                            {{--<h3 class="panel-title">&nbsp</h3>--}}
                                            {{--<div class="pull-right">--}}
                                                {{--<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<table class="table table-hover table-striped table-bordered table-advanced tablesorter">--}}
                                            {{--<thead>--}}
                                            {{--<tr class="filters">--}}
                                                {{--<th width="1%"></th>--}}
                                                {{--<th><input type="text" class="form-control" placeholder="Date" data-field="name" data-align="center" data-sortable="true" disabled></th>--}}
                                            {{--</tr>--}}
                                            {{--</thead>--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td>5</td>--}}
                                                {{--<td>20-05-2014</td>--}}
                                            {{--</tr>--}}
                                            {{--<tr>--}}
                                                {{--<td>5</td>--}}
                                                {{--<td>20-05-2014</td>--}}
                                            {{--</tr>--}}
                                            {{--<tr>--}}
                                                {{--<td>5</td>--}}
                                                {{--<td>20-05-2014</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<div class="clearfix"></div>--}}
                                        {{--<ul class="pagination pull-right pagination-green">--}}
                                            {{--<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>--}}
                                            {{--<li class="active"><a href="#">1</a></li>--}}
                                            {{--<li><a href="#">2</a></li>--}}
                                            {{--<li><a href="#">3</a></li>--}}
                                            {{--<li><a href="#">4</a></li>--}}
                                            {{--<li><a href="#">5</a></li>--}}
                                            {{--<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>


    </div>

@stop
@section('new-js')
    <script>
        $(function () {
            $('#pie-chart').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Farmers per Value Chain'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Poultry',
                        y: 100
                    }]
                }]
            });
        });
        $(function () {
            $('#bar-chart').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Average Registrations'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'No of farmers'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Lamu East',
                    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

                }, {
                    name: 'Lamu West',
                    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

                }, {
                    name: 'Lamu south',
                    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

                }, {
                    name: 'Lamu town',
                    data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

                }]
            });
        });
    </script>
    @stop