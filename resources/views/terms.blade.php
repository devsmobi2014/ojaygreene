<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ojay Greene</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="css/orange.css" id="theme-change" class="style-change color-change">
    <link type="text/css" rel="stylesheet" href="css/custom.css">
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom" class="frontend-one-page">
<!--BEGIN PAGE LOADER-->
<!--END PAGE LOADER-->
<!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
<!--END BACK TO TOP-->
<!--BEGIN CONTENT-->
<header>
    <div class="bg-overlay pattern">
        <nav role="navigation" class="navbar navbar-custom navbar-fixed-top navbar-terms">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button><a id="logo" href="/" class="navbar-brand"><img class="img-responsive" src="images/logo.png"></a>
                </div>
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav main-menu">

                        <li class="page-scroll"><a href="{{url('/')}}">Home</a>
                        </li>

                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>
</header>
<section id="about" class="section-content section-terms">
    <div class="container">
        <div class="row mbxxl">
            <div class="col-lg-12">
                <h2 class="section-heading"><span>Terms</span> And <span>Conditions</span></h2>
                <div class="line"></div>
                <li class="pull-left">The Ojay Greene platform is here to improve extension services to you as a farmer and to provide any kind of support that will aid your farm productivity.</li>

                <li class="pull-left">Communication with Ojay Greene is via text messaging at any point and calls are only when necessary</li>

                    <li class="pull-left">A farmer is expected to make any kind of reporting via text to this platform for action to be taken

                    swiftly.</li>

                    <li class="pull-left">A farmer is expected to seek advice on production challenges via this platform in order to

                    improve productivity</li>

                    <li class="pull-left">Query messages from Ojay Greene are to be responded to promptly before 24 hours</li>

                    <li class="pull-left">Communication from Ojay Greene agronomists on various activity schedules should be adhered

                    to and on the specified timelines</li>

                   <li class="pull-left"> A farmer is liable to any occurrences if instructions by Ojay Greene Agronomists are not

                    followed</li>
            </div>
        </div>

    </div>
</section>

<footer>
    <div class="container">
        <p class="text-center yellow">©2016 ojay greene</p>
    </div>
</footer>
<!--END CONTENT-->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="css/bootstrap/js/bootstrap.min.js"></script>
<script src="css/bootstrap/js/bootstrap-hover-dropdown.js"></script>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<script src="vendors/modernizr.custom.97074.js"></script>
<!--Loading script for each page-->
<script src="js/jquery.backstretch.min.js"></script>
<script src="js/one-page_slider.js"></script>
<!--CORE JAVASCRIPT-->
<script src="js/frontend-one-page.js"></script>
</body>

</html>
