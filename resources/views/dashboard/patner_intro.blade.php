

@extends('layouts.patner')
@section('content')
<div class="new-partner">
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div id="sum_box" class="row mbl">
            <div class="col-sm-6 col-md-4">
                <div class="panel profit db mbm">
                    <div class="panel-body">
                        <div class="row">
                            <h2 class="intro">Partners</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <img class="partner-img-inner" src="/images/partner.jpg">
                            </div>
                            <div class="col-md-6">
                                <h4
                                        class="value creat-part"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                                       data-duration="0"></span><span>{{count($patners)}}</span></h4>

                                <a href="{{url('patner/create')}}" class="description creat-part">Create Partner</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">

            </div>
            <div class="col-sm-6 col-md-4">
                <div class="panel profit db mbm">
                    <div class="panel-body">
                        <div class="row">
                            <h2 class="intro">Ojay Greene</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <img class="partner-img-inner" src="/images/ojay.jpg">
                            </div>
                            <div class="col-md-6">
                                <h4
                                        class="value creat-part"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                                       data-duration="0"></span><span>
                                                                </span></h4>

                                <a href="{{url('dashboard')}}" class="description creat-part">Dashboard</a></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="caption pull-left"><h4> Partners</h4> </div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">&nbsp</h3>

                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Partner Name" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Phone Number" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Actions" disabled></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($patners)
                                                <?php $n=1;?>
                                                @foreach($patners as $patner)
                                            <tr>
                                                <td>{{$n}}</td>
                                                <td>{{$patner->names}}</td>
                                                <td>{{$patner->phone}}</td>
                                                <td>
                                                    <a type="button" href="{{url('patner/view',['id'=>$patner->id])}}" class="btn btn-default btn-xs"><i
                                                                class="fa fa-eye"></i>&nbsp;
                                                        View
                                                    </a>
                                                    <a type="button" href="{{url('patner/view',['id'=>$patner->id])}}" class="btn btn-default btn-xs"><i
                                                                class="fa fa-edit"></i>&nbsp;
                                                        Edit
                                                    </a>
                                                </td>
                                            </tr>
                                                    <?php $n++;?>
                                            @endforeach

                                            @endif


                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        <ul class="pagination pull-right pagination-green">
                                            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop