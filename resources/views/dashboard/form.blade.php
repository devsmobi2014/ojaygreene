<div class="col-xs-6 col-centered">
<div class="form-group">
    {!! Form::label('extension', 'Extension: ') !!}



    <div class="input-prepend">
        <span class="add-on">*384*2014*</span>
    {!! Form::text('extension', null, ['class' => 'input-prepend', 'placeholder' => 'extension...']) !!}
        <span class="add-on">#</span>
    </div>
</div>
<div class="form-group">
    {!! Form::label('url', 'Callback URL: ') !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Type in a url here...']) !!}
</div>
    <div class="form-group">
        {!! Form::label('Default Message', 'Default Message: ') !!}
        {!! Form::text('default_message', null, ['class' => 'form-control', 'placeholder' => 'Type in a default here...']) !!}
    </div>
<div class="form-group" id="create_btn">
    {!! Form::submit($submit_text, ['class' => 'btn btn-sm btn-success']) !!}
</div>
</div>