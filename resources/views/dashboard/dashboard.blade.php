    @extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('dashboard') !!}
            </div>

            <div class="clearfix"></div>
        </div>

    <div class="page-content">
        <div id="sum_box" class="row">
            <div class="col-sm-6 col-md-4">
                <div class="panel profit db mbm">
                    <div class="panel-body"><p class="icon "><i class="icon icon-farmer fa fa-user"></i></p><h4
                                class="value"><span data-counter="" data-start="10" data-end="50" data-step="1"
                                                    data-duration="0">{{$farmers}}</span></h4>

                        <p class="description">Farmers registered</p>

                        <div class="progress progress-sm mbn">
                            <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 80%;" class="progress-bar progress-bar-yellow"><span
                                        class="sr-only">80% Complete (success)</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="panel income db mbm">
                    <div class="panel-body"><p class="icon "><i class="icon icon-crop fa fa-leaf"></i></p><h4
                                class="value"><span>{{$crops}}</span></h4>

                        <p class="description">Crops planted</p>

                        <div class="progress progress-sm mbn">
                            <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 80%;" class="progress-bar progress-bar-success"><span
                                        class="sr-only">60% Complete (success)</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="panel task db mbm">
                    <div class="panel-body"><p class="icon "><i class="icon icon-message fa fa-envelope"></i></p>
                        <h4 class="value"><span>{{$messages_sent}}</span></h4>
                        <p class="description">Messages sent</p>
                        <h4 class="value"><span>{{$recieved_messages}}</span></h4>
                        <p class="description">Recieved messages</p>

                        <div class="progress progress-sm mbn">
                            <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 80%;" class="progress-bar progress-bar-danger"><span
                                        class="sr-only">50% Complete (success)</span></div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="caption pull-left"><h4> Land in acres per region      Total acreage : {{$total_acreage}}</h4> </div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-yellow filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">&nbsp</h3>
                                            <div class="pull-right">
                                                </div>
                                        </div>

                                        <div style="min-width: 500px; height: 500px; max-width: 800px; margin: 0 auto" id="regions_div">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="caption pull-left"><h4> Expected yield per crop.</h4> </div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">&nbsp</h3>

                                        </div>
                                        <table id="get-cohorts" class="table">
                                            <thead>
                                            <tr>

                                                <th>Cohort</th>
                                                <th>Start Week</th>
                                                <th>Harvest Week</th>
                                                <th>Expected Yield</th>
                                                <th>Projected Yield</th>
                                                <th>Actual Yield</th>
                                            </tr>
                                            </thead>

                                        </table>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="sum_boxs" class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="caption pull-left"><h4> Monthly registered and active farmers</h4> </div>
                            </div>
                        </div>
                    </div>
                    <div id="table-sorter-tab" class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">&nbsp</h3>

                                        </div>

                                        <div id="registered_active"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="caption pull-left"><h4> Messages per region</h4> </div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-red filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">&nbsp</h3>

                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <td>Region</td>
                                                <th> Messages sent</th>
                                                <th>Messages in</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($messages_per_region as $message)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$message->name}}</td>
                                                <td>{{$message->messages_out}}</td>
                                                <td>{{$message->messages_in}}</td>

                                            </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        {!! $messages_per_region->render() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="sum_boxs" class="row">


        </div>


    </div>
@stop
    @section('page_scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script type="text/javascript">
        $(function () {

            $(document).ready(function () {

                // Build the chart
                $('#regions_div').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Acreage per region'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Regions',
                        colorByPoint: true,
                        data:{!!$regions_acreage!!}
                    }]
                });
            });
        });
        $(function () {
            $('#registered_active').highcharts({
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Monthly Registrations vs active farmers'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: {!!$reg_cat!!}
                },
                yAxis: {
                    title: {
                        text: 'no'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Monthly registrations',
                    data: {{$reg_per_month}}
                }, {
                    name: 'Monthly Active farmers',
                    data: {{$act_per_month}}
                }]
            });
        });
    </script>
    @stop
    @section('js')
    $(function() {
    $('#get-cohorts').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": true,
    ajax: '{{ url('/dash/get-cohorts') }}',
    "columns": [
    {
    "data": "name",
    "name": "rounds.name",
    "defaultContent": "<i>Not set</i>"
    },
    {data: 'start_date', name: 'rounds.start_date', searchable: false},
    {data: 'harvest_date', name: 'rounds.harvest_date', searchable: false},
    {data: 'expected_yield', name: 'rounds.expected_yield', searchable: false},
    {data: 'projected_yield', name: 'rounds.projected_yield', searchable: false},
    {data: 'actual_yield', name: 'rounds.actual_yield', searchable: false},

    ]
    });
    });


    $("#regions").on("click", ".delete", function(){
    if(confirm("Are you sure you want to delete this region")){
    return true;
    }
    return false;
    });

    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.regions').addClass("active");


    @endsection
