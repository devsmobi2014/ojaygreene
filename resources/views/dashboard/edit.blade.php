@extends('app')

@section('content')

<div class="container">

    <h1 id="header">Edit Resource</h1>
    <hr>

    @include('errors.valid')

    {!! Form::model($dash_resource, ['method' => 'PATCH', 'action' => ['DashController@update', $dash_resource->id]]) !!}
        <div class="form-group">
            {!! Form::label('url', 'URL: ') !!}
            {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Type in a url here...']) !!}
        </div>
        <div class="form-group" id="create_btn">
            {!! Form::submit('Update Resource', ['class' => 'btn btn-sm btn-success']) !!}
        </div>
    {!! Form::close() !!}

</div>

@endsection

@section('footer')