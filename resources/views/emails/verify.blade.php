<!DOCTYPE html>
<html lang="en">
<body>

<h2>Verify Your Email Address</h2>

<div>
	You have been added to the Ojay Greene platform as an agronomist.
    Use your email and the password : {{$password}} to login.
    Please remember to change your password after login.
	Please follow the link below to verify your email address
	{{ URL::to('emails/verified/'. $code) }}.<br>
</div>

</body>
</html>
