@extends('app')

@section('content')

	<h1 id="header">Verified Account</h1>
	<hr>
	<p>Your account has been successfuly verified.</p>
	<a href="/home" class="btn btn-sm btn-success">Continue</a>

@stop

@section('footer')
