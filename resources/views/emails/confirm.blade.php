@extends('app')

@section('content')
<div class="container" id="dashboard_container">

    <h1 id="header">Account Confirmation</h1>
    <hr>

    @include('errors.message')

    <p>Please verify your account to proceed.</p>

</div>

@endsection()

@section('footer')