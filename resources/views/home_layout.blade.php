<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ojay Greene</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="/images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="/css/orange.css" id="theme-change" class="style-change color-change">
    <link type="text/css" rel="stylesheet" href="/css/custom.css">
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom" class="frontend-one-page">
</div>
<!--END PAGE LOADER-->
<!--BEGIN BACK TO TOP--><a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
<!--END BACK TO TOP-->
<!--BEGIN CONTENT-->
<header>
    <div class="bg-overlay pattern">
        <nav role="navigation" class="navbar navbar-custom navbar-fixed-top">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button><a id="logo" href="/" class="navbar-brand"><img class="img-responsive" src="images/logo.png"></a>
                </div>
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav main-menu">
                        <li class="page-scroll"><a href="#contact">Contact us</a>
                        </li>
                        <li class="page-scroll"><a href="{{ url('/terms') }}">Terms and conditions</a>
                        <li class="page-scroll"><a class="btn btn-yellow" href="{{ route('sentinel.login') }}">Login</a>

                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <section class="intro">
            <div class="intro-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="slide-intro">
                            </div>
                            <div class="section-next page-scroll"><a href="#about" class="btn btn-circle"><i class="fa fa-angle-down"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</header>

@yield('content')
<footer>
    <div class="container">
        <p class="text-center yellow">©2015 ojay greene</p>
    </div>
</footer>
<!--END CONTENT-->
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="/css/bootstrap/js/bootstrap.min.js"></script>
<script src="/css/bootstrap/js/bootstrap-hover-dropdown.js"></script>
<script src="/js/html5shiv.js"></script>
<script src="/js/respond.min.js"></script>
<script src="/vendors/modernizr.custom.97074.js"></script>
<!--Loading script for each page-->
<script src="/js/jquery.backstretch.min.js"></script>
<script src="/js/one-page_slider.js"></script>
<!--CORE JAVASCRIPT-->
<script src="/js/frontend-one-page.js"></script>
</body>

</html>

