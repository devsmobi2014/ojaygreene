@extends('layouts.master')

@section('content')
    <div id="page-wrapper" xmlns="http://www.w3.org/1999/html"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">


            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-sm-3 col-md-2"></div>
                <div class="col-sm-9 col-md-10">
                    <div class="pull-right">

                        <div class="btn-group mlm">
                            {{--<button type="button" class="btn btn-default"><span--}}
                            {{--class="glyphicon glyphicon-chevron-left"></span></button>--}}
                            {{--<button type="button" class="btn btn-default"><span--}}
                            {{--class="glyphicon glyphicon-chevron-right"></span></button>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="mtl mbl"></div>
            <div class="row">
                <div class="col-sm-3 col-md-2"><a href="{{url('rounds/view',$round_id)}}" role="button" class="btn btn-danger btn-sm btn-block">Back </a>

                    <div class="mtm mbm"></div>
                    <div class="panel">
                        {{--<div class="panel-body pan">--}}
                        {{--<ul style="background: #fff" class="nav nav-pills nav-stacked">--}}
                        {{--<li class="active"><a href="{{url('messages')}}"><i class="fa fa-inbox fa-fw mrs"></i>Inbox</a></li>--}}
                        {{--<li><a href="/messages/sent"><i class="fa fa-plane fa-fw mrs"></i>Sent Messages</a></li>--}}
                        {{--<li><a href="/messages/activity-messages"><i class="fa fa-plane fa-fw mrs"></i>Activity  Messages</a></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                    </div>

                </div>
                <div class="col-sm-9 col-md-10">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#home" data-toggle="tab"><span
                                        class="glyphicon glyphicon-inbox"></span>&nbsp;
                                Message farmer</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <div class="list-group mail-box">

                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-envelope"></i> {{$farmer->name}}</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                @if (session('message'))
                                    <div class="alert alert-success">
                                        {{ session('message') }}
                                    </div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="{{url('rounds/send')}}" method="post" class="form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="phone" value="{{$farmer->phone}}">
                                    <input type="hidden" name="round_id" value="{{$round_id}}">
                                    <div class="form-group">
                                        <textarea name="message" required="" class="form-control" rows="3"></textarea><br>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default pull-right">Send</button>
                                    </div>

                                </form>
                                </br>
                                </br>
                                {{--<div class="panel-body">--}}
                                    {{--@if($interactions_array)--}}
                                        {{--@foreach($interactions_array as $message)--}}
                                            {{--@if($message['direction']=='in')--}}
                                                {{--<div class="patient">--}}
                                                    {{--<p class="chat">{{$message['message']}}<br>--}}
                                                        {{--<small class="pull-right">Time:--}}
                                                            {{--<cite title="Source Title">{{$message['created_at']}}</cite>--}}
                                                        {{--</small>--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                            {{--@elseif($message['direction']=='out')--}}
                                                {{--<div class="supporter">--}}
                                                    {{--<p class="chat">{{$message['message']}}.<br>--}}
                                                        {{--<small class="pull-right">By--}}
                                                            {{--<cite title="Source Title">{{$farmer->name}}</cite>&nbsp;&nbsp; Time:--}}
                                                            {{--<cite title="Source Title">{{$message['created_at']}}</cite>--}}
                                                        {{--</small>--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                            {{--@endif--}}
                                        {{--@endforeach--}}
                                    {{--@endif--}}



                                {{--</div>--}}
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    </div>
@stop