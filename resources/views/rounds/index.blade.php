@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('crop',$region,$crop) !!}
            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Time Cohorts</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{url('rounds/create',$id)}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;Create Time Cohort</a></div>

                            </div>

                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Time cohorts</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Name" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Start Week" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Harvest Week" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Status" disabled></th>

                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($rounds)
                                                <?php $n=0; ?>
                                            @foreach($rounds as $round)
                                                <?php $n++?>
                                            <tr>
                                                <td>{{$n}}</td>
                                                <td>{{$round->name}}</td>
                                                <td>{{$round->start_date}}</td>
                                                <td>{{$round->harvest_date}}</td>
                                                <td>@if($round->status):active @else closed  @endif</td>
                                                <td>
                                                    <ul class="list-unstyled list-inline">
                                                        <li><a href="{{url('/rounds/view',$round->id)}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                        class="fa fa-eye"></i>&nbsp;
                                                            </a></li>
                                                        <li><a href="{{url('rounds/edit',$round->id)}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                        class="fa fa-pencil"></i>&nbsp;
                                                            </a></li>
                                                        <li><a  href="{{url('rounds/destroy',$round->id)}}" class="btn btn-green btn-outlined btn-sm delete"><i
                                                                        class="fa fa-trash-o"></i>&nbsp;
                                                            </a></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif

                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        <ul class="pagination pull-right pagination-green">
                                            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop