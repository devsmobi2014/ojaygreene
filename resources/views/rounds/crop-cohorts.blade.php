@extends('layouts.master')

@section('content')
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            {{--{!! Breadcrumbs::render('crops') !!}--}}
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Value chains</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a href="{{url('view-harvest-dates')}}" class="btn btn-success pull-right">View Harvest dates</a>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Value Chains</h3>
                                        </div>
                                        <table  class="table">
                                            <thead>
                                            <tr>
                                                <td>#</td>
                                                <th> name</th>
                                                <th> No of cohorts</th>
                                                <th>View</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($crops)
                                                  <?php $n=1; ?>
                                               @foreach($crops as $crop)
                                            <tr>
                                               <td>{{$n}}</td>
                                               <td>{{$crop->name}}</td>
                                                <td>{{$crop->no_of_rounds}}</td>
                                                <td> <a href="{{ url('crop-cohorts/all',['id'=>$crop->id])}}"><i class="fa fa-eye"></i> View Cohorts</a> </td>
                                            </tr>

                                                      <?php $n++;?>
                                              @endforeach

                                            @endif
                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
$(function() {
$('#cohorts-table').DataTable({
processing: true,
serverSide: true,
"bStateSave": true,
ajax: '{{ url('get-rounds') }}',
"columns": [
{
"data": "name", // can be null or undefined
"defaultContent": "<i>Not set</i>",
searchable: true
},

{
"data": "start_date", // can be null or undefined
"defaultContent": "<i>Not set</i>",
searchable: true
},
{
"data": "harvest_date", // can be null or undefined
"defaultContent": "<i>Not set</i>",
},

{
"data": "id", // can be null or undefined
"render": function(data, type, full, meta) {


return '<a href="/rounds/view/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-eye"></i></a><a href="/rounds/edit/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-pencil"></i></a><a href="/rounds/destroy/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm delete"><i class="fa fa-trash"></i></a>';
},
"defaultContent": "<i>Not set</i>"
},
]
});
});

$("#cohorts-table").on("click", ".delete", function(){
if(confirm("Are you sure you want to delete this?")){
return true;
}
return false;
});
$("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
$('.cohorts').addClass("active");
@endsection