@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

    <div class="page-content">
        <div class="row">
            <div class="col-lg-8">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="caption">Edit a Time Cohort</div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">Edit Time Cohort</div>
                                    <div class="panel-body pan">
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form class="form-horizontal" role="form" method="PATCH" action="{{url('/rounds/update',$round->id)}}">
                                            <div class="form-body pal">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="region_crop_id" value="{{$round->region_crop_id}}">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left"><i class="fa fa-map-marker"></i><input
                                                                    id="RegionName" value="{{$round->name}}" name="name" type="text" placeholder="" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Start Week</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left"><input
                                                                    id="start_date datepicker" value="{{$round->start_date}}" name="start_date" type="text" placeholder="" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Harvest Week</label>
                                                    <div class="col-md-9">
                                                        <div class="input-icon left"><input
                                                                    id="start_date datepicker" value="{{$round->harvest_date}}" name="harvest_date" type="text" placeholder="" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions pal">
                                                <div class="form-group mbn">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-yellow pull-right">Edit a Time Cohort</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
