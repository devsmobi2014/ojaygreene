@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="caption">Create a Time Cohort</div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="panel panel-yellow">
                                        <div class="panel-heading">New Time Cohort</div>
                                        <div class="panel-body pan">
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <form class="form-horizontal" role="form" method="POST" action="/rounds/create-cohort">
                                                <div class="form-body pal">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Name</label>
                                                        <div class="col-md-9">
                                                            <div class="input-icon left"><i class="fa fa-map-marker"></i><input
                                                                        id="RegionName" value="{{$data['name']}}"  name="name" type="text" placeholder="" class="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Start Week</label>
                                                        <div class="col-md-9">
                                                            <div class="input-icon left"><input
                                                                        id="start_date datepicker" value="{{$data['start_date']}}" name="start_date" type="text" placeholder="" class="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Harvest Week</label>
                                                        <div class="col-md-9">
                                                            <div class="input-icon left"><input
                                                                        id="start_date datepicker" value="{{$data['harvest_date']}}" name="harvest_date" type="text" placeholder="" class="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Regions</label>
                                                        <div class="col-md-9">
                                                            @foreach($regions_array as $region)
                                                                <label>

                                                                    <input checked type="checkbox" name="regions[]"value="<?php echo $region->id ?>" onclick="return false"/>
                                                                    {{$region->name}}
                                                                </label>

                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="panel-heading">Select Farmers</div>
                                                        <div class="row">
                                                            <div class="col-lg-1"></div>
                                                            <div class="col-lg-11">
                                                                @if($farmers_array)
                                                                    <?php $n=0; ?>
                                                                    @foreach($farmers_array as $farmer)
                                                                        <?php $n++?>
                                                                        <table>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>{{$n}}</td>
                                                                                <td><input type="checkbox" name="farmer_id[]"value="<?php echo $farmer->id ?>" /></td>
                                                                                <td>{{$farmer->name}}</td>
                                                                                <td>{{$farmer->phone}}</td>
                                                                                <td></td>


                                                                            </tr>

                                                                            </tbody>
                                                                        </table>

                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="form-actions pal">
                                                    <div class="form-group mbn">
                                                        <div class="col-md-12">
                                                            <button type="submit" class="btn btn-yellow pull-right">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
