@extends('layouts.master')
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
<div class="page-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="portlet box">
                <div class="portlet-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption pull-left">
                                <ul class="list-unstyled list-inline">
                                    <li><a href="region-crop-cohort.html"><i class="fa fa-arrow-circle-left fa-4"></i> </a></li>
                                    <li><h4>Add farmers to this cohort</h4></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row mbm">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <div class="panel panel-red filterable">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{$round->name}} cohort</h3>
                                        <div class="pull-right">
                                            <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                        </div>
                                    </div>
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                    <table id="farmers_add" class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Select</th>

                                            <th>Farmers Name</th>
                                            <th>Phone Number</th>
                                            <th>Cohorts</th>
                                            <th>Region</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if($farmers_array)
                                            <?php $n=0; ?>
                                            @foreach($farmers_array as $farmer)
                                                <?php $n++?>
                                                <tr id=" {{$farmer['farmer_id']}}">

                                                    <td>{{$n}}</td>
                                                    <td><input type="checkbox" class="seelect" name="farmer_id[]" value="{{$farmer['farmer_id']}}"/></td>
                                                    <td>{{$farmer['name']}}</td>
                                                    <td>{{$farmer['phone']}}</td>
                                                    <td><a tabindex="0" data-toggle="popover" class=".pop" data-trigger="focus" title="Cohorts" data-content="
                                                     @if($farmer['cohorts'])
                                                                @foreach($farmer['cohorts'] as $cohort)
                                                                {{$cohort->name}},
                                                                @endforeach
                                                        @endif
                                                                ">View Cohorts</a></td>
                                                    <td>{{$farmer['region_name']}}</td>


                                                </tr>
                                            @endforeach
                                        @endif

                                        </tbody>

                                    </table>
                                    <form method="post" action="/rounds/store-farmer">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                        <input type="hidden" name="round_id" value="{{ $round->id }}"/>
                                        <input id="farmers" type="hidden" name="farmer_id" value="" />

                                        <div class="form-actions text-right pal">
                                            <button id="sub" type="submit" class="btn btn-green">Submit</button>
                                        </div>
                                    </form>


                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
@section('js')
    $(document).ready(function() {
    $('[data-toggle="popover"]').popover();
    var selected = [];

    $("#farmers_add").DataTable();

    $('#farmers_add tbody').on('click', '.seelect', function () {

    var id = this.value;

    var index = $.inArray(id, selected);

    if ( index === -1 ) {
    selected.push( id );
    } else {
    selected.splice( index, 1 );
    }


    $(this).toggleClass('selected');
    } );

    $('#sub').on('click',function(){

       $('#farmers').val(selected);



    });


    } );
    @stop