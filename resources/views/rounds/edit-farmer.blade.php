@extends('layouts.master')
@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">

                        <div class="caption"><a href="region-crop-farmer-cohort.html"><i class="fa fa-arrow-circle-left fa-4"></i> </a>&nbsp;&nbsp;Edit Farmer Details</div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="panel panel-red">
                                    <div class="panel-heading">Edit  farmer land details</div>
                                    <div class="panel-body pan">
                                        <form method="post" role="form" action="/rounds/storefarmerdetails">
                                            <div class="form-body pal">
                                                <div class="row">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="farmer_id" value="{{$farmer->id}}">
                                                    <input type="hidden" name="id" value="{{$round_farmer->id}}">
                                                    <input type="hidden" name="round_id" value="{{$round_farmer->round_id}}">
                                                    <div class="col-md-4">
                                                        <div class="form-group"><label class="control-label">Acreage</label>

                                                            <div class="input-icon left"><i class="fa fa-square-o"></i><input
                                                                        id="inputSubject" name="acreage"  value="{{$round_farmer->acreage}}" type="text" placeholder="" class="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group"><label class="control-label">Expected Yield</label>

                                                            <div class="input-icon left"><i class="fa fa-tasks"></i>
                                                                <input id="expected_yield" name="expected_yield"  value="{{$round_farmer->expected_yield}}" type="text" placeholder=""
                                                                        class="form-control"/></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group"><label class="control-label">Projected Yield</label>

                                                            <div class="input-icon left"><i class="fa fa-tasks"></i>
                                                                <input
                                                                        id="proYield" type="text" placeholder="" name="projected_yield"  value="{{$round_farmer->projected_yield}}"
                                                                        class="form-control"/></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group"><label class="control-label">Actual Yield</label>

                                                            <div class="input-icon left"><i class="fa fa-reorder"></i><input
                                                                        name="actual_yield"  value="{{$round_farmer->actual_yield}}"
                                                                        id="actYield" type="text" placeholder=""
                                                                        class="form-control"/></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-red">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
<!--END CONTENT-->