@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {{--{!! Breadcrumbs::render('crops') !!}--}}
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><h4>Filter cohorts with harvest week</h4></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{url('cohorts')}}" class="btn btn-success pull-right">Back to cohorts</a>
                                </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Select week</h3>
                                            </div>


                                                <form action="{{url('filter-harvest-dates')}}" method="post" class="form">
                                                    <div class="form-group">
                                                        <label for="exampleInputName2">Select Week</label>
                                                        <select class="form-control " name="week">
                                                            @for($i=1;$i<53; $i++)
                                                                @if($i==$default_week)
                                                                    <option value="{{$i}}" selected>{{$i}}</option>
                                                                @else
                                                                    <option value="{{$i}}">{{$i}}</option>
                                                                @endif
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                    <button type="submit" class="btn btn-default">Submit</button>
                                                </form>



                                            <div class="panel-heading">
                                                <h3 class="panel-title">Week {{$default_week}} Harvest cohorts</h3>
                                            </div>

                                            <table  class="table">
                                                <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <th> name</th>

                                                    <th>View</th>


                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($rounds)
                                                    <?php $n=1; ?>
                                                    @foreach($rounds as $round)
                                                        <tr>
                                                            <td>{{$n}}</td>
                                                            <td>{{$round->name}}</td>

                                                            <td> <a href="{{url('/rounds/view',$round->id)}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                            class="fa fa-eye"></i>&nbsp;
                                                                </a> </td>
                                                        </tr>

                                                        <?php $n++;?>
                                                    @endforeach

                                                @else
                                                    <tr>No cohorts ready for harvesting</tr>
                                                @endif
                                                </tbody>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    $(function() {
    $('#cohorts-table').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": true,
    ajax: '{{ url('get-rounds') }}',
    "columns": [
    {
    "data": "name", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    searchable: true
    },

    {
    "data": "start_date", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    searchable: true
    },
    {
    "data": "harvest_date", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    },

    {
    "data": "id", // can be null or undefined
    "render": function(data, type, full, meta) {


    return '<a href="/rounds/view/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-eye"></i></a><a href="/rounds/edit/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-pencil"></i></a><a href="/rounds/destroy/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm delete"><i class="fa fa-trash"></i></a>';
    },
    "defaultContent": "<i>Not set</i>"
    },
    ]
    });
    });

    $("#cohorts-table").on("click", ".delete", function(){
    if(confirm("Are you sure you want to delete this?")){
    return true;
    }
    return false;
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.cohorts').addClass("active");
@endsection