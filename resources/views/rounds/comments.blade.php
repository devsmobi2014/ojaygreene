@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><h4>Activities</h4></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="new-region pull-right"><a href="{{url('round_comment/new',$round->id)}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Comment</a></div>
                                    <div class="new-region pull-right"><a href="{{url('rounds/view',$round->id)}}" class="btn btn-green btn-outlined btn-sm ">&nbsp;&nbsp;Back</a></div>


                                </div>

                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Comments</h3>
                                                <div class="pull-right">
                                                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                                </div>
                                            </div>
                                            <table class="table">
                                                <thead>
                                                <tr class="filters">
                                                    <th>#</th>
                                                    <th>Comment</th>
                                                    <th>names</th>
                                                    <th>Date</th>

                                                    <th>Actions</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($comments)
                                                    <?php $n=0; ?>
                                                    @foreach($comments as $comment)
                                                        <?php $n++?>
                                                        <tr>
                                                            <td>{{$comment->id}}</td>
                                                            <td>{{$comment->comment}}</td>
                                                            <td>{{$comment->username}} </td>
                                                            <td>{{$comment->created_at}}</td>
                                                            <td>
                                                                <ul class="list-unstyled list-inline">

                                                                    {{--<li><a href="{{url('/round_comments/edit',$comment->id)}}" class="btn btn-green btn-outlined btn-sm"><i--}}
                                                                                    {{--class="fa fa-pencil"></i>&nbsp;--}}
                                                                        {{--</a></li>--}}
                                                                    <li><a  href="{{url('/round_comments/destroy',$comment->id)}}" class="btn btn-green btn-outlined btn-sm delete"><i
                                                                                    class="fa fa-trash-o delete"></i>&nbsp;
                                                                        </a></li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop