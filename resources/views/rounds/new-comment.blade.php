@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="caption">New Comment on {{$round->name}}</div>
                            <div class="new-region pull-right"><a href="{{url('rounds/comments',$round->id)}}" class="btn btn-green btn-outlined btn-sm ">&nbsp;&nbsp;Back</a></div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="panel panel-yellow">

                                        <div class="panel-body pan">
                                            @if (session('message'))
                                                <div class="alert alert-success">
                                                    {{ session('message') }}
                                                </div>
                                            @endif
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <form class="form-horizontal" role="form" method="POST" action="/round/store-comment">
                                                <div class="form-body pal">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="round_id" value="{{$round->id}}">
                                                    <input type="hidden" name="user_id" value="{{Sentry::getUser()->id}}">

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Comment</label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control" rows="3" name="comment"></textarea>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="form-actions pal">
                                                    <div class="form-group mbn">
                                                        <div class="col-md-12">
                                                            <button type="submit" class="btn btn-yellow pull-right">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
