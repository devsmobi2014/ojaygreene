@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {{--{!! Breadcrumbs::render('crops') !!}--}}
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><h4>{{$crop->name}} Cohorts</h4></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    </div>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <div class="panel panel-green filterable">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">{{$crop->name}} Cohorts</h3>
                                                <div class="pull-right">
                                                    {{--<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>--}}
                                                </div>
                                            </div>
                                            <table id="cohorts-table" class="table">
                                                <thead>
                                                <tr class="filters">

                                                    <th> name</th>
                                                    <th>Start week</th>
                                                    <th>Harvest week</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>

                                                </tr>
                                                </thead>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    $(function() {
    $('#cohorts-table').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": true,
    ajax: '{{ url('get-rounds',['id'=>$crop->id]) }}',
    "columns": [
    {
    "data": "name", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    searchable: true
    },

    {
    "data": "start_date", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    searchable: true
    },
    {
    "data": "harvest_date", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    },
    {
    "data": "status",
    "defaultContent": "<i>Not set</i>",
    "render": function(data, type, full, meta){
    return (data == 1) ? 'Active' : 'Closed';
    }
    },

    {
    "data": "id", // can be null or undefined
    "render": function(data, type, full, meta) {


    return '<a href="/rounds/view/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-eye"></i></a><a href="/rounds/edit/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-pencil"></i></a><a href="/rounds/destroy/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm delete"><i class="fa fa-trash"></i></a>';
    },
    "defaultContent": "<i>Not set</i>"
    },
    ]
    });
    });

    $("#cohorts-table").on("click", ".delete", function(){
    if(confirm("Are you sure you want to delete this?")){
    return true;
    }
    return false;
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.cohorts').addClass("active");
@endsection