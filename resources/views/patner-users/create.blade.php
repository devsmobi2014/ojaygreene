@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="crops.html"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>New User</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">New User Details</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patner-users/store')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-body pal">
                                                <div class="form-group"><label class="control-label"> Name</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="name" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Email</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="email" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Role</label>

                                                    <div class="input-icon ">
                                                        <div class="radio">
                                                            <label><input class="role-radio form-control" value="admin" type="radio" name="role">Patner admin</label>
                                                        </div>
                                                        <div class="divider"></div>
                                                        <div class="divider"></div>
                                                        <div class="radio">
                                                            <label><input class="role-radio form-control" value="department" type="radio" name="role">Department user</label>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="form-group" style="display: none" id="departments"><label class="control-label"> Choose  Department</label>

                                                    <div class="input-icon left">
                                                        <select name="department_id" class="form-control">
                                                            <option>Select department</option>
                                                            @if($departments)
                                                                @foreach($departments as $department)
                                                                    <option value="{{$department->id}}">{{$department->names}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Phone Number</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="phone" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Password</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="password" type="password" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label">Repeat Password</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="password_confirmation" type="password" placeholder=""
                                                                class="form-control"/></div>
                                                </div>

                                                <div class="form-group"><label class="control-label">Description</label><textarea
                                                            rows="3" name="description" class="form-control"></textarea></div>
                                                <div class="form-group">
                                                    <label class="checkbox">
                                                        <input name="activate"  value="activate" type="checkbox"> Activate
                                                    </label>
                                                </div>

                                            </div>
                                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Create User</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @stop
@section('js')
    $(document).ready(function () {

        $('input[type="radio"].role-radio').click(function () {
            var $rb = $(this);

            if ($rb.val() === 'admin') {
                $('#departments').hide();
            }
            else {
                $('#departments').show();

            }
        });

    })
@stop
