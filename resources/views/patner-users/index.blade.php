@extends('layouts.patner_dash')

@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Users</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{url('patner-users/create')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New User</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Users</h3>
                                            <div class="pull-right">
                                                <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                            </div>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr class="filters">
                                                <th><input type="text" class="form-control hidden" placeholder="#" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Name" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Email" disabled></th>
                                                <th><input type="text" class="form-control" placeholder="Department" disabled></th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($users)
                                                <?php $n=1;?>
                                                @foreach($users as $user)
                                                    <tr>
                                                        <td>{{$n}}</td>
                                                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                                                        <td>{{$user->email}}</td>
                                                        <td>{{$user->department_name ? $user->department_name:"Management" }}</td>

                                                        <td width="35%">
                                                            <ul class="list-unstyled list-inline">
                                                                <li><a href="{{url('patner-users/manage-regions',['id'=>$user->id])}}" class="btn btn-green btn-outlined btn-sm">Manage Regions</i>&nbsp;
                                                                    </a></li>
                                                                <li><a href="{{url('patner-users/edit',['id'=>$user->id])}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-pencil"></i>&nbsp;
                                                                    </a></li>
                                                                <li><a href="{{url('patner-users/delete',['id'=>$user->id])}}" class="btn btn-green btn-outlined btn-sm"><i
                                                                                class="fa fa-trash-o"></i>&nbsp;
                                                                    </a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <?php $n++?>
                                                @endforeach
                                            @endif


                                            </tbody>
                                        </table>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop