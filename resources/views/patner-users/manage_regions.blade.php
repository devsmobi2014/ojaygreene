@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="{{url('patner-users')}}"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>New User Regions</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (session('message'))
                                        <div class="alert alert-success">
                                            {{ session('message') }}
                                        </div>
                                    @endif
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">Update User Regions</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patner-users/store-user-regions')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-body pal ">
                                                @if($regions)
                                                    @foreach($regions as $region)
                                                        @if($user_regions)


                                                                        <div class="form-group">
                                                                            <label class="checkbox">
                                                                                <input name="region[]" @foreach($user_regions as $reg)  @if($reg->region_id==$region->id) checked  @endif @endforeach  value="{{$region->id}}" type="checkbox"> {{$region->name}}
                                                                            </label>
                                                                        </div>


                                                         @else

                                                            <div class="form-group">
                                                                <label class="checkbox">
                                                                    <input name="region[]"  value="{{$region->id}}" type="checkbox"> {{$region->name}}
                                                                </label>
                                                            </div>
                                                        @endif

                                                        @endforeach


                                                    @endif


                                            </div>
                                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    $(document).ready(function () {

    $('input[type="radio"].role-radio').click(function () {
    var $rb = $(this);

    if ($rb.val() === 'admin') {
    $('#departments').hide();
    }
    else {
    $('#departments').show();

    }
    });

    })
@stop
