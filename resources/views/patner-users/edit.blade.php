@extends('layouts.patner_dash')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><a href="crops.html"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                        <li><h4>New User</h4></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-8">
                                <div class="panel panel-green">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="panel-heading">Edit User Details</div>
                                    <div class="panel-body pan">
                                        <form method="post" action="{{url('patner-users/update-user')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-body pal">
                                                <div class="form-group"><label class="control-label"> Name</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="name" value="{{$user->first_name}} {{$user->last_name}}" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Email</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="email" value="{{$user->email}}" type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                <div class="form-group"><label class="control-label"> Role</label>
                                                    @if($user_patner)
                                                        <div class="input-icon ">
                                                            @if(!$user_patner->department_id)

                                                            <div class="radio">
                                                                <label><input class="role-radio form-control" checked="true" value="admin" type="radio" name="role">Patner admin</label>
                                                            </div>
                                                                <div class="radio">
                                                                    <label><input class="role-radio form-control"  value="department" type="radio" name="role">Department user</label>
                                                                </div>

                                                            <div class="divider"></div>
                                                            <div class="divider"></div>
                                                            @else
                                                                <div class="radio">
                                                                    <label><input class="role-radio form-control"  value="admin" type="radio" name="role">Patner admin</label>
                                                                </div>
                                                            <div class="radio">
                                                                <label><input class="role-radio form-control" checked="true" value="department" type="radio" name="role">Department user</label>
                                                            </div>
                                                                @endif

                                                        </div>
                                                    @else

                                                    <div class="input-icon ">
                                                        <div class="radio">
                                                            <label><input class="role-radio form-control" value="admin" type="radio" name="role">Patner admin</label>
                                                        </div>
                                                        <div class="divider"></div>
                                                        <div class="divider"></div>
                                                        <div class="radio">
                                                            <label><input class="role-radio form-control" value="department" type="radio" name="role">Department user</label>
                                                        </div>

                                                    </div>
                                                     @endif

                                                </div>
                                                @if(!$user_patner->department_id)

                                                <div class="form-group" style="display: none" id="departments"><label class="control-label"> Choose  Department</label>
                                                    @else
                                                        <div class="form-group" id="departments"><label class="control-label"> Choose  Department</label>
                                                 @endif

                                                    <div class="input-icon left">
                                                        <select name="department_id" class="form-control">
                                                            <option>Select department</option>
                                                            @if($departments)
                                                                @foreach($departments as $department)
                                                                    @if($user_patner->department_id)
                                                                    <option value="{{$department->id}}" selected>{{$department->names}}</option>
                                                                    @else
                                                                        <option value="{{$department->id}}" >{{$department->names}}</option>
                                                                        @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                    <input type="hidden" name="user_id" value="{{$user->id}}" />
                                                    <input type="hidden" name="hash" value="{{$user->hash}}" />
                                                <div class="form-group"><label class="control-label"> Phone Number</label>

                                                    <div class="input-icon left"></i><input
                                                                id="cropName" name="phone"value="{{$user->phone}}"type="text" placeholder=""
                                                                class="form-control"/></div>
                                                </div>
                                                {{--<div class="form-group"><label class="control-label"> Password</label>--}}

                                                    {{--<div class="input-icon left"></i><input--}}
                                                                {{--id="cropName" name="password" type="password" placeholder=""--}}
                                                                {{--class="form-control"/></div>--}}
                                                {{--</div>--}}
                                                {{--<div class="form-group"><label class="control-label">Repeat Password</label>--}}

                                                    {{--<div class="input-icon left"></i><input--}}
                                                                {{--id="cropName" name="password_confirmation" type="password" placeholder=""--}}
                                                                {{--class="form-control"/></div>--}}
                                                {{--</div>--}}

                                                {{--<div class="form-group"><label class="control-label">Description</label><textarea--}}
                                                            {{--rows="3" name="description" class="form-control"></textarea></div>--}}
                                                <div class="form-group">
                                                    <label class="checkbox">
                                                        <input name="activate"  checked value="activate" type="checkbox"> Activate
                                                    </label>
                                                </div>

                                            </div>
                                            <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                            <div class="form-actions text-right pal">
                                                <button type="submit" class="btn btn-green">Edit User</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    $(document).ready(function () {

    $('input[type="radio"].role-radio').click(function () {
    var $rb = $(this);

    if ($rb.val() === 'admin') {
    $('#departments').hide();
    }
    else {
    $('#departments').show();

    }
    });

    })
@stop
