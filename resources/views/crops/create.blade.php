@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption pull-left">
                                        <ul class="list-unstyled list-inline">
                                            <li><a href="crops.html"><i class="fa fa-arrow-circle-left fa-4"></i></a></li>
                                            <li><h4>Edit Value Chain</h4></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row mbm">
                                <div class="col-lg-8">
                                    <div class="panel panel-green">
                                        <div class="panel-heading">Edit Value chain details</div>
                                        <div class="panel-body pan">
                                            <form  role="form" method="POST" action="{{url('/crops/store')}}">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-body pal">
                                                    <div class="form-group"><label class="control-label"> Name</label>

                                                        <div class="input-icon left"><i class="fa fa-leaf"></i><input
                                                                    id="name" value="" name="name" type="text" placeholder=""
                                                                    class="form-control"/></div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group"><label class="control-label">Units Of measuremet</label>

                                                                <div class="input-icon left"><i class="fa fa-spinner"></i>
                                                                    <input name="unit_of_measurement"
                                                                           id="unitsOfMeasurement" value="" type="text" placeholder=""
                                                                           class="form-control"/></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-group"><label class="control-label"> description</label><textarea
                                                                rows="3" name="description" class="form-control"></textarea></div>

                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="submit" class="btn btn-green">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.crops').addClass("active");
@endsection