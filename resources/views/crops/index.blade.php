@extends('layouts.master')

@section('content')
    <div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                {!! Breadcrumbs::render('crops') !!}
            </div>

            <div class="clearfix"></div>
        </div>
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet box">
                    <div class="portlet-header">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="caption pull-left">
                                    <ul class="list-unstyled list-inline">
                                        <li><h4>Value chain</h4></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="new-region pull-right"><a href="{{url('crops/create')}}" class="btn btn-green btn-outlined btn-sm "><i class="fa fa-plus"></i>&nbsp;&nbsp;New Value Chain</a></div>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row mbm">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div class="panel panel-green filterable">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Value Chains</h3>
                                            <div class="pull-right">
                                                {{--<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>--}}
                                            </div>
                                        </div>
                                        <table id="crops-table" class="table">
                                            <thead>
                                            <tr class="filters">

                                                <th>Value Chain</th>
                                                <th>Description</th>
                                                <th>Units</th>
                                                <th>Actions</th>

                                            </tr>
                                            </thead>

                                        </table>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
@section('js')

    $(function() {

    $('#crops-table').DataTable({
    processing: true,
    serverSide: true,
    "bStateSave": true,
    ajax: '{{ url('/crops/get-crops') }}',
    "columns": [
    {
    "data": "name", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
     searchable: true
    },

    {
    "data": "description", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    searchable: true
    },
    {
    "data": "unit_of_measurement", // can be null or undefined
    "defaultContent": "<i>Not set</i>",
    },

    {
    "data": "id", // can be null or undefined
    "render": function(data, type, full, meta) {
    console.log(full)

    return '<a href="/crops/edit/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm"><i class="fa fa-pencil"></i></a><a href="/crops/destroy/'+ full.id +'" class="btn btn-warning btn-outlined btn-sm delete"><i class="fa fa-trash"></i></a>';
    },
    "defaultContent": "<i>Not set</i>"
    },
    ]
    });
    });

    $("#crops-table").on("click", ".delete", function(){
    if(confirm("Are you sure you want to delete this crop")){
    return true;
    }
    return false;
    });
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.crops').addClass("active");
@endsection