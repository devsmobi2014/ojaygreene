@extends('app')

@section('content')

   <div class="container" id="dashboard_container">
       <br><br>

    <div class="row" id="content">

        @include('errors.message')


        @include('sidebar.sidebar')


        <div class="col-lg-9" id="body">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">{{$crop->name}}<a href="/crops" class="pull-right" id="add_btn"><i class="glyphicon glyphicon-backward"></i> Back</a></h3>
                </div>
                <div class="panel-body">
                    <div>
                        <p>{{$crop->description}}</p>
                    </div>
                </div><!-- panel-body -->
            </div><!-- panel-default -->
        </div><!-- end of body -->

    </div><!-- end of content -->

   </div>
@endsection
@section('js')
    $("#side-menu").find('.active').removeClass("active"); //Remove any "active" class
    $('.crops').addClass("active");
@endsection

@section('footer')
