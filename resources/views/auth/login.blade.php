@extends('other_layout')
@section('content')
    <div class="page-form">
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form role="form" method="POST" action="/auth/login" class="form">
            <div class="header-content"><h1>Log In</h1></div>
            <div class="body-content">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <div class="input-icon right"><i class="fa fa-user"></i>
                        <input type="email" class="form-control"  placeholder="Email" name="email" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-icon right"><i class="fa fa-key"></i>
                        <input type="password" class="form-control"  placeholder="password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-icon right">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>

                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-success">Log In
                        &nbsp;<i class="fa fa-chevron-circle-right"></i></button>
                </div>
                <div class="clearfix"></div>

                <hr>
                <div class="forget-password"><h4>Forgotten your Password?</h4>

                    <p>no worries, click <a href='/password/email' class='btn-forgot-pwd'>here</a> to reset your password.</p>
                </div>
            </div>
        </form>
    </div>
@stop
